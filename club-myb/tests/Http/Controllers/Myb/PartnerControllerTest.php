<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Myb\Partner;

class PartnerControllerTest extends TestCase
{

	use DatabaseMigrations;
	use WithoutMiddleware;
	
	public function testIndexPartner()
	{
		$partners = $this->createPartner(5);

		$this->get(route('partners.index'))->seeStatusCode(200);
		
		foreach ($partners as $partner) {
			$this->seeJson([
				'name' => $partner->name,
				'url' => $partner->url
			]);
		}
	}
	
	private function createPartner($n = 1)
	{
		return factory(Partner::class, $n)->create();
	}
}
