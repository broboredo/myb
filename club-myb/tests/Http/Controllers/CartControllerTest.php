<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Spark\Spark;
use App\Listeners\Myb\MybUserSubscribed;
use Laravel\Spark\Events\Subscription\UserSubscribed;

class CartControllerTest extends TestCase
{
    use DatabaseMigrations;   
    
    /**
     * Testando inserção com usuário BÁSICO do produto VIP
     * 
     * Não inserir
     * 
     * @test
     */
    public function testBasicUserInsertVipProduct()
    {
    	//creating a product
    	$product = factory(App\Product::class)->create([
			'enable' => true,
			'vip' => true,
			'coin_value' => 70,
			'quantity' => rand(1, 100),
    	]);
    	
    	$plan = Spark::plans()->where('id', 'plan-survivor')->first(); //get basic plan
    	
    	//creating a user with basic plan
    	$user = factory(App\User::class, 1)
    	->create([
    			'email' => 'basic_user@myb.com.br',
    			'current_billing_plan' => $plan->id
    	])
    	->each(function ($user) {
    		factory(Laravel\Spark\Subscription::class)
    		->create([
    				'user_id' => $user->id,
    				'name' => $user->current_billing_plan
    		]);
    	});

		//get user and logging him
		$user = \App\User::where('email', 'basic_user@myb.com.br')->firstOrFail();
		$this->be($user);
    	
		//run event after subscribed
		$listener = new MybUserSubscribed();
		$listener->handle(new UserSubscribed($user, $plan, false));
		
		//vars before post
		$beforeQuantity = $product->quantity; //get pre quantity from product
		$beforeCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
		
		//insert product
		$this->post('cart', $product->toArray());
		
		//vars after post
		$afterProduct = \App\Product::find($product->id); //get actual quantity product
		$afterCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
		
		//tests
		$this->assertEquals($beforeQuantity, $afterProduct->quantity);
		$this->assertEquals($beforeCart, $afterCart);
    }
    
    /**
     * Testando inserção com usuário VIP do produto VIP
     * 
     * Inserir
     * 
     * @test
     */
    public function testVipUserInsertVipProduct()
    {
    	//creating a product
    	$product = factory(App\Product::class)->create([
    			'enable' => true,
    			'vip' => true,
    			'coin_value' => 5,
    			'quantity' => rand(1, 100),
    	]);
    	 
    	$plan = Spark::plans()->where('id', 'plan-insane')->first(); //get vip plan
    	 
    	//creating a user with basic plan
    	factory(App\User::class, 1)
    	->create([
    			'email' => 'vip_user@myb.com.br',
    			'current_billing_plan' => $plan->id
    	])
    	->each(function ($user) {
    		factory(Laravel\Spark\Subscription::class)
    		->create([
    				'user_id' => $user->id,
    				'name' => $user->current_billing_plan
    		]);
    	});
    	
		//get user and logging him
		$user = \App\User::where('email', 'vip_user@myb.com.br')->firstOrFail();
		$this->be($user);
    		 
		//run event after subscribed
		$listener = new MybUserSubscribed();
		$listener->handle(new UserSubscribed($user, $plan, false));
    	
		//vars before post
		$beforeQuantity = $product->quantity; //get pre quantity from product
		$beforeCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
    	
		//insert product
		$this->post('cart', $product->toArray());
    	
		//vars after post
		$afterProduct = \App\Product::find($product->id); //get actual quantity product
		$afterCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
    	
		//tests
		$this->assertEquals(75, $user->coins());
		$this->assertLessThan($beforeQuantity, $afterProduct->quantity);
		$this->assertGreaterThan($beforeCart, $afterCart);
    }
    
    /**
     * Testando inserção de produto inativo
     * 
     * Não inserir
     * 
     * @test
     */
    public function testInsertProductDisable()
    {
    	//creating a product
    	$product = factory(App\Product::class)->create([
    			'enable' => false,
    			'vip' => false,
    			'coin_value' => 5,
    			'quantity' => rand(1, 100),
    	]);
    	
    	$plan = Spark::plans()->random(); //get plan
    	
    	//creating a user with basic plan
    	factory(App\User::class, 1)
    	->create([
    			'email' => 'product_disable@myb.com.br',
    			'current_billing_plan' => $plan->id
    	])
    	->each(function ($user) {
    		factory(Laravel\Spark\Subscription::class)
    		->create([
    				'user_id' => $user->id,
    				'name' => $user->current_billing_plan
    		]);
    	});
    	
		//get user and logging him
		$user = \App\User::where('email', 'product_disable@myb.com.br')->firstOrFail();
		$this->be($user);
    		 
		//run event after subscribed
		$listener = new MybUserSubscribed();
		$listener->handle(new UserSubscribed($user, $plan, false));
    	
		//vars before post
		$beforeQuantity = $product->quantity; //get pre quantity from product
		$beforeCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
    	
		//insert product
		$this->post('cart', $product->toArray());
    	
		//vars after post
		$afterProduct = \App\Product::find($product->id); //get actual quantity product
		$afterCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
    	
		//tests
		$this->assertEquals($beforeQuantity, $afterProduct->quantity);
		$this->assertEquals($beforeCart, $afterCart);
    }
    
    /**
     * Testando inserção de produto sem estoque
     * 
     * Não inserir
     * 
     * @test
     */
    public function testInsertProductNoStock()
    {
    	//creating a product
    	$product = factory(App\Product::class)->create([
    			'enable' => true,
    			'vip' => false,
    			'coin_value' => 30,
    			'quantity' => 0,
    	]);
    	
    	$plan = Spark::plans()->random(); //get plan
    	
    	//creating a user with basic plan
    	factory(App\User::class, 1)
    	->create([
    			'email' => 'nostock@myb.com.br',
    			'current_billing_plan' => $plan->id
    	])
    	->each(function ($user) {
    		factory(Laravel\Spark\Subscription::class)
    		->create([
    				'user_id' => $user->id,
    				'name' => $user->current_billing_plan
    		]);
    	});
    	
		//get user and logging him
		$user = \App\User::where('email', 'nostock@myb.com.br')->firstOrFail();
		$this->be($user);
    		 
		//run event after subscribed
		$listener = new MybUserSubscribed();
		$listener->handle(new UserSubscribed($user, $plan, false));
    	
		//vars before post
		$beforeQuantity = $product->quantity; //get pre quantity from product
		$beforeCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
    	
		//insert product
		$this->post('cart', $product->toArray());
    	
		//vars after post
		$afterProduct = \App\Product::find($product->id); //get actual quantity product
		$afterCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
    	
		//tests
		$this->assertEquals($beforeQuantity, $afterProduct->quantity);
		$this->assertEquals($beforeCart, $afterCart);
    	
    }
    
    /**
     * Testando inserção de produto mais caro ao que o usuário
     * possui de pontos
     * 
     * Não inserir
     * 
     * @test
     */
    public function testInsertProductMoreExpensiveThanUserCoins()
    {
    	//creating a product
    	$product = factory(App\Product::class)->create([
    			'enable' => true,
    			'vip' => false,
    			'coin_value' => 120,
    			'quantity' => rand(1, 100),
    	]);
    	 
    	$plan = Spark::plans()->random(); //get plan
    	 
    	//creating a user with basic plan
    	factory(App\User::class, 1)
    	->create([
    			'email' => 'expensive@myb.com.br',
    			'current_billing_plan' => $plan->id
    	])
    	->each(function ($user) {
    		factory(Laravel\Spark\Subscription::class)
    		->create([
    				'user_id' => $user->id,
    				'name' => $user->current_billing_plan
    		]);
    	});
    	
		//get user and logging him
		$user = \App\User::where('email', 'expensive@myb.com.br')->firstOrFail();
		$this->be($user);
    		 
		//run event after subscribed
		$listener = new MybUserSubscribed();
		$listener->handle(new UserSubscribed($user, $plan, false));
    	
		//vars before post
		$beforeQuantity = $product->quantity; //get pre quantity from product
		$beforeCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
    	
		//insert product
		$this->post('cart', $product->toArray());
    	
		//vars after post
		$afterProduct = \App\Product::find($product->id); //get actual quantity product
		$afterCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
    	
		//tests
		$this->assertEquals($beforeQuantity, $afterProduct->quantity);
		$this->assertEquals($beforeCart, $afterCart);
    }
    
    public function testInsertAndRemoveProductInCart()
    {
    	//creating a product
    	$product = factory(App\Product::class)->create([
    			'enable' => true,
    			'vip' => false,
    			'coin_value' => 20,
    			'quantity' => 100,
    	]);
    	
    	$plan = Spark::plans()->random(); //get plan
    	
    	//creating a user with basic plan
    	factory(App\User::class, 1)
    	->create([
    			'email' => 'add_remove@myb.com.br',
    			'current_billing_plan' => $plan->id
    	])
    	->each(function ($user) {
    		factory(Laravel\Spark\Subscription::class)
    		->create([
    				'user_id' => $user->id,
    				'name' => $user->current_billing_plan
    		]);
    	});
    	
		//get user and logging him
		$user = \App\User::where('email', 'add_remove@myb.com.br')->firstOrFail();
		$this->be($user);
    		 
		//run event after subscribed
		$listener = new MybUserSubscribed();
		$listener->handle(new UserSubscribed($user, $plan, false));
    	
		//vars before post
		$beforeQuantity = $product->quantity; //get pre quantity from product
		$beforeCart = count(\App\Cart::where('user_id', $user->id)->get()); //get all records from cart
    	
		//insert product
		$this->post('cart', $product->toArray());
    	
		//vars after post
		$afterProduct = \App\Product::find($product->id); //get actual quantity product
		$afterCart = \App\Cart::where('user_id', $user->id)->get(); //get all records from cart
    	
		//tests
		$this->assertLessThan($beforeQuantity, $afterProduct->quantity);
		$this->assertGreaterThan(0, count($afterCart));
		$this->assertGreaterThan($beforeCart, count($afterCart));
		 
		//removing 
		foreach ($afterCart as $row)
		{
			$beforeQuantity = $row->product->quantity; //get quantity from product
		
			//$this->delete('cart/'.$row->id)->seeStatusCode(302); //deleting product of box
			$this->delete('cart/'.$row->id); //deleting product of box
		
			$product = \App\Product::find($row->product_id); //refresh quantity of product
		
			//tests
			$this->assertGreaterThan($beforeQuantity, $product->quantity);
		}
		
		$actualCart = count(\App\Cart::where('user_id', $user->id)->get());
		
		//tests
		$this->assertLessThan(count($afterCart), $actualCart);
    }
}
