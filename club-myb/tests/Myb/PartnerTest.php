<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Myb\Partner;

class PartnerTest extends TestCase
{
	use DatabaseMigrations;

	public function testCreatePartner()
	{
		$this->createPartner();
		$found_user = Partner::find(1);
		
		$this->assertNotNull($found_user->name);
		 
	}
	
	public function testDeletePartner()
	{
		$user = $this->createPartner();
		$user->delete();
		 
		$this->notSeeInDatabase('users', ['id' => 1]);
	}


	private function createPartner($n = 1)
	{
		return factory(Partner::class, $n)->create();
	}
}
