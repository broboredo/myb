<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Myb\ExtractCoins;

class ExtractCoinsTest extends TestCase
{
	use DatabaseMigrations;
	
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testInsertCoins()
    {
    	$this->withoutMiddleware();
    	
    	if(is_null(App\User::all())) {
    		factory(App\User::class, 4)->create();
    	}
    	
        factory(ExtractCoins::class, 4)->create(); //creating extract
        
        $extract = ExtractCoins::all();
        
        $this->assertGreaterThan(0, count($extract));
        
        foreach ($extract as $e)
        {
        	$this->assertNotNull($e->debit);
        	$this->assertNotNull($e->active);
        	$this->assertNotNull($e->coin_value);
        	$this->assertNotNull($e->user_id);
        }
    }
}
