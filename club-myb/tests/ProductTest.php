<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Product;

class ProductTest extends TestCase
{
    use DatabaseMigrations;

    public function testStoreProduct()
    {
    	$this->withoutMiddleware(); //no auth middleware
    	
    	$datas = factory(App\Product::class)->make(); //creating a object
    	
    	//send object to method to save it
    	//$this->action('POST', 'ProductController@store', [], $datas->toArray());
    	$this->post('products', $datas->toArray());

    	$products = Product::all(); //get all products
    	
    	//tests
    	$this->assertGreaterThan(0, count($products));
    	
    	foreach ($products as $product)
    	{    		
			$multipleOfFive = ($product->coin_value % 5 === 0) ? true : false;
			
			$this->assertGreaterThanOrEqual(5, $product->coin_value, "Greater Than or Equal 5");
			$this->assertTrue($multipleOfFive, "Multiple of 5");
			$this->assertGreaterThan(date("Y-m-d H:i:s"), $product->release);
    	}
    }
}
