<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use Laravel\Spark\Spark;
use Laravel\Spark\Subscription;
use App\Listeners\Myb\MybUserSubscribed;
use Laravel\Spark\Events\Subscription\UserSubscribed;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    public function testCreateUser()
    {
    	$this->createUser();
    	$found_user = User::find(1);
    	
    	$this->assertNotNull($found_user->name);
    	
    }
    
    public function testDeleteUser()
    {
    	$user = $this->createUser();
    	$user->delete();
    	
    	$this->notSeeInDatabase('users', ['id' => 1]);
    }
    
    public function testSubscribingUser()
    {
    	$plans = Spark::plans(); //get the last plan created
    	
    	//creating new users and subscribing they with the last plan
		factory(App\User::class, 5)
		->create([
			'current_billing_plan' => $plans->random()->name
		])
		->each(function ($user) {
			factory(Laravel\Spark\Subscription::class)
			->create([
				'user_id' => $user->id,
				'name' => $user->current_billing_plan
    		]);
		});
		
		$users = App\User::all();

		$listener = new MybUserSubscribed();
		$listener->handle(new UserSubscribed($users->first(), $plans->random(), false));
		
		//tests
		$this->assertGreaterThan(0, count($users));
		
		foreach ($users as $user)
		{
			$this->assertNotNull($user->coins());
			$this->assertNotNull($user->extract());
		}
		
		$extracts = App\Myb\ExtractCoins::all();
		
		$this->assertGreaterThan(0, count($extracts));
		
    	foreach ($extracts as $e) {	
    		$this->assertGreaterThan(0, $e->coin_value);
    		$this->assertGreaterThan(1, $e->coin_value);
    	}
    	
    }
    
    
    private function createUser($n = 1) 
    {
		return factory(App\User::class, $n)->create();
    }
}
