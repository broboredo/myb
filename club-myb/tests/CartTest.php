<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Product;

class CartTest extends TestCase
{
    use DatabaseMigrations;
    
    /**
     * Test insert cart's
     * 
     * @test
     */
    public function testInsertAndRemoveCart()
    {
    	$this->withoutMiddleware();
    	
    	factory(App\Cart::class)->create();    

    	$items = App\Cart::all();
    	$this->assertGreaterThan(0, count($items));
    	
    	foreach ($items as $item)
    	{
    		$this->assertNotNull($item->product);
    		$this->assertNotNull($item->user);
    		
    		$item->delete();
    	}
    	
    	$items = App\Cart::all();
    	$this->assertEquals(0, count($items));
    }
}
