<?php

namespace Laravel\Spark\Http\Middleware;

use Illuminate\Support\Facades\Auth;

class VerifyUserIsSubscribed
{
    /**
     * Verify the incoming request's user has a subscription.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $subscription
     * @param  string  $plan
     * @return \Illuminate\Http\Response
     */
    public function handle($request, $next, $subscription = 'default', $plan = null)
    {
    	
        if (is_null(Auth::user()->subscription()->ends_at)) {
            return $next($request);
        }

        return redirect()->route('change-plan');
    }
}
