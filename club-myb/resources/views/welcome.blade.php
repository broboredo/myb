@extends('layouts.app')

@yield('styles')

@section('content')

<section id="banner">
	<div class="container">

		<div class="typing">
			<h1>Um clube de assinatura</h1>
			<h1>para quem ama a cultura <span class="typed-here"></span></h1>
		</div>
		
		<div class="text-center">
			<a href="#works" class="btn white">Saiba mais</a>
			<a href="/register" class="btn">Seja um MYBER</a>
		</div>
	</div>

</section>

<section id="works">
	<div class="container">
		<h2>Como funciona</h2>
		<ul id="funciona" class="owl-carousel">
			<li class="item item1">
				<div class="container">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<span class="survival"></span>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<p>São <span>dois</span> planos<br> para você escolher.</p>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<span class="insane"></span>
					</div>
				</div>
			</li>
			<li class="item item2">
				<div class="container">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<span class="survival"></span>
						<p>30 Coins</p>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<p>O valor <span>pago</span> no plano<br> será convertido em <span>Coins</span>.</p>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<span class="insane"></span>
						<p>75 Coins</p>
					</div>
				</div>
			</li>
			<li class="item item3">
				<div class="container">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p>Os produtos ficarão na<br> <span>Vitrine</span> durante um mês.</p>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<span class="funko"></span>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p>Cada produto possui <span>valor</span><br> e <span>quantidade</span> diferentes.</p>
					</div>
				</div>
			</li>
			<li class="item item4">
				<div class="container">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p>Alguns produtos <br> são <span>limitados</span>.</p>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<span class="camisa"></span>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p>O quanto <span>antes</span> você <br> escolher, <span>mais</span> opções terá.</p>
					</div>
				</div>
			</li>
			<li class="item item5">
				<div class="container">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p>Escolha seus produtos<br> com <span>atenção</span>!</p>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<span class="caixa"></span>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p>Você só terá <br><span>uma</span> box por tema.</p>
					</div>
				</div>
			</li>
			<li class="item item6">
				<div class="container">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p>Você tem <span>60</span> dias <br>para usar seus Coins.</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 center coin">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<span class="coins"></span>
								<p>30 coins</p>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<span class="coins"></span>
								<p>30 coins</p>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
								<span class="coins"></span>
								<p>60 coins</p>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p>Se quiser, você pode<br>usá-los no <span>próximo</span> mês.</p>
					</div>
				</div>
			</li>
		</ul>
	</div>
</section>

<section id="video">
	<div class="container">
		<h2>Ainda com dúvidas? Veja o vídeo abaixo</h2>
		
		<div class="col-g-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
			<div class="videoWrapper">
				<iframe width="560" height="315" src="https://www.youtube.com/embed/i6FFk9_RbwQ" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>

	</div>
</section>

<section id="plans">
	<div class="container">
		<h3>Confira nossos planos e escolha o seu nível</h3>

		<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">

			<div class="row">

				<div class="col-lg-6 col-md-6 col-sm-12 text-center">
					<img alt="BrasÃ£o Plano Survival" src="img/brasao_survivor.png" class="img-responsive" />
					<h1>Survival</h1>
					<h3>Mode</h3>

					<div class="amount yellow"><span>R$</span> 85<span class="va-top">,90</span></div>
					<div class="info white">C/ frete grátis*</div>
					
					<div class="info-from white">+ 30 coins</div>
					<div class="info-from white">+ acesso à  vitrine</div>
					<div class="info-from white">+ descontos em lojas parceiras</div>
					<div class="info-from white">&nbsp;</div>
					<div class="info-from white">&nbsp;</div>

					<a id="imsurvivor" href="{{ route('register') }}" class="btn">Eu sou um SURVIVAL!</a>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 text-center">
					<img alt="BrasÃ£o Plano Insane" src="img/brasao_insane.png" class="img-responsive" />
					<h1>Insane</h1>
					<h3>Mode</h3>

					<div class="amount yellow"><span>R$</span> 169<span class="va-top">,90</span></div>
					<div class="info white">C/ frete grátis*</div>
					
					<div class="info-from white">+ 75 coins</div>
					<div class="info-from white">+ acesso à  vitrine</div>
					<div class="info-from white">+ produtos exclusivos na vitrine</div>
					<div class="info-from white">+ descontos exclusivos em lojas parceiras</div>
					<div class="info-from white">+ plano fidelidade</div>

					<a id="iminsane" href="{{ route('register') }}" class="btn">Ah, não. Com certeza sou INSANE!</a>
				</div>

				<div class="asterisk">*Os valores estão sujeitos à  mudança dos Correios, podendo haver cobrança em alguns casos e para alguns Estados.</div>
			</div>

		</div>
	</div>
</section>

<section id="vitrine">
	<h3>Veja os produtos de maiores destaques da nossa Vitrine.</h3>
	<div class="clearfix"></div>
	@foreach($products as $product)
		<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-border">
			<div class="block-product">
				<div class="img-product text-center">
					<img src="{{ asset('storage/products') }}/{{ $product->img }}" class="img-responsive" />
				</div>

				<div class="options text-center">
					<div class="coin-value" style="position: relative;top: 60px;">
						<h5>{{ $product->name }}</h5>
						@if(!is_null($product->origin) && !empty($product->origin))
							<h6>({{ $product->origin }})</h6>
						@endif
						<div class="show-coin">
							<i class="coins"></i> {{ $product->coin_value }} coins
						</div>
					</div>
					<div class="product-details" style="position: relative;">
						<a href="{{ route('register') }}">
							<button class="btn">Assinar</button>
						</a>
					</div>
				</div>
			</div>
		</div>
	@endforeach
	<div class="col-md-12 col-xs-12 col-lg-12 col-sm-12 text-center" style="margin: 20px 0">
		<a href="{{ route('site-vitrine') }}">
			<button class="btn btn-lg">
				Veja mais produtos
			</button>
		</a>
	</div>
</section>
<div class="clearfix"></div>

<section id="friends">
	<div class="container">
		<ul id="slider" class="text-center">
			<li class="item"><a href="http://www.pixtee.com.br" title="Parceiro Pixtee" target="_blank"><img src="{{ asset('img/partners/pixtee.png') }}" /></a></li>
			<li class="item"><a href="http://www.postergeek.com.br" title="Parceiro Poster Geek" target="_blank"><img src="{{ asset('img/partners/postergeek.png') }}" /></a></li>
			<li class="item"><a href="http://www.touts.com.br" title="Parceiro touts" target="_blank"><img src="{{ asset('img/partners/touts.png') }}" /></a></li>
			<li class="item"><a href="http://www.geeks.etc.br" title="Parceiro GEEK'S" target="_blank"><img src="{{ asset('img/partners/geeks.png') }}" /></a></li>
			<li class="item"><a href="http://www.geek10.com.br" title="Parceiro Geek 10" target="_blank"><img src="{{ asset('img/partners/geek10.png') }}" /></a></li>
			<li class="item"><a href="http://www.poltronavip.com" title="Parceiro Poltrona VIP" target="_blank"><img src="{{ asset('img/partners/poltronavip.png') }}" /></a></li>
		</ul>
	</div>
</section>
@endsection