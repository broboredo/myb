@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('home.dashboard') }}</div>

                    <div class="panel-body">
                        {{ trans('home.app_dashboard') }}
                    </div>
                </div>
            </div>
            
            @if (!is_null(Auth::user()->extract()->get()))
	        	<div class="col-md-4">
	                <div class="panel panel-default">
	                    <div class="panel-heading">
	                    	Extrato 
	                    	<span class="pull-right">
	                    		<strong>¢oins: </strong>{{ Auth::user()->coins() }}
                    		</span>
						</div>
	                    <div class="panel-body">
	                    	@foreach (Auth::user()->extract()->get() as $row)
	                    		<p><strong>Data: </strong>{{ date('d/m/Y - H:i', strtotime($row->created_at)) }}</p>
	                    		<p><strong>Valor: </strong>{{ $row->coin_value }}</p>
	                    		@if(!is_null($row->details))
	                    			<p>{{ $row->details }}</p>
	                    		@endif
	                    		<hr />
	                    	@endforeach
	                    </div>
	                </div>
	        	</div>
	        @endif
        </div>
    </div>
</home>
@endsection
