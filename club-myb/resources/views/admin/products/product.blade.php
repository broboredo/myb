@extends('layouts.admin')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::open(['action' => 'ProductController@store', 'files' => true]) !!}
    {{ Form::hidden('id', is_null($product) ? null :$product->id) }}
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        {{ Form::label('name', 'Nome*', ['class' => 'control-label']) }}
                        {{ Form::text('name', is_null($product) ? null : $product->name, ['id' => 'name', 'class' => 'form-control', 'required' => true]) }}
                    </div>
                </div>

                <div class="col-md-6 col-lg- col-sm-6 col-xs-6">
                    <div class="form-group">
                        {{ Form::label('origin', 'Origem', ['class' => 'control-label']) }}
                        {{ Form::text('origin', is_null($product) ? null : $product->origin, ['id' => 'origin', 'class' => 'form-control']) }}
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
                    <div class="form-group">
                        {{ Form::label('release', 'Lançamento*', ['class' => 'control-label']) }}
                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                {{ Form::select('month', $months, is_null($product) ? \Carbon\Carbon::now()->month : $product->month, ['id' => 'month', 'class'=>'form-control', 'required'=>true]) }}
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">
                                {{ Form::select('year', $years, is_null($product) ? \Carbon\Carbon::now()->year : $product->year, ['id' => 'year', 'class'=>'form-control', 'required'=>true]) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                    <div class="form-group">
                        {{ Form::label('quantity', 'Quantidade*', ['class' => 'control-label']) }}
                        {{ Form::number('quantity', is_null($product) ? 1 : $product->quantity, ['id' => 'quantity', 'class' => 'form-control', 'required' => true]) }}
                    </div>
                </div>


                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                    <div class="form-group">
                        {{ Form::label('price', 'Preço (de compra: 050.00)*', ['class' => 'control-label']) }}
                        {{ Form::text('price', is_null($product) ? null : $product->price, ['id' => 'price', 'class' => 'form-control', 'required' => true]) }}
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                    <div class="form-group">
                        {{ Form::label('purchase_price', 'Preço (de venda: 090.00)*', ['class' => 'control-label']) }}
                        {{ Form::text('purchase_price', is_null($product) ? null : $product->purchase_price, ['id' => 'purchase_price', 'class' => 'form-control', 'required' => true]) }}
                    </div>
                </div>


                <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('description', 'Descrição', ['class' => 'control-label']) }}
                        {{ Form::textarea('description', is_null($product) ? null : $product->description, ['id' => 'description', 'class' => 'form-control', 'placeholder' => 'Use tags html (<b>, <u>, <i>, <br>,...']) }}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6 col-sm-6">
                    <div class="form-group">
                        {{ Form::label('img', 'Foto*', ['class' => 'control-label']) }}
                        {{ Form::file('img', ['id' => 'img', 'required' => true]) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="checkbox">
                    <div class="row">
                        <label>
                            {{ Form::checkbox('enable', is_null($product) ? 1 : $product->enable, true, ['id' => 'enable', 'class' => 'form-control flat']) }} Ativo
                        </label>
                    </div>
                </div>

                <div class="checkbox">
                    <div class="row">
                        <label>
                            {{ Form::checkbox('adults', is_null($product) ? 0 : $product->adults, false, ['id' => 'adults', 'class' => 'form-control flat']) }} Para maiores de 18 anos?
                        </label>
                    </div>
                </div>

                <div class="checkbox">
                    <div class="row">
                        <label>
                            {{ Form::checkbox('subscriber_details', is_null($product) ? 0 : $product->subscriber_details, false, ['id' => 'subscriber_details', 'class' => 'form-control flat']) }} Necessita de detalhes do assinante?
                        </label>
                    </div>
                </div>

                <div class="checkbox">
                    <div class="row">
                        <label>
                            {{ Form::checkbox('vip', is_null($product) ? 0 : $product->vip, false, ['id' => 'vip', 'class' => 'form-control flat']) }} Exclusivo para Insane?
                        </label>
                    </div>
                </div>

                <div class="checkbox">
                    <div class="row">
                        <label>
                            {{ Form::checkbox('craft_product', is_null($product) ? 0 : $product->craft_product, false, ['id' => 'craft_product', 'class' => 'form-control flat']) }} Feito sob demanda?
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-ms-12">
            <div class="form-group pull-right">
                <button type="submit" class="btn btn-primary btn-lg">Salvar</button>
            </div>
        </div>
    {!! Form::close() !!}
@endsection