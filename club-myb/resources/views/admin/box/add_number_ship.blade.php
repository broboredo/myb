@extends('layouts.admin')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::open(['action' => 'PurchaseHistoryController@update']) !!}
    {{ Form::hidden('id', $purchase->id) }}
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        {{ Form::label('number_ship', 'Código de Rastreio*', ['class' => 'control-label']) }}
                        {{ Form::text('number_ship', null, ['id' => 'number_ship', 'class' => 'form-control', 'required' => true]) }}
                    </div>
                </div>

                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <p><strong>Usuário:</strong> {{ $purchase->user()->first()->name }}</p>
                    <p><strong>E-mail do Usuário:</strong> {{ $purchase->user()->first()->email }}</p>
                    <p><strong>Valor Total (¢):</strong> ¢ {{ $purchase->amount() }}</p><br/>
                    <p><strong>Produtos:</strong></p>

                    @foreach($purchase->products()->get() as $product)
                        <p><strong>Nome: </strong>{{ $product->product()->first()->name }} | <strong>¢oins: </strong>{{ $product->product()->first()->coin_value }}</p>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-ms-12">
            <div class="form-group pull-right">
                <button type="submit" class="btn btn-primary btn-lg">Salvar</button>
            </div>
        </div>
    {!! Form::close() !!}
@endsection