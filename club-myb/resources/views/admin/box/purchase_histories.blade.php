@extends('layouts.admin')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Boxes</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Usuário</th>
                            <th>E-mail</th>
                            <th>Valor Total (¢)</th>
                            <th>Código de Rastreio</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($purchases as $purchase)
                                <tr>
                                    <th>{{ $purchase->id }}</th>
                                    <th>{{ $purchase->user()->first()->name }}</th>
                                    <th>{{ $purchase->user()->first()->email }}</th>
                                    <th>¢ {{ $purchase->amount() }}</th>
                                    <th>
                                        @if(is_null($purchase->number_ship))
                                            <a href="{{  route('admin-purchase-add-number-ship', ['id' => $purchase->id]) }}" class="btn btn-sm btn-primary">Add Código de Rastreio</a></th>
                                        @else
                                            {{ $purchase->number_ship }}
                                        @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection