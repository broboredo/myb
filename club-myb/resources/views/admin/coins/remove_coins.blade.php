@extends('layouts.admin')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    {!! Form::open(['action' => 'Myb\ExtractCoinsController@removeCoins']) !!}
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-xs-12 col-md-6">
                            <div class="form-group">
                                {{ Form::label('user_id', 'Usuário*', ['class' => 'control-label']) }}
                                {{ Form::select('user_id', $users, null, ['id' => 'user_id', 'class' => 'form-control', 'required' => true]) }}
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('coin_value', 'Quantidade de Coins*', ['class' => 'control-label']) }}
                                {{ Form::select('coin_value', $values , 5, ['id' => 'coin_value', 'class' => 'form-control', 'required' => true]) }}
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                {{ Form::label('details', 'Detalhe*', ['class' => 'control-label']) }}
                                {{ Form::textarea('details', null, ['id' => 'details', 'class' => 'form-control', 'required' => true]) }}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-ms-12">
            <div class="form-group pull-right">
                <button type="submit" class="btn btn-primary btn-lg">Criar</button>
            </div>
        </div>
    {!! Form::close() !!}
@endsection