@extends('layouts.admin')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <ul class="nav navbar-right">
                <li><a href="{{ route('admin-add-coins') }}" class="btn btn-primary"><i class="fa fa-dollar"></i> Dar Coin</a></li>
            </ul>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Coins</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Usuário</th>
                            <th>¢</th>
                            <th>Detalhes</th>
                            <th>Ativo</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($coins as $coin)
                                <tr>
                                    <th>{{ $coin->id }}</th>
                                    <th>{{ $coin->user()->first()->name }}</th>
                                    <th>{{ $coin->coin_value }}</th>
                                    <th>{{ $coin->details }}</th>
                                    <th>{{ $coin->active }}</th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection