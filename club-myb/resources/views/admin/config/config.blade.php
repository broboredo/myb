@extends('layouts.admin')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (isset($success))
        <div class="alert alert-success">
            <ul>
                <li>{{ $success }}</li>
            </ul>
        </div>
    @endif


    {!! Form::open(['action' => 'AdminController@openShop']) !!}
        <div class="col-lg-12 col-md-12 col-ms-12">
            {{ Form::label(null, 'Este botão libera todos usuários para montar box novamente') }}
            <div class="form-group">
                <button type="submit" title="Este botão libera todos usuários para montar box novamente" class="btn btn-primary btn-lg">Abrir Mercado</button>
            </div>
        </div>
    {!! Form::close() !!}

    {!! Form::open(['action' => 'AdminController@disableWaitingLists']) !!}
        <div class="col-lg-12 col-md-12 col-ms-12">
            {{ Form::label(null, 'Desativar Lista de Espera') }}
            <div class="form-group">
                <button type="submit" title="Este botão libera os cadastros de novos usuários" class="btn btn-danger btn-lg">Desativar Lista de Espera</button>
            </div>
    {!! Form::close() !!}

    {!! Form::open(['action' => 'AdminController@enableWaitingLists']) !!}
            {{ Form::label(null, 'Ativar Lista de Espera') }}
            <div class="form-group">
                <button type="submit" title="Este botão trava os cadastros de novos usuários" class="btn btn-success btn-lg">Ativar Lista de Espera</button>
            </div>
        </div>
    {!! Form::close() !!}
@endsection