Usuario: {{ $nome }} <br/>
E-mail: {{ $email }} <br />
CPF: {{ $user->cpf }} <br />
ID_USER: {{ $user->id }} <br />
<br />
<br />
CEP: {{ $user->billing_zip }} <br/><br/>

Endereco: {{ $user->billing_address }} <br/>
Nº: {{ $user->billing_number }} <br/>
Complemento: {{ $user->billing_address_line_2 }} <br/>
Estado: {{ $user->billing_state }} <br/><br />

Cidade: {{ $user->billing_city }} <br/>
Bairro: {{ $user->billing_neighborhood }} <br/>

<br /><br />

<b>Produtos:</b> <br />
@foreach($products as $product)
	Produto: {{ $product->product()->first()->name }} <br />
	Produto: {{ $product->product()->first()->coin_value }} <br />
	ID_PRODUTO: {{ $product->product()->first()->id }} <br />	
	<hr><br />
@endforeach