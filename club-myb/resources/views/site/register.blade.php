@extends('layouts.app')

@section('scripts')
    <script src="https://js.stripe.com/v2/"></script>
    <script>
        function prev() {
            var active = $('fieldset:not(.hidden)');
            var active_id = $(active).attr('id');
            var nextFieldset = $(active).prev('fieldset.hidden');
            var nextFieldset_id = $(nextFieldset).attr('id');

            $(active).addClass('animated fadeOutRight');

            $('li #_'+active_id).removeClass('active');
            $('li #_'+nextFieldset_id).addClass('active');

            setTimeout(function () {
                $(active).addClass('hidden').removeClass('animated fadeOutRight');

                $(nextFieldset).removeClass('hidden').addClass('animated fadeInLeft');
            }, 500);

            return false;
        }


        function next() {
            if($('.country').val() == null) {
                $('.country').val('BR');
            }
            var active = $('fieldset:not(.hidden)');
            var active_id = $(active).attr('id');
            var nextFieldset = $(active).next('fieldset.hidden');
            var nextFieldset_id = $(nextFieldset).attr('id');

            $(active).addClass('animated fadeOutLeft');
            
            $('li #_'+active_id).removeClass('active');
            $('li #_'+nextFieldset_id).addClass('active');

            setTimeout(function () {
                $(active).addClass('hidden').removeClass('animated fadeOutLeft');

                $(nextFieldset).removeClass('hidden').addClass('animated fadeInRight');
            }, 500);

            return false;
        }

    </script>
@endsection

@section('content')
    <spark-register-stripe inline-template>
        <div class="register">
            <section id="banner">
                <div class="container">
                    <h1>Faça Seu Cadastro</h1>
                </div>
            </section>

            <section id="content" class="pre-section">
                <div class="container">
                    <h1>"IT'S TIME."</h1>
                    <h3>- Bruce Buffer -</h3>
                </div>
            </section>

            <section class="menu-wizard container-fluid visible-lg visible-md">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="menu-wizard">
                        <li class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div id="_plans" class="active">
                                <i class="fa fa-4x fa-pencil"></i>
                                <span>Assinatura</span>
                            </div>
                        </li>
                        <li class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div id="_user">
                                <i class="fa fa-4x fa-smile-o"></i>
                                <span>Perfil</span>
                            </div>
                        </li>
                        <li class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div id="_card">
                                <i class="fa fa-4x fa-credit-card"></i>
                                <span>Pagamento</span>
                            </div>
                        </li>
                        <li class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div id="_confirm">
                                <i class="fa fa-4x fa-check"></i>
                                <span>Confirmação</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>

            <section id="wizard-content">
                <div class="spark-screen container">
                    <div v-if="selectedPlan && selectedPlan.price > 0">
                        @include('site.register.warnings')

                        <form class="form-horizontal" role="form">
                            <fieldset id="plans">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <!-- Common Register Form Contents -->
                                        @include('site.register.plan')
                                    </div>
                                </div>

                                <div class="row area-buttons">
                                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <button onclick="next(); return false;" class="btn center first-button disabled" data-id="next">Próximo Passo</button>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="hidden" id="user">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <!-- Common Register Form Contents -->
                                        @include('site.register.user')
                                    </div>
                                </div>

                                <div class="row area-buttons">
                                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
                                        <button onclick="next(); return false;" class="btn pull-right" data-id="next">Próximo Passo</button>
                                        <button onclick="prev(); return false;" class="btn btn-white pull-right" data-id="previous" style="margin-right: 5px;">Voltar</button>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="hidden" id="card">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        @include('site.register.card')
                                    </div>
                                </div>

                                <div class="row area-buttons">
                                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        <button onclick="next(); return false;" class="btn pull-right" data-id="next">Próximo Passo</button>
                                        <button onclick="prev(); return false;" class="btn btn-white pull-right" data-id="previous" style="margin-right: 5px;">Voltar</button>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset class="hidden" id="confirm">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                        @include('site.register.confirm')
                                    </div>

                                    <div class="row area-buttons">
                                        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                                            <button type="submit" class="btn pull-right" @click.prevent="register" :disabled="registerForm.busy">
                                                <span v-if="registerForm.busy">
                                                    <i class="fa fa-btn fa-spinner fa-spin"></i>{{ trans('spark.auth.stripe.registering') }}
                                                </span>

                                                <span v-else>
                                                    <i class="fa fa-btn fa-check-circle"></i>{{ trans('spark.auth.stripe.register') }}
                                                </span>
                                            </button>

                                            <button onclick="prev(); return false;" class="btn btn-white pull-right" data-id="previous" style="margin-right: 5px;">Voltar</button>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>

            </section>

            <!-- Plan Features Modal -->
            @include('spark::modals.plan-details')
        </div>
    </spark-register-stripe>
@endsection