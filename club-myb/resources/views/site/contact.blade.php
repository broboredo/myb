@extends('layouts.app')

@section('content')

<div class="contato">
    <section id="banner">
        <div class="container">
            <h1>Fale com a gente</h1>
        </div>
    </section>

    <section id="content" class="pre-section">
        <div class="container">
            <h1>"A dúvida é o príncipio da sabedoria."</h1>
            <h3>- Aristóteles -</h3>
        </div>
    </section>

    <section id="texto">
        <div class="container">

            @if(Session::has('message'))
                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12">
                    <div class="alert alert-success">
                        <i class="fa fa-2x fa-check"></i>
                        {{ Session::get('message') }}
                    </div>
                </div>
            @else
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-0 col-sm-12">
                    <h4>
                        E aí? Está com algum princípio de sabedoria?
                        Preencha o formulário abaixo que a gente vai te ajudar.
                        Ah! E se quiser elogiar ou criticar, essa é a hora! Fique a vontade.
                    </h4>
                </div>
            @endif

            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="row">
                    {!! Form::open(['action' => 'ContactController@store']) !!}
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('name', 'Nome*', ['class' => 'label']) }}
                                {{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control input-lg', 'required' => true]) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('email', 'E-mail*', ['class' => 'label']) }}
                                {{ Form::email('email', null, ['id' => 'email', 'class' => 'form-control input-lg', 'required' => true]) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('phone', 'Telefone', ['class' => 'label']) }}
                                {{ Form::text('phone', null, ['id' => 'phone', 'class' => 'form-control input-lg phone']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('subject', 'Assunto*', ['class' => 'label']) }}
                                {{ Form::select('subject', ['Elogio' => 'Elogio', 'Reclamação' => 'Reclamação', 'Sugestão' => 'Sugestão', 'Parceiro' => 'Seja um Parceiro'], null, ['id' => 'subject', 'class' => 'form-control input-lg', 'required' => true]) }}
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('message', 'Mensagem*', ['	class' => 'label']) }}
                                {{ Form::textarea('message', null, ['class' => 'form-control input-lg', 'required' => true]) }}
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-ms-12">
                            <div class="form-group pull-right">
                                <button type="submit" class="btn">Enviar</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
</div>

@endsection