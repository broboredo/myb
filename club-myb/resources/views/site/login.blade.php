@extends('layouts.app')

@section('content')

<div class="login">
    <section id="banner">
        <div class="container">
            <h1>Entrar na sua nave</h1>
        </div>
    </section>

    <section id="content" class="pre-section">
        <div class="container">
            <h1>"Sigam-me os bons"</h1>
            <h3>- Chapolin Colorado -</h3>
        </div>
    </section>

    <section id="texto">
        <div class="container">

            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12">
                @include('spark::shared.errors')

                <form class="form-horizontal" role="form" method="POST" action="/login">
                    {{ csrf_field() }}

                    <!-- E-Mail Address -->
                    <div class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <label>{{ trans('spark.auth.login.email_address') }}</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
                        </div>
                    </div>

                    <!-- Password -->
                    <div class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <label>{{ trans('spark.auth.login.password') }}</label>
                            <input type="password" class="form-control" name="password" style="margin-bottom: 10px;">
                            <div class="control-group pull-left">
                                <label class="control control--checkbox">
                                    <input type="checkbox" name="remember">
                                    <div class="control__indicator"></div>
                                    <div class="text">{{ trans('spark.auth.login.remember_me') }}</div>
                                </label>
                            </div>
                            <a class="pull-right" href="{{ route('forgot') }}">Putz! Esqueci a senha</a>
                        </div>
                    </div>

                    <!-- Remember Me -->
                    <div class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                        </div>
                    </div>



                    <!-- Login Button -->
                    <div class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12">
                            <button type="submit" class="btn pull-right">
                                Vamos Nessa
                            </button>
                        </div>
                    </div>
                </form>

                <section class="text-center">
                    <h4 style="margin-bottom: 0px;">Ainda não faz parte do Clube? Tá esperando o que?</h4>
                    <a class="yellow" href="{{ route('register') }}">Faça seu cadastro e se torne um Myber!</a>
                </section>
            </div>
        </div>
    </section>
</div>

@endsection