@extends('layouts.app')

@section('content')

<div class="contato">
    <section id="banner">
        <div class="container">
            <h1>Vai perder?</h1>
        </div>
    </section>

    <section id="content" class="pre-section">
        <div class="container">
            <h1>"Vocês pediram, a #MYB atendeu."</h1>
            <h3>- Make Your Box -</h3>
        </div>
    </section>

    <section id="vitrine">
        <div class="container-fluid">
			@foreach($products as $key=>$product)
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-border">
			    	<div class="block-product">
			       		<div class="img-product text-center">
			            	<img src="{{ asset('storage/products') }}/{{ $product->img }}" class="img-responsive" />
			         	</div>
			
			            <div class="options text-center">
			            	<div class="coin-value" style="position: relative;top: 60px;">
			                	<h5>{{ $product->name }}</h5>
			                	@if(!is_null($product->origin) && !empty($product->origin))
			                    	<h6>({{ $product->origin }})</h6>
			                    @endif
			                    <div class="show-coin">
			                    	<i class="coins"></i> {{ $product->coin_value }} coins
			                    </div>
			             	</div>
			                <div class="product-details" style="position: relative;">
			                	<a href="{{ route('register') }}">
			                    	<button class="btn">Assinar</button>
			               		</a>
			               	</div>
			         	</div>
			      	</div>
				</div>
			@endforeach
			
		</div>
    </section>
</div>

@endsection