@extends('layouts.app')

@yield('styles')

@section('content')

<div class="informacoes">
    <section id="banner">
        <div class="container">
            <h1>Informações Gerais</h1>
        </div>
    </section>

    <section id="content" class="pre-section">
        <div class="container">

            <div class="info-menu">

                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('questions') }}">
                                <img src="{{ asset('img/question.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('terms') }}">
                                <img src="{{ asset('img/terms.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('returns') }}">
                                <img src="{{ asset('img/returns.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('privacy') }}">
                                <img src="{{ asset('img/privacy_active.png') }}" class="img-responsive" />
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <h2>Política de Privacidade</h2>
        </div>
    </section>

    <section id="texto">
        <div class="container">

            <div class="col-lg-12 col-md-12 col-sm-12">
				<p>A proteção e a manipulação de seus dados pessoais são extremamente importantes e as tratamos com muito cuidado. 
				A coleta, como parte do processo de assinatura, gerenciamento, processamento ou utilização das informações pessoais necessárias, 
				são feitas seguindo estritamente às normas de Proteção das Informações brasileiras e não são disponibilizadas a terceiros.</p>
				
				<p>Suas informações são importantes, pois nos ajudam a tornar o site um lugar melhor e cada vez mais direcionado à você, usuário, 
				na busca de sua total satisfação. Com prévia autorização dos clientes, enviamos e-mails informando as novidades ou promoções vigentes 
				em nosso site, porém nossos clientes podem solicitar o não recebimento destes e-mails.</p>
				
				<p>Não há o armazenamento de informações que não sejam necessárias para a efetivação da assinatura. O uso do CPF é exigido por lei para a 
				emissão de notas fiscais e para o envio de mercadorias. As senhas fornecidas em nosso banco de dados são arquivadas de maneira criptografada, 
				permitindo que apenas o dono do cadastro tenha conhecimento.</p>
				
				<p>No âmbito da regulamentação jurídica, estamos aptos para fins de publicidade, pesquisa de mercado e melhoria dos nossos serviços para 
				avaliar os perfis de utilização sob um pseudônimo, apenas caso você não tenha exercido seu direito legal de se opor a esta utilização de seus dados.</p>
				
				<p>O nosso clube também é munido com o Certificado de Segurança RapidSSL, um dos certificados digitais mais modernos. Através dele, todas as informações transitadas pela internet são criptografadas.</p>
				
				<p>Este é um site 100% seguro e utiliza a tecnologia de Cookies apenas para proporcionar mais facilidade no momento de sua assinatura. 
				O objetivo do Cookie é customizar ao máximo sua navegação no site, permitindo maior facilidade e agilidade no processo de assinatura.</p>
				
				<p>Em conformidade com as disposições legais, caso as informações tenham de ser utilizadas para finalidade que requeira o consentimento 
				explícito do assinante, solicitaremos, individualmente, pela sua autorização. E, uma vez dado o consentimento para uso das informações, 
				o cliente poderá revogá-lo a qualquer momento para usos futuros. O assinante também tem o direito de, a qualquer momento, receber, 
				corrigir, adicionar ou apagar informações sobre seus dados pessoais armazenados. Os nossos contatos para tanto podem ser encontrados no site.</p>
				
				<strong>Pagamentos</strong>
				<p>Visando proporcionar maior segurança a nossos clientes, trabalhamos em parceria com a <a href="stripe.com" target="_blank">Stripe</a>.</p>
				
				<p>Os números de cartões de crédito fornecidos são registrados diretamente no banco de dados das administradoras de cartão, não permitindo o 
				acesso a essas informações por parte do site. Ao informar os dados para a administradora, essa realiza a verificação da transação on-line e 
				retorna apenas se a compra está liberada ou não. Mesmo que a forma de pagamento escolhida na assinatura seja no débito automático, as 
				informações do cartão de crédito permanecem armazenadas com nosso parceiro Stripe. Em nenhum momento o site armazena todas informações 
				necessárias para a cobrança.</p>
				
				<p>Na Make Your Box, será cobrado mensalmente o valor da assinatura escolhida no cartão de crédito cadastrado, até que o associado opte 
				por suspender um mês ou mesmo cancelar a assinatura. Ela se renovará, automaticamente, todo ano.</p>
				
				<p>A Make Your Box poderá alterar a política deste website de tempos em tempos, portanto sempre atualizando esta página. 
				Você deve acessar esta página de tempos em tempos para certificar-se que está de acordo e satisfeito com as alterações.</p>
				
				<p>Todo associado pode ficar traquilo ao fazer uma compra no site da Make Your Box.</p>

            </div>
        </div>
    </section>
</div>

@endsection