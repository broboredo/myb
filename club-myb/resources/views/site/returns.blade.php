@extends('layouts.app')

@yield('styles')

@section('content')

<div class="informacoes">
    <section id="banner">
        <div class="container">
            <h1>Informações Gerais</h1>
        </div>
    </section>

    <section id="content" class="pre-section">
        <div class="container">

            <div class="info-menu">

                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('questions') }}">
                                <img src="{{ asset('img/question.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('terms') }}">
                                <img src="{{ asset('img/terms.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('returns') }}">
                                <img src="{{ asset('img/returns_active.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('privacy') }}">
                                <img src="{{ asset('img/privacy.png') }}" class="img-responsive" />
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <h2>Trocas e Devoluções</h2>
        </div>
    </section>

    <section id="texto">
        <div class="container">

            <div class="col-lg-12 col-md-12 col-sm-12">
				<p>As trocas são permitidas para casos de produtos com defeito de fabricação ou algum tipo de
				dano causado no transporte. Além de produtos enviados com tamanho diferente do especificado no perfil do usuário.</p>
				
				<p>Os pedidos de troca deverão ser reportados, por e-mail, ao nosso SAC, através do
				<a href="mailto:resolvemeuproblema@makeyourbox.com.br">resolvemeuproblema@makeyourbox.com.br</a>, 
				tendo como título do e-mail: "Troca de produto MYB".</p>
				
				<p>Para facilitar e agilizar a troca, pedimos que anexe ao e-mail, fotos dos produtos que precisam
				ser trocados e informe seus dados, utilizados no cadastro de assinatura. </p>
				
				<p>Mantenha seu produto no mesmo estado que o recebeu. </p>
				
				<p>Caso seja identificado sinistro (extravio ou roubo de carga) no trajeto até o destinatário, a
				Make Your Box se responsabilizará pelo estorno da quantidade de Coins que foram gastos
				nessa box, por parte do usuário, mediante autorização do cliente.</p>
				
				<p>Para devolver um produto por desistência ou arrependimento, serão analisadas as seguintes condições:</p>
				
				<p>• O prazo para desistência da compra é de até 7 dias úteis, a contar da data de recebimento;<br />
				• O produto deverá ser encaminhado na embalagem original, sem indícios de uso, sem violação do lacre original do fabricante, acompanhado de nota fiscal e acessórios; <br />
				• O ressarcimento dos Coins correspondentes serão efetuados após recebimento, análise e aprovação da área de qualidade; </p>
            </div>
        </div>
    </section>
</div>

@endsection