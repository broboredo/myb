@extends('layouts.app')

@section('content')
<section id="banner">
	<div class="container">
		<h1>Redefinir Senha</h1>
	</div>
</section>

<section>
	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">{{ trans('spark.auth.passwords.reset.reset_password') }}</div>
	
	                <div class="panel-body">
	                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
	                        {!! csrf_field() !!}

                        	<input type="hidden" name="token" value="{{ $token }}">
	
	                        <!-- E-Mail Address -->
	                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
	                            <div class="col-md-12">
	                            	<label>{{ trans('spark.auth.passwords.reset.email_address') }}</label>
	                                <input type="email" class="form-control" name="email" value="{{ $email or old('email') }}" autofocus>
	
	                                @if ($errors->has('email'))
	                                    <span class="help-block">
	                                        {{ $errors->first('email') }}
	                                    </span>
	                                @endif
	                            </div>
	                        </div>
	
	                        <!-- Password -->
	                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
	                            <div class="col-md-12">
	                            	<label>{{ trans('spark.auth.passwords.reset.password') }}</label>
	                                <input type="password" class="form-control" name="password">
	
	                                @if ($errors->has('password'))
	                                    <span class="help-block">
	                                        {{ $errors->first('password') }}
	                                    </span>
	                                @endif
	                            </div>
	                        </div>
	
	                        <!-- Password Confirmation -->
	                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
	                            <div class="col-md-12">
	                            	<label>{{ trans('spark.auth.passwords.reset.confirm_password') }}</label>
	                                <input type="password" class="form-control" name="password_confirmation">
	
	                                @if ($errors->has('password_confirmation'))
	                                    <span class="help-block">
	                                        {{ $errors->first('password_confirmation') }}
	                                    </span>
	                                @endif
	                            </div>
	                        </div>
	
	                        <!-- Reset Button -->
	                        <div class="form-group">
	                            <div class="col-md-12">
	                                <button type="submit" class="btn pull-right">{{ trans('spark.auth.passwords.reset.reset_password') }}</button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</section>
@endsection