@extends('layouts.app')

@section('content')
<section id="banner">
	<div class="container">
		<h1>Esqueci a Senha</h1>
	</div>
</section>

<section>
	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {!! csrf_field() !!}

                        <!-- E-Mail Address -->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                            	<label>{{ trans('spark.auth.passwords.email.email_address') }}</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- Send Password Reset Link Button -->
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn pull-right">Redefinir Senha</button>
                            </div>
                        </div>
                    </form>
	        </div>
	    </div>
	</div>
</section>
@endsection