<div class="pre-meet row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
        <h5>Viu como foi rápido?</h5>
        <h5>Para acabar, só falta colocar os dados de cobrança.</h5>
    </div>
</div>

<!-- Generic 500 Level Error Message / Stripe Threw Exception -->
<div class="alert alert-danger" v-if="registerForm.errors.has('form')">
    {{ trans('spark.auth.stripe.error_500') }}
</div>

<!-- Cardholder's Name -->
<div class="form-group">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label for="name">{{ trans('spark.auth.stripe.credit_card_name') }}* (igual ao do cartão)</label>
        <input type="text" class="form-control" name="name" v-model="cardForm.name">
    </div>
</div>

<!-- Card Number -->
<div class="form-group" :class="{'has-error': cardForm.errors.has('number')}">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <label>{{ trans('spark.auth.stripe.credit_card_number') }}*</label>
        <input type="text" class="form-control" name="number" data-stripe="number" v-model="cardForm.number">

        <span class="help-block" v-show="cardForm.errors.has('number')">
            @{{ cardForm.errors.get('number') }}
        </span>
    </div>

    <!-- Security Code -->
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <label>{{ trans('spark.auth.stripe.credit_card_security_code') }}*</label>
        <input type="text" class="form-control" name="cvc" data-stripe="cvc" v-model="cardForm.cvc">
    </div>



    <!-- Expiration -->
    <!-- Month -->
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <label>Data de Validade*</label>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="month"
                       placeholder="MM" maxlength="2" data-stripe="exp-month" v-model="cardForm.month">
            </div>

            <!-- Year -->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="year"
                       placeholder="AAAA" maxlength="4" data-stripe="exp-year" v-model="cardForm.year">
            </div>
        </div>
    </div>
</div>

<!-- Coupon Code -->
<div class="form-group" :class="{'has-error': registerForm.errors.has('coupon')}" v-if="query.coupon">
    <div class="col-md-6 col-xs-12">
        <label>{{ trans('spark.auth.stripe.coupon_code') }}</label>
        <input type="text" class="form-control" name="coupon" v-model="registerForm.coupon">

        <span class="help-block" v-show="registerForm.errors.has('coupon')">
                                                @{{ registerForm.errors.get('coupon') }}
                                            </span>
    </div>
</div>

<!-- Tax / Price Information -->
<div class="form-group" v-if="spark.collectsEuropeanVat && countryCollectsVat && selectedPlan">
    <div class="col-md-6">
        <label>&nbsp;</label>
        <div class="alert alert-info" style="margin: 0;">
            <strong>{{ trans('spark.auth.stripe.tax') }}</strong> @{{ taxAmount(selectedPlan) | currency }}
            <br><br>
            <strong>{{ trans('spark.auth.stripe.tax_total') }}</strong>
            @{{ priceWithTax(selectedPlan) | currency }} / @{{ selectedPlan.interval | capitalize }}
        </div>
    </div>
</div>