<div class="pre-meet row">
    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1">
        <h5>Vamos começar!</h5>
        <h5>Primeiro escolha o plano que mais lhe agrada!</h5>
    </div>
</div>

<!-- Plan Selection -->
<div v-if="paidPlans.length > 0">
    <div class="row">

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 plano">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <img alt="Brasão Plano Survival" src="{{ asset('img/brasao_survivor.png') }}" class="img-responsive" />
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4>Survival</h4>
                <h5>Mode</h5>
                <a class="yellow" @click="showPlanDetails(paidPlans[1])">Saiba Mais</a>

                <div class="choose-plan">
                    <div v-for="plan in paidPlans">
                        <span v-if="plan.name == 'Survival'">
                            <div class="control-group">
                                <label class="control control--radio">
                                    <input type="radio" name="planChoose" @click="selectPlan(plan)">
                                    <div class="control__indicator"></div>
                                    <span v-if="plan.interval == 'monthly'">
                                        <div class="text">Mensal | <span>R$ @{{ plan.price }}</span></div>

                                    </span>
                                    <span v-else>
                                        <div class="text">Anual | <span>R$ @{{ plan.price }}</span></div>
                                    </span>
                                </label>
                            </div>
                        </span>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 plano">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <img alt="Brasão Plano Insane" src="{{ asset('img/brasao_insane.png') }}" class="img-responsive" />
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <h4>Insane</h4>
                <h5>Mode</h5>
                <a class="yellow" @click="showPlanDetails(paidPlans[0])">Saiba Mais</a>

                <div class="choose-plan">
                    <div v-for="plan in paidPlans">
                        <span v-if="plan.name == 'Insane'">

                            <div class="control-group">
                                <label class="control control--radio">
                                    <input type="radio" name="planChoose" @click="selectPlan(plan)">
                                    <div class="control__indicator"></div>
                                    <span v-if="plan.interval == 'monthly'">
                                        <div class="text">Mensal | <span>R$ @{{ plan.price }}</span></div>

                                    </span>
                                    <span v-else>
                                        <div class="text">Anual | <span>R$ @{{ plan.price }}</span></div>
                                    </span>
                                </label>
                            </div>
                        </span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>