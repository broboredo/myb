<!-- Coupon -->
<div class="row" v-if="coupon">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-success">
            <div class="panel-heading">{{ trans('spark.auth.register.coupons.discount') }}</div>

            <div class="panel-body">
                {{ trans('spark.auth.register.coupons.discount_message', ['discount' => @discount]) }}
            </div>
        </div>
    </div>
</div>

<!-- Invalid Coupon -->
<div class="row" v-if="invalidCoupon">
    <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-danger">
            {{ trans('spark.auth.register.coupons.invalid') }}
        </div>
    </div>
</div>

<!-- Invitation -->
<div class="row" v-if="invitation">
    <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-success">
            {{ trans('spark.auth.register.coupons.invitation') }} <strong>@{{ invitation.team.name }}</strong> {{ Spark::teamString() }}!
        </div>
    </div>
</div>

<!-- Invalid Invitation -->
<div class="row" v-if="invalidInvitation">
    <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-danger">
            {{ trans('spark.auth.register.coupons.invitation_invalid') }}
        </div>
    </div>
</div>