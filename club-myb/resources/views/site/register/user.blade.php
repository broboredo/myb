<div class="pre-meet row">
    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 center">
        <h5>Vamos falar do mais interessante agora. Você!</h5>
        <h5>É coisa rápida, tá? A gente também não curte formulários gigantescos.</h5>
    </div>
</div>

<!-- Basic Profile -->
<!-- Generic Error Message -->
<div class="alert alert-danger" v-if="registerForm.errors.has('form')">
    @{{ registerForm.errors.get('form') }}
</div>

<!-- Invitation Code Error -->
<div class="alert alert-danger" v-if="registerForm.errors.has('invitation')">
    @{{ registerForm.errors.get('invitation') }}
</div>

<!-- Registration Form -->
<!-- Name -->
<div class="form-group" :class="{'has-error': registerForm.errors.has('name'), 'has-error': registerForm.errors.has('email')}">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <label>{{ trans('spark.auth.register.name') }}*</label>
        <input type="name" class="form-control" name="name" v-model="registerForm.name">

        <span class="help-block" v-show="registerForm.errors.has('name')">
            @{{ registerForm.errors.get('name') }}
        </span>
    </div>

    <!-- Email -->
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <label>{{ trans('spark.auth.register.email') }}*</label>
        <input type="email" class="form-control" name="email" v-model="registerForm.email">

        <span class="help-block" v-show="registerForm.errors.has('email')">
            @{{ registerForm.errors.get('email') }}
        </span>
    </div>
</div>

<div class="form-group" :class="{'has-error': registerForm.errors.has('cpf'), 'has-error': registerForm.errors.has('password'), 'has-error': registerForm.errors.has('password_confirmation')}">
    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
        <label>CPF*</label>
        <input type="text" class="form-control cpf" name="cpf" v-model="registerForm.cpf">

        <span class="help-block" v-show="registerForm.errors.has('cpf')">
            @{{ registerForm.errors.get('cpf') }}
        </span>
    </div>

    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
        <label>{{ trans('spark.auth.register.password') }}*</label>
        <input type="password" class="form-control" name="password" v-model="registerForm.password">

        <span class="help-block" v-show="registerForm.errors.has('password')">
            @{{ registerForm.errors.get('password') }}
        </span>
    </div>

    <!-- Password Confirmation -->
    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
        <label>{{ trans('spark.auth.register.confirm_password') }}*</label>
        <input type="password" class="form-control" name="password_confirmation" v-model="registerForm.password_confirmation">

        <span class="help-block" v-show="registerForm.errors.has('password_confirmation')">
            @{{ registerForm.errors.get('password_confirmation') }}
        </span>
    </div>
</div>

<div class="form-group" :class="{'has-error': registerForm.errors.has('state'), 'has-error': registerForm.errors.has('city'), 'has-error': registerForm.errors.has('neighborhood')}">
    <!-- CEP -->
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <label>CEP*</label></label>
        <input type="text" class="form-control cep" required="required" v-model="registerForm.zip">

        <span class="help-block" v-show="registerForm.errors.has('zip')">
            @{{ registerForm.errors.get('zip') }}
        </span>
    </div>

    <!-- Neighborhood -->
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <label>Bairro*</label>
        <input type="text" class="form-control neighborhood" required="required" v-model="registerForm.neighborhood">

        <span class="help-block" v-show="registerForm.errors.has('neighborhood')">
            @{{ registerForm.errors.get('neighborhood') }}
        </span>
    </div>

    <!-- City -->
    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
        <label>{{ trans('spark.auth.register_address.city') }}*</label></label>
        <input type="text" class="form-control city" required="required" v-model="registerForm.city">

        <span class="help-block" v-show="registerForm.errors.has('city')">
            @{{ registerForm.errors.get('city') }}
        </span>
    </div>

    <!-- State -->
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
        <label>Estado*</label>
        <input type="text" class="form-control state" required="required" v-model="registerForm.state">

        <span class="help-block" v-show="registerForm.errors.has('state')">
            @{{ registerForm.errors.get('state') }}
        </span>
    </div>
</div>


<!-- Address -->
<div class="form-group" :class="{'has-error': registerForm.errors.has('address'), 'has-error': registerForm.errors.has('address_line_2')}">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
        <label>{{ trans('spark.auth.register_address.address') }}*</label>
        <input type="text" class="form-control address" required="required" v-model="registerForm.address">

        <span class="help-block" v-show="registerForm.errors.has('address')">
            @{{ registerForm.errors.get('address') }}
        </span>
    </div>

    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
        <label>Nº*</label>
        <input type="text" class="form-control number" name="number" required="required" v-model="registerForm.number">

        <span class="help-block" v-show="registerForm.errors.has('number')">
            @{{ registerForm.errors.get('number') }}
        </span>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <label>{{ trans('spark.auth.register_address.address_line_2') }}</label>
        <input type="text" class="form-control address2" v-model="registerForm.address_line_2">

        <span class="help-block" v-show="registerForm.errors.has('address_line_2')">
            @{{ registerForm.errors.get('address_line_2') }}
        </span>
    </div>
</div>

<!-- Country -->
<div class="form-group hidden" :class="{'has-error': registerForm.errors.has('country')}">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>{{ trans('spark.auth.register_address.country') }}*</label>
        <select class="form-control country">
            <option value="BR" selected="selected">Brasil</option>
        </select>

        <span class="help-block" v-show="registerForm.errors.has('country')">
            @{{ registerForm.errors.get('country') }}
        </span>
    </div>
</div>

<!-- European VAT ID -->
<div class="form-group" :class="{'has-error': registerForm.errors.has('vat_id')}" v-if="countryCollectsVat">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <label>{{ trans('spark.auth.register_address.vat_id') }}</label>
        <input type="text" class="form-control" v-model.lazy="registerForm.vat_id">

        <span class="help-block" v-show="registerForm.errors.has('vat_id')">
            @{{ registerForm.errors.get('vat_id') }}
        </span>
    </div>
</div>
