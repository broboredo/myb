<div class="pre-meet row">
    <div class="col-lg-8 col-md-8 col-sm-10 center">
        <h5>Está tudo pronto!</h5>
        <h5>Só dá uma conferidinha nos seus dados antes de prosseguir.</h5>
    </div>
</div>

<!-- Terms And Conditions -->
<div class="form-group" :class="{'has-error': registerForm.errors.has('terms')}">
    <div class="col-md-12 col-xs-12 center">
    
	<span v-if="selectedPlan">
		<span v-if="selectedPlan.name == 'Insane'">
		    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 center confirmar-logo">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<img alt="Brasão Plano Insane" src="{{ asset('img/brasao_insane.png') }}" class="img-responsive" />
				</div>
		
				<div class="col-lg-6 col-md-6 col-sm-4 col-xs-6">
					<h4>Insane</h4>
					<h5>Mode</h5>
				</div>
			</div>
		</span>
		<span v-else>
		    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 center confirmar-logo">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<img alt="Brasão Plano Survival" src="{{ asset('img/brasao_survivor.png') }}" class="img-responsive" />
				</div>
		
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<h4>Survival</h4>
					<h5>Mode</h5>
				</div>
			</div>
		</span>
	</span>
	
	<div class="clearfix"></div>

	<!-- Registration Form -->
		<!-- Name -->
		<div class="form-group">
		    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" :class="{'has-error': registerForm.errors.has('name')}">
		        <label>{{ trans('spark.auth.register.name') }}</label>
		        <input type="name" class="form-control" name="name" v-model="registerForm.name" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('name')">
		            @{{ registerForm.errors.get('name') }}
		        </span>
		    </div>
		
		    <!-- Email -->
		    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" :class="{'has-error': registerForm.errors.has('email')}">
		        <label>{{ trans('spark.auth.register.email') }}</label>
		        <input type="email" class="form-control" name="email" v-model="registerForm.email" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('email')">
		            @{{ registerForm.errors.get('email') }}
		        </span>
		    </div>
		</div>

		<div class="form-group">
		    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" :class="{'has-error': registerForm.errors.has('cpf')}">
		        <label>CPF</label>
		        <input type="text" class="form-control cpf" name="cpf" v-model="registerForm.cpf" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('cpf')">
		            @{{ registerForm.errors.get('cpf') }}
		        </span>
		    </div>
		
		    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" :class="{'has-error': registerForm.errors.has('password')}">
		        <label>{{ trans('spark.auth.register.password') }}</label>
		        <input type="password" class="form-control" name="password" v-model="registerForm.password" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('password')">
		            @{{ registerForm.errors.get('password') }}
		        </span>
		    </div>
		
		    <!-- Password Confirmation -->
		    <div class="col-lg-5 col-md-5 col-ms-5 col-xs-12" :class="{'has-error': registerForm.errors.has('password_confirmation')}">
		        <label>{{ trans('spark.auth.register.confirm_password') }}</label>
		        <input type="password" class="form-control" name="password_confirmation" v-model="registerForm.password_confirmation" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('password_confirmation')">
		            @{{ registerForm.errors.get('password_confirmation') }}
		        </span>
		    </div>
		</div>

		<div class="form-group">
		    <!-- CEP -->
		    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" :class="{'has-error': registerForm.errors.has('zip')}">
		        <label>CEP</label></label>
		        <input type="text" class="form-control cep" v-model="registerForm.zip" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('zip')">
		            @{{ registerForm.errors.get('zip') }}
		        </span>
		    </div>
		
		    <!-- Neighborhood -->
		    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" :class="{'has-error': registerForm.errors.has('neighborhood')}">
		        <label>Bairro</label>
		        <input type="text" class="form-control neighborhood" v-model="registerForm.neighborhood" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('neighborhood')">
		            @{{ registerForm.errors.get('neighborhood') }}
		        </span>
		    </div>
		
		    <!-- City -->
		    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12" :class="{'has-error': registerForm.errors.has('city')}">
		        <label>{{ trans('spark.auth.register_address.city') }}</label></label>
		        <input type="text" class="form-control city" v-model="registerForm.city" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('city')">
		            @{{ registerForm.errors.get('city') }}
		        </span>
		    </div>
		
		    <!-- State -->
		    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12" :class="{'has-error': registerForm.errors.has('state')}">
		        <label>Estado</label>
		        <input type="text" class="form-control state" v-model="registerForm.state" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('state')">
		            @{{ registerForm.errors.get('state') }}
		        </span>
		    </div>
		</div>


		<!-- Address -->
		<div class="form-group">
		    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" :class="{'has-error': registerForm.errors.has('address')}">
		        <label>{{ trans('spark.auth.register_address.address') }}</label>
		        <input type="text" class="form-control address" v-model="registerForm.address" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('address')">
		            @{{ registerForm.errors.get('address') }}
		        </span>
		    </div>
		
		    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" :class="{'has-error': registerForm.errors.has('address_line_2')}">
		        <label>{{ trans('spark.auth.register_address.address_line_2') }}</label>
		        <input type="text" class="form-control address2" v-model="registerForm.address_line_2" disabled="disabled">
		
		        <span class="help-block" v-show="registerForm.errors.has('address_line_2')">
		            @{{ registerForm.errors.get('address_line_2') }}
		        </span>
		    </div>
		</div>
		
		
		<!-- Cardholder's Name -->
		<div class="form-group">
		    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		        <label for="name">{{ trans('spark.auth.stripe.credit_card_name') }}</label>
		        <input type="text" class="form-control" name="name" v-model="cardForm.name" disabled="disabled">
		    </div>
		</div>
		
		<!-- Card Number -->
		<div class="form-group" :class="{'has-error': cardForm.errors.has('number')}">
		    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		        <label>{{ trans('spark.auth.stripe.credit_card_number') }}</label>
		        <input type="text" class="form-control" name="number" data-stripe="number" v-model="cardForm.number" disabled="disabled">
		
		        <span class="help-block" v-show="cardForm.errors.has('number')">
		            @{{ cardForm.errors.get('number') }}
		        </span>
		    </div>
		
		    <!-- Security Code -->
		    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
		        <label>{{ trans('spark.auth.stripe.credit_card_security_code') }}</label>
		        <input type="text" class="form-control" name="cvc" data-stripe="cvc" v-model="cardForm.cvc" disabled="disabled">
		    </div>
		
		
		
		    <!-- Expiration -->
		    <!-- Month -->
		    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		        <label>Data de Validade</label>
		        <div class="row">
		            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		                <input type="text" class="form-control" name="month" maxlength="2" min="2" data-stripe="exp-month" v-model="cardForm.month" disabled="disabled">
		            </div>
		
		            <!-- Year -->
		            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		                <input type="text" class="form-control" name="year" min="4" maxlength="4" data-stripe="exp-year" v-model="cardForm.year" disabled="disabled">
		            </div>
		        </div>
		    </div>
		</div>

        <div class="control-group center">
            <label class="control control--checkbox">
                <input type="checkbox" v-model="registerForm.terms">
                <div class="control__indicator"></div>
                <div class="text">
                    {{ trans('spark.auth.stripe.accept') }}
                    <a href="{{ route('terms') }}" target="_blank">
                        Termos de Uso
                    </a>
                </div>
            </label>
            <span class="help-block" v-show="registerForm.errors.has('terms')">
                <strong>@{{ registerForm.errors.get('terms') }}</strong>
            </span>
        </div>
    </div>
</div>