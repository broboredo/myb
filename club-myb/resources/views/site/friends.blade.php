@extends('layouts.app')

@yield('styles')

@section('content')

    <div class="friends">
        <section id="banner">
            <div class="container">
                <h1>Nossos aliados</h1>
            </div>
        </section>

        <section id="content" class="pre-section">
            <div class="container">
                <h1>
                    "No que diz respeito ao empenho, ao compromisso, ao esforço, à dedicação, não existe meio termo.
                    Ou você faz uma coisa bem feita ou não faz."
                </h1>
                <h3>- Ayrton Senna -</h3>
            </div>
        </section>

        <section id="text">
            <div class="container">

                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-0 col-sm-12">
                    <h4>
                        Esse é um pensamento compartilhado por nós e todos nossos aliados.
                        Você pensa igual? <a href="{{ route('contact') }}">Seja nosso aliado!</a>
                    </h4>
                </div>
				<div class="clearfix"></div>

                <div class="col-lg-2 col-md-2 col-sm-6 block-partner">
                	<a href="http://www.postergeek.com.br" title="Parceiro Poster Geek" target="_blank"><img src="{{ asset('img/partners/postergeek.png') }}" class="img-responsive" /></a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 block-partner">
                	<a href="http://www.pixtee.com.br" title="Parceiro Pixtee" target="_blank"><img src="{{ asset('img/partners/pixtee.png') }}" class="img-responsive" style="margin-top: 20px;" /></a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 block-partner">
                	<a href="http://www.geek10.com.br" title="Parceiro Geek 10" target="_blank"><img src="{{ asset('img/partners/geek10.png') }}" class="img-responsive" style="margin-top: 20px;" /></a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 block-partner">
                	<a href="http://www.geeks.etc.br" title="Parceiro GEEK'S" target="_blank"><img src="{{ asset('img/partners/geeks.png') }}" class="img-responsive" /></a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 block-partner">
                	<a href="http://www.poltronavip.com" title="Parceiro Poltrona VIP" target="_blank"><img src="{{ asset('img/partners/poltronavip.png') }}" class="img-responsive" style="margin-top: 20px;" /></a>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 block-partner">
                	<a href="http://www.touts.com.br" title="Parceiro touts" target="_blank"><img src="{{ asset('img/partners/touts.png') }}" class="img-responsive" /></a>
                </div>
            </div>
        </section>
    </div>

@endsection