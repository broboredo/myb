@extends('layouts.app')

@yield('styles')

@section('content')

<div class="informacoes">
    <section id="banner">
        <div class="container">
            <h1>Informações Gerais</h1>
        </div>
    </section>

    <section id="content" class="pre-section">
        <div class="container">

            <div class="info-menu">

                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('questions') }}">
                                <img src="{{ asset('img/question_active.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('terms') }}">
                                <img src="{{ asset('img/terms.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('returns') }}">
                                <img src="{{ asset('img/returns.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('privacy') }}">
                                <img src="{{ asset('img/privacy.png') }}" class="img-responsive" />
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <h2>Perguntas Frequentes</h2>
        </div>
    </section>

    <section id="texto">
        <div class="container">

            <div class="col-lg-12 col-md-12 col-sm-12">
				<a class="question" data-toggle="collapse" data-target="#quero-assinar">Quero assinar a Make Your Box! Como faço?</a>
				<div id="quero-assinar" class="collapse">
					É muito fácil. Para se tornar um Myber, basta clicar <a href="{{ route('register') }}">aqui</a> e seguir o passo a passo.
				</div>
				
				<!-- <a class="question" data-toggle="collapse" data-target="#ate-quando">Até quando posso me inscrever no clube Make Your Box para receber a box do mês?</a>
				<div id="ate-quando" class="collapse">
					Até o dia 15 de todo mês.
				</div> -->
				
				<a class="question" data-toggle="collapse" data-target="#quanto-tempo-assinatura">Por quanto tempo dura uma assinatura?</a>
				<div id="quanto-tempo-assinatura" class="collapse">
					A assinatura não possui prazo de encerramento. O pagamento é feito de forma automática e o assinante só precisa usufruir 
					de nossos benefícios. No momento em que o assinante não quiser mais participar do clube, basta efetuar 
					o cancelamento. Dessa forma, a cobrança da mensalidade será suspensa.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#quantos-produtos">Quantos e quais produtos receberei?</a>
				<div id="quantos-produtos" class="collapse">
					A Make Your Box te dá o benefício de escolher os produtos que quer receber. 
					Ao efetuar o pagamento, você receberá uma quantidade de Coins por mês. Use estes Coins e troque pelos itens de 
					sua escolha, disponíveis em nossa Vitrine de Produtos. Aqui, quem administra seus Coins é somente você. 
					Então, faça boas escolhas!
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#gift-box">Posso enviar uma caixa de presente?</a>
				<div id="gift-box" class="collapse">
					Infelizmente AINDA não. Mas estamos trabalhando para, em breve, abrir esta possibilidade.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#beneficios">Como funcionam os benefícios do clube?</a>
				<div id="beneficios" class="collapse">
					No momento em que o usuário assinar nosso clube, ele já ganhará benefícios! 
					Cada um dos nossos aliados fornecerá um tipo de benefício diferente, podendo ser alterado 
					sempre que ele quiser. Quem fizer parte do Insane Mode, desfrutará de todas nossas parcerias. 
					Quem fizer parte do Survival Mode, terá direito a bastante coisa também, mas não tudo! 
					Os códigos gerados para utilização desses benefícios, serão constantemente alterados, portanto, 
					ao se desligar do nosso clube, você não terá mais acesso a tais descontos!
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#pagamento">Quais são os meios de pagamento aceitos?</a>
				<div id="pagamento" class="collapse">
					A Make Your Box aposta na praticidade da cobrança recorrente via cartão de crédito, 
					assim você não precisa se preocupar em efetuar o pagamento todo o mês. Esta etapa será automática.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#boleto">Não tenho cartão de crédito. Posso pagar com boleto ou depósito bancário?</a>
				<div id="boleto" class="collapse">
					Infelizmente não. No momento só estamos aceitando cartão de crédito. 
					Mas estamos trabalhando para poder dar mais opções para nossos atuais e futuros Mybers.
				</div>
				
				<!-- <a class="question" data-toggle="collapse" data-target="#cartao-nao-processou">O meu cartão não processou o pagamento deste mês. O que devo fazer?</a>
				<div id="cartao-nao-processou" class="collapse">
					Sabemos que é normal o cartão não passar ... tem mês que ultrapassamos o limite ou o cartão pode estar 
					vencido / com algum dado errado. Por esse motivo, nós praticamos quatro dias seguidos de tentativas 
					de cobrança no seu cartão, sendo que na quinta tentativa (dia) nossa equipe entrará em contato 
					pedindo sua colaboração para que atualize as informações de sua conta. 
					Nesse momento, sua assinatura será automaticamente pausada e recuperada após 15 dias, 
					no qual repetiremos os mesmos procedimentos de cobrança, assim consecutivamente até que o 
					pagamento seja efetivado. Assim que a situação é normalizada voltamos a produzir suas caixas.
				</div> -->
				
				<!-- <a class="question" data-toggle="collapse" data-target="#nota-fiscal">Como é emitida a nota fiscal?</a>
				<div id="nota-fiscal" class="collapse">
					A nota fiscal será emitida no nome do cliente cadastrado. Para isso, disponibilizamos a opção de 
					cadastro tanto de Pessoa Física – cujo documento requerido é o CPF, quanto de Pessoa Jurídica – 
					no qual é requerido o número do CNPJ.

					Esclarecemos que há diferenças entre o cadastro de Pessoa Física e o de Pessoa Jurídica: 
					o nome que será impresso na nota fiscal é o da empresa que possui cadastro no site e as entregas 
					só podem ser realizadas no endereço cadastrado conforme o registro na Receita Federal. 
					Além disso, cadastro em Pessoa Jurídica não gera preço diferenciado ou desconto nas compras realizadas no site.
					Dentro da página "Pedidos" se encontram todas as notas fiscais relacionadas com os pedidos feitos 
					na loja. Você pode baixar a nota nos formatos PDF ou XML.
				</div> -->
				
				<a class="question" data-toggle="collapse" data-target="#alterar-endereco">Posso alterar o endereço de entrega cadastrado?</a>
				<div id="alterar-endereco" class="collapse">
					É claro que pode! Seus dados cadastrais estão disponíveis no seu Perfil Myber. 
					Basta acessar e efetuar a troca. 
					É de extrema importância e de sua responsabilidade manter seus dados sempre atualizados. 
					Quando for alterar seu endereço, esteja ciente de fazê-lo com, pelo menos, 
					15 dias de antecedência da virada do mês, assim a entrega do próximo mês será feita de forma correta.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#frete">Além do plano, eu preciso pagar o frete?</a>
				<div id="frete" class="collapse">
					De jeito nenhum! Não deixe que ninguém te cobre pela entrega. 
					O frete é por nossa conta. 
					Caso você precise pagar algo a mais, por motivos de alteração no valor do serviço de entrega, 
					nós lhe avisaremos com antecedência e o valor pago será abatido junto à sua mensalidade.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#quando-recebo-box">Já escolhi os itens que quero receber. Quando recebo minha box?</a>
				<div id="quando-recebo-box" class="collapse">
					Após a seleção de produtos, iremos preparar sua box com todo carinho e cuidado do mundo. 
					Caso tenhamos todos os produtos em mãos, enviaremos dentro de 2 dias úteis. 
					Porém, caso algum produto que você tenha escolhido seja feito sob encomenda, 
					iremos efetuar o pedido junto ao fornecedor e lhe enviaremos assim que possível, 
					respeitando o prazo fornecido pelo próprio fornecedor.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#produto-danificado">Faltou um produto na caixa e/ou ele chegou danificado, o que faço?</a>
				<div id="produto-danificado" class="collapse">
					Poxa, que vacilo! Neste caso, entra em contato através do e-mail <a href="mailto:resolvemeuproblema@makeyourbox.com.br">resolvemeuproblema@makeyourbox.com.br</a>, 
					nos envie fotos do produto, que daremos início ao processo de troca. Pedimos para manter o produto no mesmo estado que o recebeu.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#nao-recebi-box">Eu não recebi minha box, o que devo fazer?</a>
				<div id="nao-recebi-box" class="collapse">
					Infelizmente, nós não podemos tomar conta das entregas. Com isso, será uma empresa especializada. Caso você não receba sua box no prazo definido, 
					basta entrar em contato com a gente que iremos buscar uma solução junto à empresa responsável. 
					Não precisa entrar em contato com eles, somente conosco! Mas fique tranquilo que você não será prejudicado.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#na-portaria">A box pode ser deixado na portaria do condomínio ou prédio?</a>
				<div id="na-portaria" class="collapse">
					Sim, desde que o porteiro ou zelador assinem o recebimento e tenham mais de 18 anos. 
					Sabemos das dificuldades em receber encomendas durante a semana nas residências e por isso também 
					possibilitamos que os kits sejam entregues em endereços comerciais.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#entregou-cade">Meu pedido consta como entregue, mas não recebi.</a>
				<div id="entregou-cade" class="collapse">
					Entre em contato com a gente através do e-mail <a href="mailto:resolvemeuproblema@makeyourbox.com.br">resolvemeuproblema@makeyourbox.com.br</a> 
					que buscaremos o motivo e uma solução para passar a você! Lembre-se de colocar no assunto “Não recebi minha caixa” 
					e nos dizer, detalhadamente, o que aconteceu!
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#troca">Se eu quiser devolver ou trocar algum produto recebido, como eu faço?</a>
				<div id="troca" class="collapse">
					Nesses casos, você tem até sete dias úteis, a contar da data de recebimento, para avisar sobre o arrependimento. 
					Basta entrar em contato com a gente por e-mail e explicar sua situação, que iremos dar início às etapas de troca para deixa-lo 100% satisfeito. 
					Não se esqueça de manter o produto que queira trocar no mesmo estado que o encontrou. 
					Nosso sistema funciona com distribuição de Coins. Ao recebermos o produto que você não quer mais, 
					adicionaremos os número de Coins gastos ao seu perfil Myber.
					
					<br />
					
					Dá uma espiada na nossa <a href="{{ route('returns') }}">Política de Trocas e Devoluções</a>.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#cancelei-mas-quero-voltar">Cancelei minha assinatura, mas gostaria de reativá-la. Posso?</a>
				<div id="cancelei-mas-quero-voltar" class="collapse">
					Com certeza! Gostamos de ter você com a gente. 
					Então é só efetuar novamente o cadastro para receber nossas novidades.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#pedido-cancelado">Meu pedido foi cancelado, por quê?</a>
				<div id="pedido-cancelado" class="collapse">
					Geralmente os pedidos são cancelados por problemas com o cartão, problemas de limite ou dados incorretos. 
					Não poderemos te dizer os motivos do cancelamento, pois são informações sigilosas. Nesses casos, verifique se as 
					informações preenchidas em seu cadastro estão corretas e entre em contato com a operadora do seu cartão.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#parceria-produto">Gostaria de ver meus produtos na Make Your Box. Como faço?</a>
				<div id="parceria-produto" class="collapse">
					Ficaremos muito felizes em negociar com você! Entre em contato com a gente através do e-mail falecomigo@makeyourbox.com.br 
					ou de nosso formulário em nosso site, que retornaremos o mais breve possível.
				</div>
				
				<a class="question" data-toggle="collapse" data-target="#nao-fui-respondido">Sua dúvida não foi respondida?</a>
				<div id="nao-fui-respondido" class="collapse">
					Entre em contato com a gente através do e-mail falecomigo@makeyourbox.com.br ou de nosso 
					formulário em nosso site, que teremos o maior prazer em ajudá-lo.
				</div>
            </div>
        </div>
    </section>
</div>

@endsection