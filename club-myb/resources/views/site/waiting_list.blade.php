@extends('layouts.app')

@yield('styles')

@section('content')

<div class="contato">
    <section id="banner">
        <div class="container">
            <h1>Fale com a gente</h1>
        </div>
    </section>

    <section id="content" class="pre-section">
        <div class="container">
            <h1>"A dúvida é o príncipio da sabedoria."</h1>
            <h3>- Aristóteles -</h3>
        </div>
    </section>

    <section id="texto">
        <div class="container">

            @if(Session::has('message'))
                <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12">
                    <div class="alert alert-success">
                        <i class="fa fa-2x fa-check"></i>
                        {{ Session::get('message') }}
                    </div>
                </div>
            @else
                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-0 col-sm-12">
                    <h4>
                        Melhor correr antes que seja tarde.
                    </h4>
                </div>
            @endif

            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
                <div class="row">
                    {!! Form::open(['action' => 'WelcomeController@addToWaitingList']) !!}

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('name', 'Nome*', ['class' => 'label']) }}
                                {{ Form::text('name', null, ['id' => 'name', 'class' => 'form-control input-lg', 'required' => true]) }}
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('email', 'E-mail*', ['class' => 'label']) }}
                                {{ Form::email('email', null, ['id' => 'email', 'class' => 'form-control input-lg', 'required' => true]) }}
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group">
                                {{ Form::label('plan_name', 'E qual plano você deseja?*', ['class' => 'label']) }}
                                {{ Form::select('plan_name', [
                                    'Survival_mensal' => 'Survival | Mensal | R$ 85,90',
                                    'Survival_anual' => 'Survival | Anual | R$ 79,90',
                                    'Insane_mensal' => 'Insane | Mensal | R$ 169,90',
                                    'Insane_anual' => 'Insane | Anual | R$ 158,00'
                                ], null, ['id' => 'plan_name', 'class' => 'form-control input-lg', 'required' => true]) }}
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group pull-right">
                                <button type="submit" class="btn">Quero ser um Myber!</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
</div>

@endsection