@extends('layouts.app')

@yield('styles')

@section('content')

    <div class="about">
        <section id="banner">
            <div class="container">
                <h1>Sobre Nós</h1>
            </div>
        </section>

        <section id="content" class="pre-section">
            <div class="container">
                <h1>"A vida não nos dá propósito. Nós damos propósito à vida."</h1>
                <h3>- The Flash -</h3>
            </div>
        </section>

        <section id="texto">
            <div class="container">

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="col-lg-6 col-md-6 col-sm-12">

                        <h5>IDEIAS PRECISAM SAIR DO PAPEL.</h5>

                        <p>
                            Muitas pessoas têm ideias incríveis, mas que só ficam por escrito, e, as vezes, nem isso.
                            Então, será que essas ideias são tão incríveis mesmo? A única forma de saber é tentando,
                            arriscando e experimentando.
                        </p>

                        <p>
                            Tentativa e erro. Quantas vezes não ouvimos isso? Mas, também, ouvimos muito que é errando
                            que se aprende. E queremos aprender junto com vocês. O universo Geek / Nerd / Pop / Gamer
                            e tantos outros apaixonados por uma cultura, precisam ter algo que os una. Nos una. Afinal,
                            nós também somos apaixonados.
                        </p>

                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <img src="{{ asset('img/plans.png') }}" alt="Emblemas dos Planos Survival e Insane" />
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12">

                        <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                            <img src="{{ asset('img/lamp.png') }}" alt="Emblemas dos Planos Survival e Insane" />
                        </div>

                        <p>
                            Foi pensando nisso que o Clube Make Your Box saiu do papel. Uma forma de juntar esses
                            milhares de apaixonados com empresas que possuem a mesma linguagem, o mesmo desejo de
                            consumir cultura.
                        </p>

                        <p>
                            A partir do momento em que um usuário escolhe um plano, ele se torna um Myber. Com isso,
                            de cara, já obtem todos os benefícios de nossos Aliados. Cada um com uma proposta diferente
                            da outra. Além disso, o Clube fornece a possibilidade do Myber escolher produtos que quer
                            receber a cada mês. Cada item possui uma pontuação diferente. Use sua estratégia para
                            escolher e montar a sua box.
                        </p>

                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection