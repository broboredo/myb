@extends('layouts.app')

@yield('styles')

@section('content')

<div class="informacoes">
    <section id="banner">
        <div class="container">
            <h1>Informações Gerais</h1>
        </div>
    </section>

    <section id="content" class="pre-section">
        <div class="container">

            <div class="info-menu">

                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('questions') }}">
                                <img src="{{ asset('img/question.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('terms') }}">
                                <img src="{{ asset('img/terms_active.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('returns') }}">
                                <img src="{{ asset('img/returns.png') }}" class="img-responsive" />
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a href="{{ route('privacy') }}">
                                <img src="{{ asset('img/privacy.png') }}" class="img-responsive" />
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <h2>Termos de Uso</h2>
        </div>
    </section>

    <section id="texto">
        <div class="container">

            <div class="col-lg-12 col-md-12 col-sm-12">

				<strong class="green">1 - DA ASSINATURA</strong>
				<p>1.1 - A assinatura da Make Your Box consiste no recebimento mensal de 1 (uma) box, com produtos escolhidos pelo assinante, e de benefícios fornecidos por parte de empresas parceiras; <br />
				1.2 - A ativação, o envio da box e a participação no clube de benefícios está atrelado diretamente a aprovação do pagamento do pedido;</p>
				
				<br />
				
				<strong class="green">2 - DOS PLANOS</strong>
				<p>2.1 - O assinante terá duas opções de planos disponíveis; <br />
				2.2 - Na assinatura mensal, o assinante é cobrado, automaticamente, todos os meses no dia exato em que foi efetivado a assinatura; <br />
				2.3 - Na assinatura anual, o pagamento é realizado a cada doze meses, concedendo um desconto de 7% do valor do plano mensal;</p>
				
				<strong class="green">3 - DO PAGAMENTO</strong>
				<p>3.1 - O pagamento da assinatura é feito exclusivamente através de cartões de crédito das bandeiras VISA e MASTERCARD; <br />
				3.2 - Ao assinar a Make Your Box o assinante concorda em ser cobrado automaticamente de acordo com a periodicidade do plano escolhido; <br />
				3.3 - Para que o pedido seja aprovado é necessário que todos os dados do cartão tenham sido preenchidos corretamente e o assinante possua limite disponível no cartão de crédito fornecido na data escolhida para cobrança; <br />
				3.4 - Caso o assinante tenha qualquer tipo de problema com o cartão disponibilizado, a troca ou atualização dos dados deve ser realizada diretamente em sua conta, acessada através da página da Make Your Box; <br />
				3.5 - O envio do produto só é garantido após a aprovação do pagamento;</p>
				
				<strong class="green">4 - DO PRODUTO</strong>
				<p>4.1 - A Make Your Box é um clube de assinatura, onde o usuário recebe uma caixa, mensalmente, com produtos de sua escolha, dentre as opções fornecidas na Vitrine de Produtos na página da Make Your Box; <br />
				4.2 - Excepcionalmente alguns produtos da Make Your Box, podem ser disponibilizados para venda em algumas lojas parceiras do clube; <br />
				4.3 - Todos os itens utilizados para compor a Vitrine de Produtos são disponibilizados em quantidades limitadas; </p> 
				
				<strong class="green">5 - DA TROCA DE PRODUTOS</strong>
				<p>5.1 - Qualquer produto com defeito de fabricação ou danificado durante o processo de entrega pode ser trocado; <br />
				5.2 - Para solicitar a troca do produto, o assinante deve enviar um e-mail para <a href="mailto:resolvemeuproblema@makeyourbox.com.br">resolvemeuproblema@makeyourbox.com.br</a> explicando o problema e anexando fotos que evidenciem o dano do produto; <br />
				5.2.1 - A solicitação será analisada e, em caso de defeito comprovado, será dado início ao processo de troca; <br />
				5.2.3 - O envio do novo produto será feito após o recebimento do danificado; <br />
				5.2.3.1 - O novo produto será enviado juntamente à próxima box fechada pelo assinante; <br />
				5.2.3.1.1 - Caso o assinante não venha a dar prosseguimento à sua assinatura, o novo produto será enviado o mais breve possível; <br />
				5.2.4 - Caso a Make Your Box não obtenha em seu estoque o produto danificado, a troca será revertida em coins; </p>
				
				
				<strong class="green">6 - DO PREENCHIMENTO/FORNECIMENTO DOS DADOS NECESSÁRIOS PARA A ASSINATURA</strong>
				<p>6.1 - O assinante se compromete a preencher corretamente todos os dados pessoais requeridos para a realização da assinatura, bem como o correto endereço de entrega da box; <br />
				6.2 - Confira atentamente os dados de seu pedido (plano e tamanho) antes da confirmação de sua compra. Não serão permitidos cancelamentos, trocas ou devoluções fora das condições aqui definidas; <br />
				6.3 - Todos os dados do assinante são mantidos em segurança, revise a nossa <a href="{{ route('privacy') }}">Política de Privacidade</a>; </p>
				
				<strong class="green">7 - DO PROGRAMA DE FIDELIDADE</strong>
				<p>7.1 – O programa de fidelidade é válido, somente, para os assinantes do Plano Insane Mode; <br />
				7.2 – Após o período de seis e doze meses de assinatura, o assinante ganhará um produto de escolha da Make Your Box; <br />
				7.3 – O produto só será concedido para o assinante que permanecer durante o período exigido de forma ininterrupta. Ou seja, se o assinante cancelar sua assinatura e voltar, em seguida, independentemente do tempo de ausência, sua contagem será reiniciada; </p>
				
				<strong class="green">8 - DO ENVIO</strong>
				<p>8.1 - Os envios são realizados sempre após o assinante concluir a escolha dos itens que gostaria de receber; <br />
				8.1.1 - Alguns produtos podem aumentar o tempo para envio da box. A informação está presente nos detalhes de cada produto; <br />
				8.1.2 - No caso de escolha, pelo cliente, de produto que exija alteração do prazo de entrega, deverá dar ciência acerca da dilação do prazo informado no site; <br />
				8.1.3 - Para o envio ser efetivado, é necessário que o assinante escolha seus itens dentro do mês; <br />
				8.2 - A Make Your Box se reserva o direito de escolher a empresa de transportes que ofereça o melhor serviço de entregas; <br />
				8.3 - Em caso de extravio/roubo comprovado durante o processo de entrega, a Make Your Box garante o envio de um novo produto de acordo com a disponibilidade ou a devolução de coins no valor integral pago pela unidade em questão; </p>
				
				<strong class="green">9 - DO FRETE</strong>
				<p>9.1 - Os fretes estão inclusos nos valores dos planos; <br />
				9.2 - Em caso de devolução da box por motivos de ausência, desistência do produto, mudança de endereço ou endereço incorreto/incompleto, o assinante fica ciente que o reenvio só acontecerá mediante ao pagamento de um novo frete; <br />
				9.2.1 - O valor do frete referente à região do assinante deverá ser transferido ou depositado em conta corrente disponibilizada pelo atendimento ao cliente; </p>
				
				<strong class="green">10 - DO CANCELAMENTO</strong>
				<p>10.1 - Você pode cancelar sua assinatura MYB quando quiser e continuará a ter acesso ao serviço até o fim do período mensal de 
				faturamento. Na medida permitida pelas leis aplicáveis, os pagamentos não são reembolsáveis e a Make Your Box não 
				oferece reembolsos ou créditos por períodos de assinatura utilizados parcialmente ou por benefícios não utilizados. 
				Para cancelar, acesse a página "Meu perfil" e siga as instruções de cancelamento. Se você cancelar sua assinatura, sua conta será 
				automaticamente fechada ao fim do período de cobrança em andamento; <br />
				10.2 - Em caso de suspeita de fraude, a Make Your Box pode cancelar automaticamente a assinatura e/ou pedidos, sem aviso prévio; <br />
				10.3 - A devolução injustificada dos produtos em três vezes consecutivas, acarretará no cancelamento da assinatura.</p>
				
				<strong class="green">11 - DA ALTERAÇÃO DOS TERMOS DE USO</strong>
				<p>11.1 - A Make Your Box reserva-se ao direito de, a seu critério e a qualquer tempo, modificar, adicionar ou remover quaisquer cláusulas ou condições deste Termo de Uso, comunicando ao assinante por correio eletrônico (e-mail) ou via postal.</p>
				
				<strong class="green">12 - DO ACEITE DO PRESENTE TERMO</strong>
				<p>12.1 - O assinante declara expressamente que leu, compreendeu, e aceitou estes termos de condições e que está totalmente ciente dos direitos e obrigações que deles emanam.</p>
            </div>
        </div>
    </section>
</div>

@endsection