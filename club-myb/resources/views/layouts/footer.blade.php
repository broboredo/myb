<!-- Footer -->
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="col-lg-3 col-md-3 col-sm-12">
					<ul class="media">
						<li><a href="https://www.facebook.com/makeyourbox/" title="Facebook" target="_blank" class="media button"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://www.instagram.com/_makeyourbox/" title="Instagram" target="_blank" class="media button"><i class="fa fa-instagram"></i></a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4">
					<ul>
						<h4>Formas de Pagamento</h4>
						<img src="{{ asset('img/visa.png') }}" alt="Cartão Visa para pagamento" />
						<img src="{{ asset('img/mastercard.png') }}" alt="Cartão Master Card para pagamento" />
					</ul>
				</div>
				<div class="col-g-3 col-md-3 col-sm-4">
					<ul>
						<h4>Formas de Entrega</h4>
						<img src="{{ asset('img/pac.png') }}" alt="PAC para forma de envio" />
					</ul>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4">
					<ul>
						<h4>Segurança</h4>
						<img src="{{ asset('img/rapidssl.png') }}" alt="RapidSSL" />
					</ul>
				</div>
			</div>
		</div>

		<hr />

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="col-lg-3 col-md-3 col-sm-4">
					<ul class="logo">
						<h4><img src="{{ asset('img/logo_footer.png') }}" class="logo" /></h4>
						<li>CNPJ: 26.848.301/0001-63</li>
						<li><a href="mailto:falecomigo@makeyourbox.com.br">falecomigo@makeyourbox.com.br</a></li>
					</ul>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4">
					<ul>
						<h4>O Clube</h4>
						<li><a href="{{ route('about') }}">Sobre Nós</a></li>
						{{--<li><a href="">Nossos Planos</a></li>--}}
						<li><a href="{{ route('profile') }}">Minha Conta</a></li>
						<li><a href="/register">Cadastro</a></li>
						<li><a href="{{ route('friends') }}">Nossos Aliados</a></li>
						<li><a href="{{ route('contact') }}">Fale com a Gente</a></li>
					</ul>
				</div>
				<div class="col-g-3 col-md-3 col-sm-4">
					<ul>
						<h4>Informações Gerais</h4>
						<li><a href="{{ route('questions') }}">Perguntas Frequentes</a></li>
						<li><a href="{{ route('terms') }}">Termos de Uso</a></li>
						<li><a href="{{ route('returns') }}">Trocas e Devoluções</a></li>
						<li><a href="{{ route('privacy') }}">Política de Privacidade</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="container-fluid">
		<ul class="copyright">
			Copyright &copy; {{ date('Y') }} - Todos os direitos reservados
		</ul>
	</div>
</footer>