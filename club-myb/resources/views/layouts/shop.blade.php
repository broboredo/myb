<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">

    <link rel="icon" type="image/x-icon" href="{{ asset('img/favico.ico') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favico.ico') }}">
    
    <title>Make Your Box | Vitrine</title>

    <link href="/css/sweetalert.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}" rel="stylesheet">
    @include('layouts.styles')

    <!-- Global Spark Object -->
    <script>
        window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(), []
        )); ?>;
    </script>

</head>
<body>


    @if(!App::isLocal())
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-93913966-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
	@endif
	
	<div id="spark-app" v-cloak>
		<div class="container-fluid">
		
	    	@include('layouts.header')

            <section id="banner">
                <div class="container">
                    <h1>Vitrine de Produtos</h1>
                </div>
            </section>

            <section class="pre-section">
                <div class="container text-center">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">
                        Bem vindo à <div class="yellow dib">Vitrine de Produtos</div> <br />
                        Aqui você verá todas as opções de produtos para que possa fazer a sua escolha. <br />
                        Mas não deixa para depois. Os outros Mybers verão os mesmos itens.
                    </div>
                </div>
            </section>

            <section id="vitrine-menu">
                <ul class="text-center">
                    <li><a href="{{ route('profile') }}">Meu Perfil</a></li>
                    <li><a href="{{ route('plan-details') }}">Detalhes do Plano</a></li>
                    <li><a href="{{ route('my-box') }}">Minha Box</a></li>
                    <li><i class="fa fa-dolar"></i> <i class="coins"></i> <div coins></div> coins disponívels</li>
                    <li><a href="{{ route('checkout') }}" class="yellow">Fechar Box</a></li>
                </ul>
            </section>


            @if(!isset($products))
                <section id="vitrine-filter">
                    <a class="go-to-vitrine" href="{{ route('vitrine') }}">Voltar para a Vitrine</a>
                </section>
            @endif

            <section id="vitrine">
                <div class="container-fluid">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        @yield('content')
                    </div>
                </div>
            </section>

			@include('layouts.footer')

            <!-- Application Level Modals -->
            @if (Auth::check())
                @include('spark::modals.notifications')
                @include('spark::modals.support')
                @include('spark::modals.session-expired')
            @endif
		</div>
    </div>
    
    <!-- Scripts -->
    @include('layouts.scripts')
    <script src="{{ asset('js/vitrine.js') }}" type="text/javascript"></script>

    <script src="/js/app.js"></script>
    <script src="/js/sweetalert.min.js"></script>
</body>
</html>
