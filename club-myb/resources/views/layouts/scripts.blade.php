<!--[if lte IE 8]><script src="{{ asset('js/respond.min.js') }}"></script><![endif]-->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/typed.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/plugins.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('js/wow.min.js') }}"></script>
<script src="{{ asset('js/noty.min.js') }}"></script>
<script src="{{ asset('js/jquery.mask.js') }}"></script>

@if($view_name != "welcome")
    <script>
        $(document).ready(function() {
            $('section#banner').css('min-height', '150px');
        });
    </script>
@endif

@yield('scripts', '')