<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">

    <link rel="icon" type="image/x-icon" href="{{ asset('img/favico.ico') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favico.ico') }}">
    
    <title>Make Your Box | Perfil Myber</title>

    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @include('layouts.styles')
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}" rel="stylesheet" />

    <!-- Global Spark Object -->
    <script>
        window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(), []
        )); ?>;
    </script>

</head>
<body>


    @if(!App::isLocal())
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-93913966-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
	@endif

	<div id="spark-app" v-cloak>
		<div class="container-fluid">

	    	@include('layouts.header')

            <section id="banner">
                <div class="container">
                    <h1>{{ Auth::user()->name }}</h1>
                </div>
            </section>

            <section id="perfil">
                <div class="container">
                    <div class="col-lg-4 col-md-4 col-sm-3 profile-menu">
                        @include('profile.menu')
                    </div>

                    <div class="col-lg-8 col-md-8 col-sm-9">
                        {{--<div class="col-md-12 col-lg-12 col-sm-12 breadcrumb">

                        </div>--}}

                        @if(Session::has('alert-message'))
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="alert alert-{{ Session::get('alert') }}">
                                    <i class="fa fa-4x fa-trophy"></i>
                                    <div>
                                        {{ Session::get('alert-message') }}
                                    </div>
                                </div>
                            </div>
                        @endif

                        @yield('content')
                    </div>

                </div>
            </section>

			@include('layouts.footer')

            <!-- Application Level Modals -->
            @if (Auth::check())
                @include('spark::modals.notifications')
                @include('spark::modals.support')
                @include('spark::modals.session-expired')
            @endif
		</div>
    </div>
    
    <!-- Scripts -->
    @include('layouts.scripts')
    <script src="{{ asset('js/vitrine.js') }}" type="text/javascript"></script>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
</body>
</html>
