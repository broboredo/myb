<!-- Menu -->
<nav id="menu" class="hidden">
	<ul class="links">
		<li><a href="index.html">Home</a></li>
		<li><a href="landing.html">Landing</a></li>
		<li><a href="generic.html">Generic</a></li>
		<li><a href="elements.html">Elements</a></li>
	</ul>
	
	<img alt="Logo Make Your Box" src="img/logo_header.png" />
					
	<ul class="actions vertical">
		<li><a href="#" class="button special fit">Get Started</a></li>
		<li><a href="#" class="button fit">Log In</a></li>
	</ul>
</nav>