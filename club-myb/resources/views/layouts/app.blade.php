<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Decepcionado com sua mystery box? Pense fora da caixa, faça parte desse Império.">
    <meta name="keywords" content="mystery box, loot, box, make your box, nerdloot, nerd, geek, stranger things, game of thrones, the walking dead"/>
    <meta name="author" content="falecomigo@makeyourbox.com.br">
    
    <link rel="icon" type="image/x-icon" href="{{ asset('img/favico.ico') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favico.ico') }}">

    @if(!App::isLocal())
	    <script type="text/javascript">
	        window.smartlook||(function(d) {
	        var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
	        var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
	        c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
	        })(document);
	        smartlook('init', '82192c63cdf5580c15716ebecaf045227bcbe48b');
	    </script>
	@endif
    
    <title>Make Your Box</title>

    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @include('layouts.styles')

    <!-- Global Spark Object -->
    <script>
        window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(), []
        )); ?>;
    </script>

</head>
<body>

    @if(!App::isLocal())
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-93894674-1', 'auto');
		  ga('send', 'pageview');
		</script>
	@endif
    
    <div id="spark-app" v-cloak>
		<div class="container-fluid">
		
	    	@include('layouts.header')
	    
			@yield('content')
	    
			@include('layouts.footer')

            <!-- Application Level Modals -->
            @if (Auth::check())
                @include('spark::modals.notifications')
                @include('spark::modals.support')
                @include('spark::modals.session-expired')
            @endif
		</div>
    </div>
    
    <!-- Scripts -->
    @include('layouts.scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
</body>
</html>
