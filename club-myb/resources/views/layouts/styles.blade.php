<!-- CSS -->
<link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}" />
<link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}" />

<link rel="stylesheet" href="{{ asset('css/animate.min.css') }}" rel="stylesheet" />

<link rel="stylesheet" href="{{ asset('css/style.css') }}" rel="stylesheet" />


<!--[if lte IE 9]><link rel="stylesheet" href="{{ asset('css/ie9.css') }}" /><![endif]-->
<!--[if lte IE 8]><link rel="stylesheet" href="{{ asset('css/ie8.css') }}" /><![endif]-->