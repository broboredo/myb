<!-- Header -->
<header>
	<div class="container-fluid text-center">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="row">
				
				@if($view_name === "preview")
					<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-xs-4">
						<a href="#" class="logo">
							<img alt="Logo Make Your Box" class="img-responsive" src="{{ asset('img/logo_header.png') }}" />
						</a>
					</div>
				@else
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<nav class="pull-left">
							<a href="#menu" class="box-shadow-menu">Menu</a>
						</nav>
						
						<div class="clearfix"></div>
						
						<nav class="menu-principal">
							<ul>
								@if (!Auth::check())
									<li class="visible-xs visible-sm"><a href="{{ route('register') }}">Assinar</a></li>
								@endif
								<li><a href="{{ route('home') }}">Página Inicial</a></li>
								<li><a href="{{ route('about') }}">Sobre Nós</a></li>
								{{--<li><a href="">Nossos Planos</a></li>--}}
								<li><a href="{{ route('profile') }}">Minha Conta</a></li>
								<li><a href="/register">Cadastro</a></li>
								<li><a href="{{ route('friends') }}">Nossos Aliados</a></li>
								<li><a href="{{ route('contact') }}">Fale com a Gente</a></li>
								@if (!Auth::check())
									<li class="visible-xs visible-sm"><a href="{{ route('login') }}">Entrar</a></li>
								@else
									<li class="logout">
				                        <a href="/logout" class="red bold">Sair</a>
				                    </li>
								@endif
							</ul>
						</nav>
					</div>
					
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<a href="{{ route('home') }}" class="logo">
							<img alt="Logo Make Your Box" class="img-responsive" src="{{ asset('img/logo_header.png') }}" />
						</a>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						@if (!Auth::check())
							<div class="pull-right caps links visible-lg visible-md">
								Já é um MYBER?
								<a href="{{ route('login') }}">{{ trans('welcome.login') }}</a>
								<strong>/</strong>
								<a href="{{ route('register') }}">{{ trans('welcome.subscribe') }}</a>
							</div>
							<div class="caps links visible-xs visible-sm">
								<a href="{{ route('login') }}">{{ trans('welcome.login') }}</a>
							</div>
						@else
                            @include('profile.dropdown')
						@endif
					</div>
				@endif
					
			</div>				
		</div>
	</div>
	
	@include('layouts.menu')
</header>