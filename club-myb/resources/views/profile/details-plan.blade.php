@extends('layouts.profile')

@section('content')
    <div class="col-md-12 col-lg-12 col-sm-12 details-plan">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 the-plan">
                @if(!Auth::user()->isInsane())
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <img alt="Brasão Plano Survival" src="{{ asset('img/brasao_survivor.png') }}" class="img-responsive" />
                    </div>

                    <div class="col-lg-9 col-md-9 col-sm-6">
                        <h4>Survival</h4>
                        <h5>Mode</h5>
                        <h6>R$ 85.90/mensal</h6>
                        <div class="text">
                            Plano @if(Auth::user()->isPlanYearly()) Anual @else Mensal @endif <br />
                            Quantidade de Coins Mensais = 30 coins
                        </div>
                    </div>
                @else
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <img alt="Brasão Plano Survival" src="{{ asset('img/brasao_insane.png') }}" class="img-responsive" />
                    </div>

                    <div class="col-lg-9 col-md-9 col-sm-6">
                        <h4>Insane</h4>
                        <h5>Mode</h5>
                        <h6>R$ 169.90/mensal</h6>
                        <div class="text">
                            Plano @if(Auth::user()->isPlanYearly()) Anual @else Mensal @endif <br />
                            Quantidade de Coins Mensais = 75 coins
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-lg-12 col-sm-12 col-md-12 p-b-lg p-t-lg">Veja o que nossos aliados oferecem pra você</div>

        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6 block-partner p-b-lg">
                <div class="col-lg-4 col-md-4 col-sm-4 block-partner-img" style="margin-top: 10px">
                    <img src="{{ asset('img/partners/geeks.png') }}" class="img-responsive">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    10% de desconto em qualquer produto no site <br />
                    Cupom: MAKEYOURBOX<br />
                    Válido até: 01/09/2017*<br />
                    <a href="http://www.geeks.etc.br/" target="_blank">geeks.etc.br</a>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 block-partner p-b-lg">
                <div class="col-lg-4 col-md-4 col-sm-4 block-partner-img" style="margin-top: 25px">
                    <img src="{{ asset('img/partners/geek10.png') }}" class="img-responsive">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    10% de desconto em qualquer produto no site <br />
                    Cupom: #MAKEYOURBOX<br />
                    Válido até: 28/02/2018*<br />
                    <a href="http://www.geek10.com.br" target="_blank">geek10.com.br</a>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 block-partner p-b-lg">
                <div class="col-lg-4 col-md-4 col-sm-4 block-partner-img" style="margin-top: 30px;">
                    <img src="{{ asset('img/partners/pixtee.png') }}" class="img-responsive">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    10% de desconto em qualquer produto no site <br />
                    Cupom: MYB2365A12 <br />
                    Válido até: 28/02/2018*<br />
                    <a href="http://www.pixtee.com.br" target="_blank">pixtee.com.br</a>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 block-partner p-b-lg">
                <div class="col-lg-4 col-md-4 col-sm-4 block-partner-img" style="margin-top: 30px;">
                    <img src="{{ asset('img/partners/postergeek.png') }}" class="img-responsive">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    10% de desconto em qualquer produto no site <br />
                    Cupom: MYB<br />
                    Válido até: 28/02/2018*<br />
                    <a href="http://www.postergeek.com.br" target="_blank">postergeek.com.br</a>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 block-partner p-b-lg">
                <div class="col-lg-4 col-md-4 col-sm-4 block-partner-img" style="margin-top: 15px;">
                    <img src="{{ asset('img/partners/touts.png') }}" class="img-responsive">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    @if(Auth::user()->isInsane())
                        15% de desconto em compras acima de R$150,00 no site <br />
                    	Cupom: #MYBINSN<br />
                    @else
                        15% de desconto na primeira compra no site <br />
                    	Cupom: #MYBSRVVL<br />
                    @endif
                    Válido até: 06/06/2017*<br />
                    <a href="http://www.touts.com.br" target="_blank">touts.com.br</a>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12">
                *Os contratos podem ser alterados ou cancelados sem aviso prévio
            </div>

        </div>
@endsection