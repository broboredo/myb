@extends('layouts.profile')

@section('content')


    <div class="col-md-12 col-lg-12 col-sm-12 profile-pre-text">

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="title-my-box">
                <i class="myb-box-gg" style="margin-right: 20px;"></i>
                Minha Box |
                @if(Auth::user()->purchased)
                    {{ $status }}
                @endif
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12">
            @if(count($items) > 0 and !Auth::user()->purchased)
                <p>Os itens que estão aqui estão seguros por algumas horas, então é melhor fechar logo essa box!</p>
            @else
                <p>Corre lá na <a href="{{ route('vitrine') }}">Vitrine</a> e enche logo essa box!</p>
            @endif
        </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12">


        @if(!Auth::user()->purchased)
		
			@if(Auth::user()->sobDemanda())
				<div class="row">
			        <div class="col-lg-12 col-md-12 col-sm-12">
			           <p>A sua box contém itens que são feitos sob-demanda.</p>
			        </div>
				</div>
			@endif



            {{ csrf_field() }}

            <div class="row">
                @if(count($items) > 0)
                    @foreach($items as $item)
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="my-item">

                                <div class="product-value">
                                    <i class="coins"></i> {{ $item->product()->first()->coin_value }} coins
                                </div>

                                <div class="product-image text-center">
                                    <img src="{{ asset('storage/products') }}/{{ $item->product()->first()->img }}" style="height: 100%"  />
                                    <a href="{{ route('remove-product-cart-mybox', ['id' => $item->product()->first()->id]) }}">
                                        <label type="button" class="btn btn-danger btn-sm btn-remove">
                                            <i class="fa fa-times"></i>
                                        </label>
                                    </a>
                                </div>

                                <div class="product-info">
                                    <h5>{{ $item->product()->first()->name }}</h5>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <a href="{{ route('vitrine') }}" class="hold-add-item">
                        <div class="add-item">
                            [+] Adicione um item
                        </div>
                    </a>
                </div>
            </div>


            <div class="col-lg-12 col-sm-12 col-md-12">

                <div class="form-group pull-left">
                    TOTAL: {{ Auth::user()->myCartTotal() }} coins
                </div>

                @if(count($items) > 0)
                    <div class="form-group pull-right">
                        <a href="{{ route('remove-all') }}">
                            <button class="btn btn-white">
                                Limpar Box
                            </button>
                        </a>
                        <a href="{{ route('checkout') }}">
                            <button class="btn">
                                Fechar Box
                            </button>
                        </a>
                    </div>
                @endif

            </div>

        @else

            <h3>STOP MAN!</h3>
            <h5>Você já montou sua box desse tema, agora é aguardar o próximo.</h5>

        @endif

    </div>


@endsection
