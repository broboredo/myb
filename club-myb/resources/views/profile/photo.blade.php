<spark-update-profile-photo :user="user" inline-template>
        <div class="photo-user" v-if="user">
            <form role="form">
                <!-- Photo Preview-->
                <div class="alert alert-danger" v-if="form.errors.has('photo')">
                    @{{ form.errors.get('photo') }}
                </div>

                <img :src="user.photo_url" class="img-responsive">

                <label type="button" class="btn btn-primary btn-sm btn-upload" :disabled="form.busy">
                    <i class="fa fa-upload"></i>

                    <input ref="photo" type="file" class="form-control" name="photo" @change="update">
                </label>
            </form>
        </div>
</spark-update-profile-photo>
