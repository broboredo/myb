@extends('layouts.profile')

@section('content')
<div class="change-plan">
    <spark-settings :user="user" :teams="teams" inline-template>
        <spark-subscription :user="user" :team="team" :billable-type="billableType" inline-template>
            <div>
                <div v-if="plans.length > 0">

                    <!-- Update Subscription -->
                    <div v-if="subscriptionIsActive">
                        @include('profile.change-plan-plans')
                    </div>

                    <!-- Resume Subscription -->
                    <div v-else>
                        @include('profile.choose-plan')
                    </div>

                <!-- Plan Features Modal -->
                @include('spark::modals.plan-details')
            </div>
        </spark-subscription>
    </spark-settings>
</div>
@endsection
