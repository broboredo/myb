<spark-resume-subscription :user="user" :team="team"
                           :plans="plans" :billable-type="billableType" inline-template>

    <div class="row">


        <div class="col-md-12 col-lg-12 col-sm-12 profile-pre-text">

            <div class="col-lg-12 col-md-12 col-sm-12">
                <!-- Plan Error Message - In General Will Never Be Shown -->
                <div class="alert alert-danger" v-if="planForm.errors.has('plan')">
                    @{{ planForm.errors.get('plan') }}
                </div>

                <!-- Cancellation Information -->
                <div class="p-b-lg">
                    Você cancelou sua assinatura quando era um Myber <strong>@{{ activePlan.name }}</strong> (<span v-if="activePlan.interval == 'monthly'">Mensal</span><span v-else>Anual</span>).
                </div>

                <div class="p-b-lg">
                    Os benefícios de sua assinatura continuarão até que seu período de faturamento atual termine em
                    <strong>{{ Carbon\Carbon::parse(Auth::user()->subscription()->first()->ends_at)->addMonth(1)->format('d/m/y') }}</strong>.
                    Você pode retomar sua assinatura sem nenhum custo extra até o final do período de faturamento.
                </div>

                <div class="p-b-lg">
                    Volte a ser um MYBER!
                </div>
            </div>
        </div>


        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="row">
                <!-- Planos Survival -->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <img alt="Brasão Plano Survival" src="{{ asset('img/brasao_survivor.png') }}" class="img-responsive" />
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h4>Survival</h4>
                        <h5>Mode</h5>
                        <a class="yellow" @click="showPlanDetails(paidPlans[2])">Saiba Mais</a>

                        <div class="choose-plan">
                            <div v-for="plan in paidPlans">
                                    <span v-if="plan.name == 'Survival'">
                                        <div class="control-group">
                                            <label class="control control--radio">
                                                <input type="radio" name="planChoose"
                                                v-bind:class="{'btn-warning-outline': ! isActivePlan(plan), 'btn-warning': isActivePlan(plan)}"
                                                @click="selectSubscription(plan)"
                                                :disabled="selectingPlan">

                                                <div class="control__indicator"></div>
                                                <span v-if="plan.interval == 'monthly'">
                                                    <div class="text">Mensal | <span>R$ @{{ plan.price }}</span></div>

                                                </span>
                                                <span v-else>
                                                    <div class="text">Anual | <span>R$ @{{ plan.price }}</span></div>
                                                </span>
                                            </label>
                                        </div>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Planos Insane -->
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <img alt="Brasão Plano Insane" src="{{ asset('img/brasao_insane.png') }}" class="img-responsive" />
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h4>Insane</h4>
                        <h5>Mode</h5>
                        <a class="yellow" @click="showPlanDetails(paidPlans[0])">Saiba Mais</a>

                        <div class="choose-plan">
                            <div v-for="plan in paidPlans">
                                    <span v-if="plan.name == 'Insane'">
                                        <div class="control-group">
                                            <label class="control control--radio">
                                                <input type="radio" name="planChoose"
                                               v-bind:class="{'btn-warning-outline': ! isActivePlan(plan), 'btn-warning': isActivePlan(plan)}"
                                                @click="selectSubscription(plan)"
                                                :disabled="selectingPlan">

                                                <div class="control__indicator"></div>
                                                <span v-if="plan.interval == 'monthly'">
                                                    <div class="text">Mensal | <span>R$ @{{ plan.price }}</span></div>

                                                </span>
                                                <span v-else>
                                                    <div class="text">Anual | <span>R$ @{{ plan.price }}</span></div>
                                                </span>
                                            </label>
                                        </div>
                                    </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-lg-12 col-sm-12 p-t-lg">
            <div class="pull-right">
                <button class="btn" :class="selectingPlan" @click="updateSubscription(selectedPlan)">Quero voltar a ser um Myber</button>
            </div>
        </div>
    </div>

























        <div class="panel-body table-responsive">

            <!-- European VAT Notice -->
            @if (Spark::collectsEuropeanVat())
                <p class="p-b-lg">
                    All subscription plan prices include applicable VAT.
                </p>
            @endif

            <table class="table table-borderless m-b-none">
                <thead></thead>
                <tbody>
                <tr v-for="plan in paidPlansForActiveInterval">
                    <!-- Plan Name -->
                    <td>
                        <div class="btn-table-align" @click="showPlanDetails(plan)">
                        <span style="cursor: pointer;">
                                    <strong>@{{ plan.name }}</strong>
                                </span>
        </div>
        </td>

        <!-- Plan Features Button -->
        <td>
            <button class="btn btn-default m-l-sm" @click="showPlanDetails(plan)">
            <i class="fa fa-btn fa-star-o"></i>Plan Features
            </button>
        </td>

        <!-- Plan Price -->
        <td>
            <div class="btn-table-align">
                @{{ priceWithTax(plan) | currency }} / @{{ plan.interval | capitalize }}
            </div>
        </td>

        <!-- Plan Select Button -->
        <td class="text-right">
            <button class="btn btn-plan"
                    v-bind:class="{'btn-warning-outline': ! isActivePlan(plan), 'btn-warning': isActivePlan(plan)}"
            @click="updateSubscription(plan)"
            :disabled="selectingPlan">

            <span v-if="selectingPlan === plan">
                                    <i class="fa fa-btn fa-spinner fa-spin"></i>Resuming
                                </span>

            <span v-if="! isActivePlan(plan) && selectingPlan !== plan">
                                    Switch
                                </span>

            <span v-if="isActivePlan(plan) && selectingPlan !== plan">
                                    Resume
                                </span>
            </button>
        </td>
        </tr>
        </tbody>
        </table>
    </div>
</spark-resume-subscription>
