@extends('layouts.shop')

@section('content')
    <div class="product-page" data-id="{{ $product->id }}">
        <div class="row">


            @if(!isset($product))

                <h2>Epaaa! Esse produto não existe =(</h2>

            @else

                {{ csrf_field() }}

                <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12">



                    <div class="col-lg-1 col-lg-offset-0 col-md-1 col-md-offset-0 col-sm-12 visible-lg visible-md">
                        <div class="row">
                            <ul class="product-buttons pull-right" data-id="{{ $product->id }}">
                                <li><a href="{{ asset('storage/products') }}/{{ $product->img }}" target="_blank" title="Zoom na imagem" class="zoom"><i class="myb-lupe"></i></a></li>
                                <li class="disabled"><a title="Galeria de imagem" class="gallery"><i class="myb-gallery"></i></a></li>
                                <li class="love @if($product->productLike()) active @endif"><a title="Desejo esse produto" class="love" onclick="likeThisProduct(this); vitrine=false;"><i class="fa fa-heart-o"></i></a></li>
                                <li class="add @if($product->productInCart()) disabled @else active @endif"><a onclick="addProduct(this); vitrine=false;" title="Adicionar à minha box" class="add"><i class="myb-box"></i></a></li>
                                <li class="remove @if(!$product->productInCart()) disabled @endif"><a onclick="removeProduct(this); vitrine=false;" title="Remover da minha box" class="remove"><i class="myb-box-remove"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-5 col-lg-offset-0 col-md-5 col-md-offset-0 col-sm-8 col-sm-offset-2">

                        <div class="row product-image text-center">
                            <img src="{{ asset('storage/products') }}/{{ $product->img }}" class="img-responsive" />
                        </div>


                        <div class="row">
                            <div class="product-value">
                                <i class="coins"></i> {{ $product->coin_value }} coins
                            </div>
                        </div>


                    </div>


                    <div class="col-lg-6 col-lg-offset-0 col-md-6 col-md-offset-0 col-sm-8 col-sm-offset-2">

                        <h4>
                            {{ $product->name }}
                            @if(!is_null($product->origin) and !empty($product->origin))
                                <span>({{ $product->origin }})</span>
                            @endif
                        </h4>

                        <div class="product-details">

                            {!! $product->description !!}

                        </div>

                        <div class="buttons">
                            <a class="love @if($product->productLike()) active @endif" onclick="loveIt(this);" data-id="{{ $product->id }}"><i class="myb-like"></i> Desejo</a>
                            <a class="add @if($product->productInCart()) active @endif" data-id="{{ $product->id }}" onclick="productToggle(this);"><i class="myb-box-g"></i> Adicionar à Minha Box</a>
                        </div>
                    </div>

                @endif
        </div>
    </div>
@endsection