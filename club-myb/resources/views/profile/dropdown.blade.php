<spark-navbar
        :user="user"
        :teams="teams"
        :current-team="currentTeam"
        :has-unread-notifications="hasUnreadNotifications"
        :has-unread-announcements="hasUnreadAnnouncements"
        inline-template>

    <div v-if="user">
        {{--<div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <div class="hamburger">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#spark-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        </div>--}}

        <div class="collapse navbar-collapse" id="spark-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                @includeIf('spark::nav.user-left')
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                @includeIf('spark::nav.user-right')

                <!-- Notifications -->
                {{--<li>
                    <a @click="showNotifications" class="has-activity-indicator">
                        <div class="navbar-icon">
                            <i class="activity-indicator" v-if="hasUnreadNotifications || hasUnreadAnnouncements"></i>
                            <i class="icon fa fa-bell"></i>
                        </div>
                    </a>
                </li>--}}

                <li class="dropdown">
                    <!-- User Photo / Name -->
                    <a href="#" class="dropdown-toggle menu-user" data-toggle="dropdown" role="button" aria-expanded="false">
                        <img :src="user.photo_url" class="profile-photo">
                        <span class="caret"></span>
                        <div class="user-name">
                            Olá, <strong>{{ explode(" ", Auth::user()->name)[0] }}</strong>
                        </div>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        @if(Auth::user()->email === "makeyourbox1@gmail.com")
                            <li class="dropdown-header">Administradores</li>
                            <li>
                                <a href="{{ route('admin-product') }}">Admin</a>
                            </li>
                            <li class="divider"></li>
                        @endif
                        <li class="dropdown-header">Configurações</li>
                        <li>
                            <a href="{{ route('profile') }}">Perfil</a>
                        </li>
                        <li>
                            <a href="{{ route('my-box') }}">Minha Box</a>
                        </li>
                        <li>
                            <a href="{{ route('vitrine') }}">Vitrine</a>
                        </li>
                    </ul>

                    <li class="logout">
                        <a href="/logout" class="red bold">Sair</a>
                    </li>
                </li>
            </ul>
        </div>
    </div>
</spark-navbar>