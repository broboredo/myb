@extends('layouts.profile')

@section('content')
	 <div class="col-md-12 col-lg-12 col-sm-12 profile-pre-text">	
		<div class="col-lg-12 col-md-12 col-sm-12">
			<p>Seu tempo de assinatura vale muito! Queremos que você seja um eterno Myber</p>
			<p>Por isso a Make Your Box criou o Controle de Fidelidade. Onde nosso Myber pode ganhar um baita prêmio.</p>
        </div>
    </div>


    <div class="col-md-12 col-lg-12 col-sm-12">
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity active">
				<span>01</span>
			</div>
			<div class="label-month">Mês Um</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity">
				<span>02</span>
			</div>
			<div class="label-month">Mês Dois</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity">
				<span>03</span>
			</div>
			<div class="label-month">Mês Três</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity">
				<span>04</span>
			</div>
			<div class="label-month">Mês Quatro</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity">
				<span>05</span>
			</div>
			<div class="label-month">Mês Cinco</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity gift"></div>
			<div class="label-month">Mês Seis</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity">
				<span>07</span>
			</div>
			<div class="label-month">Mês Sete</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity">
				<span>08</span>
			</div>
			<div class="label-month">Mês Oito</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity">
				<span>09</span>
			</div>
			<div class="label-month">Mês Nove</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity">
				<span>10</span>
			</div>
			<div class="label-month">Mês Dez</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity">
				<span>11</span>
			</div>
			<div class="label-month">Mês Onze</div>
		</div>
		
    	<div class="col-lg-3 col-md-3 col-sm-4 month">
			<div class="control-fidelity gift"></div>
			<div class="label-month">Mês Doze</div>
		</div>
    </div>
@endsection
