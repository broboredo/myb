@extends('layouts.profile')


@section('scripts')
    <script src="https://js.stripe.com/v2/"></script>
@endsection

@section('content')
<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
<div class="col-md-12 col-ld-12 col-sm-12 profile-pre-text">
    <p class="ajude">Complete o seu cadastro e nos ajude a te dar uma melhor experiência em nossa plataforma.</p>
</div>

<ul class="nav nav-tabs" role="tablist" id="tabs">
	<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Nome e Email</a></li>
	<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Alterar Senha</a></li>
	<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Endereço e Cartão</a></li>
</ul>

<div class="tab-content">
	<div role="tabpanel" class="tab-pane active" id="home">
		<spark-settings :user="user" :teams="teams" inline-template>
		    <spark-update-contact-information :user="user" inline-template>
		        <div>
		            <!-- Success Message -->
		            <div class="alert alert-success" v-if="form.successful">
		                {{ trans('spark.profile.contact_information_has_been_updated') }}
		            </div>
		
		            <form class="form-horizontal" role="form">
		                
		                
		                
		                <!-- Name -->
		                <div class="form-group" :class="{'has-error': form.errors.has('name'),'has-error': form.errors.has('email')}">
		                    <div class="col-md-6">
		                        <label>{{ trans('spark.profile.name') }}</label>
		                        <input type="text" class="form-control" name="name" v-model="form.name">
		
		                        <span class="help-block" v-show="form.errors.has('name')">
		                            @{{ form.errors.get('name') }}
		                        </span>
		                    </div>
		
		                    <div class="col-md-6">
		                        <label>{{ trans('spark.profile.email_address') }}</label>
		                        <input type="email" class="form-control" name="email" v-model="form.email">
		
		                        <span class="help-block" v-show="form.errors.has('email')">
		                            @{{ form.errors.get('email') }}
		                        </span>
		                    </div>
		                </div>
		
		                <!-- Update Button -->
		                <div class="form-group">
		                    <div class="col-md-12">
		                        <button type="submit" class="btn btn-primary pull-right" @click.prevent="update" :disabled="form.busy">
		                            {{ trans('spark.profile.update') }}
		                        </button>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </spark-update-contact-information>
		</spark-settings>
	</div>
	
	<div role="tabpanel" class="tab-pane" id="profile">
		<spark-settings :user="user" :teams="teams" inline-template>
		    <spark-update-password inline-template>
		        <div>
		                <!-- Success Message -->
		            <div class="alert alert-success" v-if="form.successful">
		                {{ trans('spark.settings.security.update.password_updated') }}
		            </div>
		
		            <form class="form-horizontal" role="form">
		                <div class="form-group" :class="{'has-error': form.errors.has('current_password'), 'has-error': form.errors.has('password'), 'has-error': form.errors.has('password_confirmation')}">
		                    <!-- Current Password -->
		                    <div class="col-md-4">
		                        <label>{{ trans('spark.settings.security.update.current_password') }}</label>
		                        <input type="password" class="form-control" name="current_password" v-model="form.current_password">
		
		                        <span class="help-block" v-show="form.errors.has('current_password')">
		                            @{{ form.errors.get('current_password') }}
		                        </span>
		                    </div>
		
		                    <!-- New Password -->
		                    <div class="col-md-4">
		                        <label>{{ trans('spark.settings.security.update.password') }}</label>
		                        <input type="password" class="form-control" name="password" v-model="form.password">
		
		                        <span class="help-block" v-show="form.errors.has('password')">
		                            @{{ form.errors.get('password') }}
		                        </span>
		                    </div>
		
		                    <!-- Confirmation New Password -->
		                    <div class="col-md-4">
		                        <label>{{ trans('spark.settings.security.update.confirm_password') }}</label>
		                        <input type="password" class="form-control" name="password_confirmation" v-model="form.password_confirmation">
		
		                        <span class="help-block" v-show="form.errors.has('password_confirmation')">
		                            @{{ form.errors.get('password_confirmation') }}
		                        </span>
		                    </div>
		                </div>
		
		                <!-- Update Button -->
		                <div class="form-group">
		                    <div class="col-md-12">
		                        <button type="submit" class="btn btn-primary pull-right" @click.prevent="update" :disabled="form.busy">
		                            {{ trans('spark.settings.security.update.update') }}
		                        </button>
		                    </div>
		                </div>
		            </form>
		        </div>
		    </spark-update-password>
		</spark-settings>
	</div>
	
	<div role="tabpanel" class="tab-pane" id="messages">
		<spark-settings :user="user" :teams="teams" inline-template>
		    <spark-payment-method-stripe :user="user" :team="team" :billable-type="billableType" inline-template>
		        <div>
		            <!-- Current Discount -->
		            <div class="panel panel-success" v-if="currentDiscount">
		                <div class="panel-heading">{{ trans('spark.settings.payment_method.current_discount') }}</div>
		
		                <div class="panel-body">
		                    {{ trans('spark.settings.payment_method.curently_receive') }} @{{ formattedDiscount(currentDiscount) }}
		                    {{ trans('spark.settings.payment_method.for') }} @{{ formattedDiscountDuration(currentDiscount) }}.
		                </div>
		            </div>
		
		            <!-- Update Card -->
		            <spark-update-payment-method-stripe :user="user" :team="team" :billable-type="billableType" inline-template>
		                    <div>
		                        <!-- Card Update Success Message -->
		                        <div class="alert alert-success" v-if="form.successful">
		                            {{ trans('spark.settings.payment_method.update_payment_stripe.card_updated') }}
		                        </div>
		
		                        <!-- Generic 500 Level Error Message / Stripe Threw Exception -->
		                        <div class="alert alert-danger" v-if="form.errors.has('form')">
		                            {{ trans('spark.settings.payment_method.update_payment_stripe.alert') }}
		                        </div>
		
		                        <form class="form-horizontal" role="form">
		                            <!-- Billing Address Fields -->
		                            @if (Spark::collectsBillingAddress())
		                                <!-- Address -->
		                                <div class="form-group" :class="{'has-error': form.errors.has('address')}">
		
		                                    <div class="col-sm-6">
		                                        <label>{{ trans('spark.settings.payment_method.update_payment_address.address') }}</label>
		                                        <input type="text" class="form-control" v-model="form.address">
		
		                                        <span class="help-block" v-show="form.errors.has('address')">
		                                            @{{ form.errors.get('address') }}
		                                        </span>
		                                    </div>

											<!-- Address Line 2 -->
											<div class="col-sm-2">
												<label>Nº</label>
												<input type="text" class="form-control number" required="required" v-model="form.number">

												<span class="help-block" v-show="form.errors.has('number')">
		                                            @{{ form.errors.get('number') }}
		                                        </span>
											</div>
		
		                                    <!-- Address Line 2 -->
		                                    <div class="col-sm-4">
		                                        <label>{{ trans('spark.settings.payment_method.update_payment_address.address_2') }}</label>
		                                        <input type="text" class="form-control" v-model="form.address_line_2">
		
		                                        <span class="help-block" v-show="form.errors.has('address_line_2')">
		                                            @{{ form.errors.get('address_line_2') }}
		                                        </span>
		                                    </div>
		                                </div>
		
		                                <!-- State & ZIP Code -->
		                                <div class="form-group" :class="{'has-error': form.errors.has('state')}">
		                                    <!-- Zip Code -->
		                                    <div class="col-sm-6 col-lg-3 col-md-3">
		                                        <label>{{ trans('spark.settings.payment_method.update_payment_address.zip') }}</label>
		                                        <input type="text" class="form-control" v-model="form.zip" placeholder="Postal Code">
		
		                                        <span class="help-block" v-show="form.errors.has('zip')">
		                                            @{{ form.errors.get('zip') }}
		                                        </span>
		                                    </div>
		
		                                    <!-- State -->
		                                    <div class="col-sm-6 col-lg-4 col-md-4">
		                                        <label>Estado</label>
		                                        <input type="text" class="form-control" v-model="form.state" placeholder="State">
		
		                                        <span class="help-block" v-show="form.errors.has('state')">
		                                            @{{ form.errors.get('state') }}
		                                        </span>
		                                    </div>
		
		                                    <!-- City -->
		                                    <div class="col-sm-12 col-lg-5 col-md-5">
		                                        <label>{{ trans('spark.settings.payment_method.update_payment_address.city') }}</label>
		                                        <input type="text" class="form-control" v-model="form.city">
		
		                                        <span class="help-block" v-show="form.errors.has('city')">
		                                            @{{ form.errors.get('city') }}
		                                        </span>
		                                    </div>
		                                </div>
		
		
		                                <div class="cut-form">{{ trans('spark.settings.payment_method.update_payment_stripe.credit_card') }}</div>
		                            @endif
		
		                            <!-- Cardholder's Name -->
		                            <div class="form-group">
		                                <div class="col-md-12">
		                                    <label for="name">{{ trans('spark.settings.payment_method.update_payment_stripe.cardholders_name') }}</label>
		                                    <input type="text" class="form-control" v-model="cardForm.name">
		                                </div>
		                            </div>
		
		                            <!-- Card Number -->
		                            <div class="form-group" :class="{'has-error': cardForm.errors.has('number')}">
		                                <div class="col-md-6">
		                                    <label for="number">{{ trans('spark.settings.payment_method.update_payment_stripe.card_number') }}</label>
		                                    <input type="text"
		                                           class="form-control"
		                                           data-stripe="number"
		                                           :placeholder="placeholder"
		                                           v-model="cardForm.number">
		
		                                    <span class="help-block" v-show="cardForm.errors.has('number')">
		                                        @{{ cardForm.errors.get('number') }}
		                                    </span>
		                                </div>
		
		                                <!-- Security Code -->
		                                <div class="col-md-3">
		                                    <label for="cvc">{{ trans('spark.settings.payment_method.update_payment_stripe.security_code') }}</label>
		                                    <input type="text" class="form-control" data-stripe="cvc" v-model="cardForm.cvc">
		                                </div>
		
		                                <div class="col-md-3 col-lg-3 col-sm-6">
		                                    <!-- Expiration Information -->
		                                    <label>{{ trans('spark.settings.payment_method.update_payment_stripe.expiration') }}</label>
		                                    <div class="row">
		                                        <div class="col-md-5">
		                                            <!-- Month -->
		                                            <input type="text" class="form-control" placeholder="10" maxlength="2" data-stripe="exp-month" v-model="cardForm.month">
		                                        </div>
		
		                                        <div class="col-md-7">
		                                            <!-- Year -->
		                                            <input type="text" class="form-control" placeholder="2020" maxlength="4" data-stripe="exp-year" v-model="cardForm.year">
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		
		                            <!-- Update Button -->
		                            <div class="form-group">
		                                <div class="col-md-12 col-lg-12 col-sm-12">
		                                    <button type="submit" class="btn pull-right" @click.prevent="update" :disabled="form.busy">
		                                        <span v-if="form.busy">
		                                            <i class="fa fa-btn fa-spinner fa-spin"></i>{{ trans('spark.settings.payment_method.update_payment_stripe.updating') }}
		                                        </span>
		
		                                        <span v-else>
		                                            {{ trans('spark.settings.payment_method.update_payment_stripe.update') }}
		                                        </span>
		                                    </button>
		                                </div>
		                            </div>
		                        </form>
		                    </div>
		            </spark-update-payment-method-stripe>
		
		
		            <div class="hidden">
		                <div v-if="billable.stripe_id">
		                    <!-- Redeem Coupon -->
		                    <spark-redeem-coupon :user="user" :team="team" :billable-type="billableType" inline-template>
		                        <div>
		                            <div class="alert alert-success" v-if="form.successful">
		                                {{ trans('spark.settings.payment_method.redeem.coupon_accepted') }}
		                            </div>
		
		                            <form class="form-horizontal" role="form">
		                                <!-- Coupon Code -->
		                                <div class="form-group" :class="{'has-error': form.errors.has('coupon')}">
		
		                                    <div class="col-md-12">
		                                        <label>{{ trans('spark.settings.payment_method.redeem.coupon_code') }}</label>
		                                        <input type="text" class="form-control" name="coupon" v-model="form.coupon">
		
		                                        <span class="help-block" v-show="form.errors.has('coupon')">
		                                            @{{ form.errors.get('coupon') }}
		                                        </span>
		                                    </div>
		                                </div>
		
		                                <!-- Redeem Button -->
		                                <div class="form-group">
		                                    <div class="col-md-12 col-lg-12 col-sm-12">
		                                        <button type="submit" class="btn pull-right"
		                                                @click.prevent="redeem"
		                                                :disabled="form.busy">
		
		                                            <span v-if="form.busy">
		                                                <i class="fa fa-btn fa-spinner fa-spin"></i>{{ trans('spark.settings.payment_method.redeem.redeeming') }}
		                                            </span>
		
		                                            <span v-else>
		                                                {{ trans('spark.settings.payment_method.redeem.redeem') }}
		                                            </span>
		                                        </button>
		                                    </div>
		                                </div>
		                            </form>
		                        </div>
		                    </spark-redeem-coupon>
		                </div>
		            </div>
		        </div>
		    </spark-payment-method-stripe>
		</spark-settings>
	</div>
</div>
</div>
@endsection
