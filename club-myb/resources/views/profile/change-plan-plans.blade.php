<spark-update-subscription :user="user" :team="team"
                   :plans="plans" :billable-type="billableType" inline-template>
    <div>
        <div class="row">


            <div class="col-md-12 col-lg-12 col-sm-12 profile-pre-text">

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <!-- Plan Error Message - In General Will Never Be Shown -->
                    <div class="alert alert-danger" v-if="planForm.errors.has('plan')">
                        @{{ planForm.errors.get('plan') }}
                    </div>
                    
                    <div class="alert alert-success" v-if="success">
                        Assinatura Atualizada
                    </div>

                    <!-- Current Subscription (Active) -->
                    <div class="p-b-lg" v-if="activePlan.active">
                        <p>Você pode escolher mudar de plano ou continuar no que você está, com outras opções de pagamento.</p>
                        <p>
                            Atualmente você está inscrito no Plano
                            <strong>@{{ activePlan.name }}</strong> (<span v-if="activePlan.interval == 'monthly'">Mensal</span><span v-else>Anual</span>).
                        </p>
                    </div>

                    <!-- Current Subscription (Archived) -->
                    <div class="alert alert-warning m-b-lg" v-if=" ! activePlan.active">
                        Você está atualmente inscrito no plano
                        <strong>@{{ activePlan.name }}</strong> (<span v-if="activePlan.interval == 'monthly'">Mensal</span><span v-else>Anual</span>).
                        Este plano foi descontinuado, mas pode continuar a subscrição deste plano enquanto desejar.
                        Se você cancelar sua assinatura e desejar iniciar uma nova assinatura, você precisará escolher um dos planos ativos listados abaixo.
                    </div>
                </div>
            </div>


            <!-- Planos Survival -->
            <div class="col-lg-6 col-md-6 col-sm-12 block-plan">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <img alt="Brasão Plano Survival" src="{{ asset('img/brasao_survivor.png') }}" class="img-responsive" />
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4>Survival</h4>
                    <h5>Mode</h5>
                    <a class="yellow" @click="showPlanDetails(paidPlans[1])">Saiba Mais</a>

                    <div class="choose-plan">
                        <div v-for="plan in paidPlans">
                            <span v-if="plan.name == 'Survival'">
                                <div class="control-group">
                                    <label class="control control--radio">
                                        <input type="radio" name="planChoose" v-if="isActivePlan(plan)" disabled checked="checked">
                                        <input type="radio" name="planChoose" v-if=" ! isActivePlan(plan) && selectingPlan !== plan" @click="confirmPlanUpdate(plan)" :disabled="selectingPlan">
                                        <input type="radio" name="planChoose" v-if="selectingPlan && selectingPlan === plan" disabled>

                                        <div class="control__indicator"></div>
                                        <span v-if="plan.interval == 'monthly'">
                                            <div class="text">Mensal | <span>R$ @{{ plan.price }}</span></div>

                                        </span>
                                        <span v-else>
                                            <div class="text">Anual | <span>R$ @{{ plan.price }}</span></div>
                                        </span>
                                    </label>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Planos Insane -->
            <div class="col-lg-6 col-md-6 col-sm-12 block-plan">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <img alt="Brasão Plano Insane" src="{{ asset('img/brasao_insane.png') }}" class="img-responsive" />
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6">
                    <h4>Insane</h4>
                    <h5>Mode</h5>
                    <a class="yellow" @click="showPlanDetails(paidPlans[0])">Saiba Mais</a>

                    <div class="choose-plan">
                        <div v-for="plan in paidPlans">
                            <span v-if="plan.name == 'Insane'">
                                <div class="control-group">
                                    <label class="control control--radio">
                                        <input type="radio" name="planChoose" v-if="isActivePlan(plan)" disabled checked="checked">
                                        <input type="radio" name="planChoose" v-if=" ! isActivePlan(plan) && selectingPlan !== plan" @click="confirmPlanUpdate(plan)" :disabled="selectingPlan">
                                        <input type="radio" name="planChoose" v-if="selectingPlan && selectingPlan === plan" disabled>

                                        <div class="control__indicator"></div>
                                        <span v-if="plan.interval == 'monthly'">
                                            <div class="text">Mensal | <span>R$ @{{ plan.price }}</span></div>

                                        </span>
                                        <span v-else>
                                            <div class="text">Anual | <span>R$ @{{ plan.price }}</span></div>
                                        </span>
                                    </label>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Confirm Plan Update Modal -->
        <div class="modal fade" id="modal-confirm-plan-update" tabindex="-2" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content" v-if="confirmingPlan">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">
                            Trocar de Plano
                        </h4>
                    </div>

                    <!-- Modal Body -->
                    <div class="modal-body">
                        <p>
                            Você tem certeza que deseja alterar sua assinatura para o plano
                            <strong>@{{ confirmingPlan.name }}</strong> (<span v-if="confirmingPlan.interval == 'monthly'">Mensal</span><span v-else>Anual</span>) no valor de R$ @{{ confirmingPlan.price }}?
                        </p>
                    </div>

                    <!-- Modal Actions -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Não, agora não</button>
                        <button type="button" class="btn" @click="approvePlanUpdate">Sim, eu quero!</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</spark-update-subscription>