<div class="row">
    <div class="col-lg-5 col-md-5 col-sm-12">
        @include('profile.photo')
    </div>
    <div class="col-lg-7 col-md-7 col-sm-12">
        <h5>{{  Auth::user()->name }}</h5>
        <p>{{ Auth::user()->email }}</p>
        <p>CPF: {{ Auth::user()->cpf }}</p>
        <p>{{ Auth::user()->billing_address }}, {{ Auth::user()->billing_number }}</p>
        <p>CEP: {{ Auth::user()->billing_zip }}</p>
        <p>{{ Auth::user()->billing_neighborhood }} - {{ Auth::user()->billing_city }} - {{ Auth::user()->billing_state }}</p>
    </div>
    
    <div class="col-lg-12 col-md-12 col-sm-12">
    	<!-- MENU -->
        <ul class="user-coins">
            <i class="coins"></i> <div coins></div> coins
        </ul>


        <ul class="menu">
            <li><a href="{{ route('edit-profile') }}">Editar perfil</a></li>
            <li><a href="{{ route('plan-details') }}">Benefícios do Plano</a></li>
            <li><a href="{{ route('change-plan') }}">Trocar de Plano</a></li>
        </ul>


        <ul class="menu">
            <li><a href="{{ route('vitrine') }}">Vitrine de Produtos</a></li>
            <li><a href="{{ route('my-box') }}">Minha Box</a></li>
            <li><a href="{{ route('history') }}">Histórico de Boxes</a></li>
            @if(Auth::user()->isInsane())
                <li><a href="{{ route('fidelity') }}">Controle Fidelidade</a></li>
            @endif
            {{--<li><a href="">Minhas Conquistas</a><span>1</span></li>--}}{{--<span class="inverse">NOVO</span>--}}
        </ul>


        <ul class="cancel menu">
            <li><a href="{{ route('cancel') }}">Cancelar Assinatura</a></li>
        </ul>
    </div>
</div>