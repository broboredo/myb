@extends('layouts.profile')

@section('content')

    @if(Session::has('message'))
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="alert alert-danger">
                {{ Session::get('message') }}
            </div>
        </div>
    @endif

    <div class="col-md-12 col-lg-12 col-sm-12 profile-pre-text">

        <div class="col-lg-12 col-md-12 col-sm-12">
            @foreach($purchase as $p)
                ID do Pedido: {{ $p->id }} <br/>

                Status: {{ $p->status }} <br />

                Quantidade de produtos: {{ count($p->products()->get()) }} <br/>

                Total da compra: <i class="coins"></i> {{ $p->amount() }} coins

                <hr>
            @endforeach
        </div>
    </div>


@endsection
