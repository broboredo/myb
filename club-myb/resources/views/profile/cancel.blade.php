@extends('layouts.profile')

@section('content')

    <div class="col-md-12 col-lg-12 col-sm-12 profile-pre-text">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <p>
                Você pode cancelar sua assinatura se quiser, mas lembre-se, as vagas ainda são limitadas. <br />
                Corre o risco de não conseguir uma vaguinha da próxima vez.
            </p>
        </div>
    </div>

    <div class="col-md-12 col-lg-12 col-sm-12">
        <spark-settings :user="user" :teams="teams" inline-template>
            <spark-subscription :user="user" :team="team" :billable-type="billableType" inline-template>
                <div>
                    <div v-if="plans.length > 0">
                        <!-- Cancel Subscription -->
                        <div v-if="subscriptionIsActive">
                            <spark-cancel-subscription :user="user" :team="team" :billable-type="billableType" inline-template>
                                <div>
                                    <button class="btn"
                                    @click="confirmCancellation"
                                    :disabled="form.busy">
                                        Cancelar Assinatura
                                    </button>

                                    <!-- Confirm Cancellation Modal -->
                                    <div class="modal fade" id="modal-confirm-cancellation" tabindex="-1" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                                    <h4 class="modal-title">
                                                        Cancelar Assinatura
                                                    </h4>
                                                </div>

                                                <div class="modal-body">
                                                    <p>
                                                        Você pode cancelar sua assinatura se quiser, mas lembre-se, as vagas ainda são limitadas. <br />
                                                        Corre o risco de não conseguir uma vaguinha da próxima vez.
                                                    </p>
                                                </div>

                                                <!-- Modal Actions -->
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" @click="cancel" :disabled="form.busy">
                                                        <span v-if="form.busy">
                                                            <i class="fa fa-btn fa-spinner fa-spin"></i>Cancelando
                                                        </span>

                                                        <span v-else>
                                                            Quero Cancelar
                                                        </span>
                                                    </button>

                                                    <button type="button" class="btn" data-dismiss="modal">Não quero arriscar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </spark-cancel-subscription>

                        </div>
                    </div>
                </div>
            </spark-subscription>
        </spark-settings>
    </div>
@endsection
