@extends('layouts.shop')

@section('content')
    <div class="row">
        {{ csrf_field() }}


        @if(count($products) <= 0)

            <h3 style="margin: 80px 0">Opa! Ainda não têm produtos na vitrine... Aguarda só mais um pouquinho.</h3>

        @else


            @foreach($products as $product)

                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product-border">
                    <div class="block-product">
                        <div class="vitrine-product-top visible-lg visible-md">
                            <div class="ul-buttons" style="
        position: relative;
    z-index: 999">
                                <ul class="product-buttons" data-id="{{ $product->id }}">
                                    <a class="love pull-left @if($product->productLike()) hidden @endif" onclick="likeProduct(this);">
                                        <i class="fa fa-heart-o"></i>
                                        <i class="fa fa-heart"></i>
                                        Desejo
                                    </a>
                                    <a class="unlove pull-left @if(!$product->productLike()) hidden @endif" onclick="unlikeProduct(this);">
                                        <i class="fa fa-heart-o"></i>
                                        <i class="fa fa-heart"></i>
                                        Não Desejo
                                    </a>

                                    @if($product->quantity > 0 || $product->productInCart())
                                        <a class="add pull-right @if($product->productInCart()) hidden @endif" onclick="addProduct(this)">
                                            <i class="myb-box-o"></i>
                                            <i class="myb-box"></i>
                                            Adicionar à Minha Box
                                        </a>

                                        <a class="remove pull-right @if(!$product->productInCart()) hidden @endif" onclick="removeProduct(this);">
                                            <i class="myb-box-remove-o"></i>
                                            <i class="myb-box-remove"></i>
                                            Remover da Minha Box
                                        </a>
                                    @endif
                                </ul>
                            </div>

                            <div style="clear: both;"></div>

                            <div class="progressbar-row" style="
        position: relative;
        top: -40px;">
                                <div class="progressbar text-center">
                                    @if($product->quantity > 0)
                                        <div class="progress-text">Peças restantes</div>
                                        <progress max="10" value="{{ $product->quantity }}"></progress>
                                    @else
                                        <div class="progress-text red">ESGOTADO!</div>
                                        <progress max="100" value="1" class="sold-out"></progress>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="img-product text-center">
                            <img src="{{ asset('storage/products') }}/{{ $product->img }}" class="img-responsive" />
                        </div>

                        <div class="options text-center">
                            <div class="coin-value" style="
        position: relative;
        top: 60px;">
                                <h5>{{ $product->name }}</h5>
                                @if(!is_null($product->origin) and !empty($product->origin))
                                    <h6>({{ $product->origin }})</h6>
                                @endif
                                <div class="show-coin">
                                    <i class="coins"></i> {{ $product->coin_value }} coins
                                </div>
                            </div>
                            <div class="product-details" style="
        position: relative;">
                                <a href="{{ route('product-page', ["slug" => $product->slug]) }}">
                                    <button class="btn">
                                        Mais Detalhes
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@endsection