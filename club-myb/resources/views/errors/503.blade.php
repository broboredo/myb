<!DOCTYPE html>
<html lang="pt-br">
<head>
    <!-- Meta Information -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Decepcionado com sua mystery box? Pense fora da caixa, faça parte desse Império.">
    <meta name="keywords" content="mystery box, loot, box, make your box, nerdloot, nerd, geek, stranger things, game of thrones, the walking dead"/>
    <meta name="author" content="falecomigo@makeyourbox.com.br">
    
    <link rel="icon" type="image/x-icon" href="{{ asset('img/favico.ico') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favico.ico') }}">

    @if(!App::isLocal())
	    <script type="text/javascript">
	        window.smartlook||(function(d) {
	        var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
	        var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
	        c.charset='utf-8';c.src='//rec.smartlook.com/recorder.js';h.appendChild(c);
	        })(document);
	        smartlook('init', '82192c63cdf5580c15716ebecaf045227bcbe48b');
	    </script>
	@endif
    
    <title>Make Your Box</title>

    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @include('layouts.styles')

    <!-- Global Spark Object -->
    <script>
        window.Spark = <?php echo json_encode(array_merge(
            Spark::scriptVariables(), []
        )); ?>;
    </script>

</head>
<body>

    @if(!App::isLocal())
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-93894674-1', 'auto');
		  ga('send', 'pageview');
		</script>
	@endif
    
    <div id="spark-app" v-cloak>
		<div class="container-fluid">
		
	    	<!-- Header -->
			<header>
				<div class="container-fluid text-center">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="row">
							
							<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-xs-4">
								<a href="#" class="logo">
									<img alt="Logo Make Your Box" class="img-responsive" src="{{ asset('img/logo_header.png') }}" />
								</a>
							</div>
								
						</div>				
					</div>
				</div>
			</header>
	    
			
			<section id="banner">
				<div class="container">

					<div class="typing">
						<h1>Estamos nos <div style="color:#efc74a; display: inline-block;">reformulando</div> para dar</h1>
						<h1>a melhor experiência possível <div style="color:#efc74a; display: inline-block;">para você</div>.</h1>
					</div>
					
					<div class="text-center">
						{{--  <a href="#" class="btn white">Já é um Myber? <div style="color:#efc74a; display: inline-block;">Faça seu login aqui.</div></a>  --}}
					</div>
				</div>

			</section>
	    
			
			<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="col-lg-3 col-md-3 col-sm-12">
								<ul class="media">
									<li><a href="https://www.facebook.com/makeyourbox/" title="Facebook" target="_blank" class="media button"><i class="fa fa-facebook"></i></a></li>
									<li><a href="https://www.instagram.com/_makeyourbox/" title="Instagram" target="_blank" class="media button"><i class="fa fa-instagram"></i></a></li>
								</ul>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-4">
								<ul>
									<h4>Formas de Pagamento</h4>
									<img src="{{ asset('img/visa.png') }}" alt="Cartão Visa para pagamento" />
									<img src="{{ asset('img/mastercard.png') }}" alt="Cartão Master Card para pagamento" />
								</ul>
							</div>
							<div class="col-g-3 col-md-3 col-sm-4">
								<ul>
									<h4>Formas de Entrega</h4>
									<img src="{{ asset('img/pac.png') }}" alt="PAC para forma de envio" />
								</ul>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-4">
								<ul>
									<h4>Segurança</h4>
									<img src="{{ asset('img/rapidssl.png') }}" alt="RapidSSL" />
								</ul>
							</div>
						</div>
					</div>

					<hr />

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="col-lg-3 col-md-3 col-sm-4">
								<ul class="logo">
									<h4><img src="{{ asset('img/logo_footer.png') }}" class="logo" /></h4>
									<li>CNPJ: 26.848.301/0001-63</li>
									<li><a href="mailto:falecomigo@makeyourbox.com.br">falecomigo@makeyourbox.com.br</a></li>
								</ul>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-4">
								<ul>
									<h4>A Make Your Box</h4>
									<li><a href="{{ route('friends') }}">Nossos Aliados</a></li>
								</ul>
							</div>
							<div class="col-g-3 col-md-3 col-sm-4">
								<ul>
									<h4>Informações Gerais</h4>
									<li><a href="{{ route('questions') }}">Perguntas Frequentes</a></li>
									<li><a href="{{ route('terms') }}">Termos de Uso</a></li>
									<li><a href="{{ route('returns') }}">Trocas e Devoluções</a></li>
									<li><a href="{{ route('privacy') }}">Política de Privacidade</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="container-fluid">
					<ul class="copyright">
						Copyright &copy; {{ date('Y') }} - Todos os direitos reservados
					</ul>
				</div>
			</footer>
		</div>
    </div>
    
    <!-- Scripts -->
    @include('layouts.scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
</body>
</html>