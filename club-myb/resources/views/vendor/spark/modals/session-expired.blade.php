<!-- Session Expired Modal -->
<div class="modal fade" id="modal-session-expired" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    {{ trans('spark.modals.session_expired.session_expired') }}
                </h4>
            </div>

            <div class="modal-body">
                {{ trans('spark.modals.session_expired.please_login_again') }}
            </div>

            <!-- Modal Actions -->
            <div class="modal-footer">
                <a href="/login">
                    <button type="button" class="btn btn-default">
                        <i class="fa fa-btn fa-sign-in"></i>{{ trans('spark.modals.session_expired.go_to_login') }}
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>
