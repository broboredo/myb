@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>{{ trans('spark.shared.errors.whoops') }}!</strong> {{ trans('spark.shared.errors.something_went_wrong') }}!
        <br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
