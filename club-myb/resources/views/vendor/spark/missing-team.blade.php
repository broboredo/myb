@extends('spark::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-warning">
                <div class="panel-heading">{{ trans('spark.missing_team.wheres_your') }} {{ ucfirst(Spark::teamString()) }}?</div>

                <div class="panel-body">
                    {{ trans('spark.missing_team.it_looks') }} {{ Spark::teamString() }}! {{ trans('spark.missing_team.you_can_create') }} 
                    <a href="/settings#/{{ str_plural(Spark::teamString()) }}">{{ trans('spark.missing_team.settings') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
