<spark-update-payment-method-stripe :user="user" :team="team" :billable-type="billableType" inline-template>
    <div class="panel panel-default">
        <!-- Update Payment Method Heading -->
        <div class="panel-heading">
            <div class="pull-left">
                {{ trans('spark.settings.payment_method.update_payment_stripe.update_payment') }}
            </div>

            <div class="pull-right">
                <span v-if="billable.card_last_four">
                    <i :class="['fa', 'fa-btn', cardIcon]"></i>
                    ************@{{ billable.card_last_four }}
                </span>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="panel-body">
            <!-- Card Update Success Message -->
            <div class="alert alert-success" v-if="form.successful">
                {{ trans('spark.settings.payment_method.update_payment_stripe.card_updated') }}
            </div>

            <!-- Generic 500 Level Error Message / Stripe Threw Exception -->
            <div class="alert alert-danger" v-if="form.errors.has('form')">
            	{{ trans('spark.settings.payment_method.update_payment_stripe.alert') }}
            </div>

            <form class="form-horizontal" role="form">
                <!-- Billing Address Fields -->
                @if (Spark::collectsBillingAddress())
                    <h2><i class="fa fa-btn fa-map-marker"></i>{{ trans('spark.settings.payment_method.update_payment_stripe.billing_address') }}</h2>

                    @include('spark::settings.payment-method.update-payment-method-address')

                    <h2><i class="fa fa-btn fa-credit-card"></i>{{ trans('spark.settings.payment_method.update_payment_stripe.credit_card') }}</h2>
                @endif

                <!-- Cardholder's Name -->
                <div class="form-group">
                    <label for="name" class="col-md-4 control-label">{{ trans('spark.settings.payment_method.update_payment_stripe.cardholders_name') }}</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" v-model="cardForm.name">
                    </div>
                </div>

                <!-- Card Number -->
                <div class="form-group" :class="{'has-error': cardForm.errors.has('number')}">
                    <label for="number" class="col-md-4 control-label">{{ trans('spark.settings.payment_method.update_payment_stripe.card_number') }}</label>

                    <div class="col-md-6">
                        <input type="text"
                            class="form-control"
                            data-stripe="number"
                            :placeholder="placeholder"
                            v-model="cardForm.number">

                        <span class="help-block" v-show="cardForm.errors.has('number')">
                            @{{ cardForm.errors.get('number') }}
                        </span>
                    </div>
                </div>

                <!-- Security Code -->
                <div class="form-group">
                    <label for="cvc" class="col-md-4 control-label">{{ trans('spark.settings.payment_method.update_payment_stripe.security_code') }}</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" data-stripe="cvc" v-model="cardForm.cvc">
                    </div>
                </div>

                <!-- Expiration Information -->
                <div class="form-group">
                    <label class="col-md-4 control-label">{{ trans('spark.settings.payment_method.update_payment_stripe.expiration') }}</label>

                    <div class="col-md-6">
                        <div class="row">
                            <!-- Month -->
                            <div class="col-xs-6">
                                <input type="text" class="form-control"
                                    placeholder="MM" maxlength="2" data-stripe="exp-month" v-model="cardForm.month">
                            </div>

                            <!-- Year -->
                            <div class="col-xs-6">
                                <input type="text" class="form-control"
                                    placeholder="YYYY" maxlength="4" data-stripe="exp-year" v-model="cardForm.year">
                            </div>  
                        </div>
                    </div>
                </div>

                <!-- Zip Code -->
                <div class="form-group" v-if=" ! spark.collectsBillingAddress">
                    <label for="zip" class="col-md-4 control-label">{{ trans('spark.settings.payment_method.update_payment_stripe.zip') }}</label>

                    <div class="col-md-6">
                        <input type="text" class="form-control" v-model="form.zip">
                    </div>
                </div>

                <!-- Update Button -->
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" @click.prevent="update" :disabled="form.busy">
                            <span v-if="form.busy">
                                <i class="fa fa-btn fa-spinner fa-spin"></i>{{ trans('spark.settings.payment_method.update_payment_stripe.updating') }}
                            </span>

                            <span v-else>
                                {{ trans('spark.settings.payment_method.update_payment_stripe.update') }}
                            </span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</spark-update-payment-method-stripe>
