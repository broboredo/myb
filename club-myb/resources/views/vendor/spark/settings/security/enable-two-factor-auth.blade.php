<spark-enable-two-factor-auth :user="user" inline-template>
    <div class="panel panel-default">
        <div class="panel-heading">{{ trans('spark.settings.security.enable.two_factor') }}</div>

        <div class="panel-body">
            <!-- Information Message -->
            <div class="alert alert-info">
            	{{ trans('spark.settings.security.enable.two_factor') }}
            </div>

            <form class="form-horizontal" role="form">
                <!-- Country Code -->
                <div class="form-group" :class="{'has-error': form.errors.has('country_code')}">
                    <label class="col-md-4 control-label">{{ trans('spark.settings.security.enable.country_code') }}</label>

                    <div class="col-md-6">
                        <input type="number" class="form-control" name="country_code" v-model="form.country_code">

                        <span class="help-block" v-show="form.errors.has('country_code')">
                            @{{ form.errors.get('country_code') }}
                        </span>
                    </div>
                </div>

                <!-- Phone Number -->
                <div class="form-group" :class="{'has-error': form.errors.has('phone')}">
                    <label class="col-md-4 control-label">{{ trans('spark.settings.security.enable.phone_number') }}</label>

                    <div class="col-md-6">
                        <input type="tel" class="form-control" name="phone" v-model="form.phone">

                        <span class="help-block" v-show="form.errors.has('phone')">
                            @{{ form.errors.get('phone') }}
                        </span>
                    </div>
                </div>

                <!-- Enable Button -->
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-6">
                        <button type="submit" class="btn btn-primary"
                                @click.prevent="enable"
                                :disabled="form.busy">

                            <span v-if="form.busy">
                                <i class="fa fa-btn fa-spinner fa-spin"></i>{{ trans('spark.settings.security.enable.enabling') }}
                            </span>

                            <span v-else>
                                {{ trans('spark.settings.security.enable.enable') }}
                            </span>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</spark-enable-two-factor-auth>
