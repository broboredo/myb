<div class="panel panel-default">
    <div class="panel-heading">
        <div class="pull-left" :class="{'btn-table-align': hasMonthlyAndYearlyPlans}">
            {{ trans('spark.settings.subscription.subscribe_common.subscribe') }}
        </div>

        <!-- Interval Selector Button Group -->
        <div class="pull-right">
            <div class="btn-group" v-if="hasMonthlyAndYearlyPaidPlans">
                <!-- Monthly Plans -->
                <button type="button" class="btn btn-default"
                        @click="showMonthlyPlans"
                        :class="{'active': showingMonthlyPlans}">

                    {{ trans('spark.settings.subscription.subscribe_common.monthly') }}
                </button>

                <!-- Yearly Plans -->
                <button type="button" class="btn btn-default"
                        @click="showYearlyPlans"
                        :class="{'active': showingYearlyPlans}">

                    {{ trans('spark.settings.subscription.subscribe_common.yearly') }}
                </button>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

    <div class="panel-body table-responsive">
        <!-- Subscription Notice -->
        <div class="p-b-lg">
            {{ trans('spark.settings.subscription.subscribe_common.not_subscribed') }}
        </div>

        <!-- European VAT Notice -->
        @if (Spark::collectsEuropeanVat())
            <p class="p-b-lg">
                {{ trans('spark.settings.subscription.subscribe_common.vat_notice') }}
            </p>
        @endif

        <!-- Plan Error Message -->
        <div class="alert alert-danger" v-if="form.errors.has('plan')">
            @{{ form.errors.get('plan') }}
        </div>

        <table class="table table-borderless m-b-none">
            <thead></thead>
            <tbody>
                <tr v-for="plan in paidPlansForActiveInterval">
                    <!-- Plan Name -->
                    <td>
                        <div class="btn-table-align" @click="showPlanDetails(plan)">
                            <span style="cursor: pointer;">
                                <strong>@{{ plan.name }}</strong>
                            </span>
                        </div>
                    </td>

                    <!-- Plan Features Button -->
                    <td>
                        <button class="btn btn-default m-l-sm" @click="showPlanDetails(plan)">
                            <i class="fa fa-btn fa-star-o"></i>{{ trans('spark.settings.subscription.subscribe_common.plan_features') }}
                        </button>
                    </td>

                    <!-- Plan Price -->
                    <td>
                        <div class="btn-table-align">
                            @{{ plan.price | currency }} / @{{ plan.interval | capitalize }}
                        </div>
                    </td>

                    <!-- Trial Days -->
                    <td>
                        <div class="btn-table-align" v-if="plan.trialDays && ! hasSubscribed(plan)">
                            @{{ plan.trialDays}} {{ trans('spark.settings.subscription.subscribe_common.day_trial') }}
                        </div>
                    </td>

                    <!-- Plan Select Button -->
                    <td class="text-right">
                        <button class="btn btn-primary-outline btn-plan"
                                v-if="selectedPlan !== plan"
                                @click="selectPlan(plan)"
                                :disabled="form.busy">

                            {{ trans('spark.settings.subscription.subscribe_common.select') }}
                        </button>

                        <button class="btn btn-primary btn-plan"
                                v-if="selectedPlan === plan"
                                disabled>

                            <i class="fa fa-btn fa-check"></i>{{ trans('spark.settings.subscription.subscribe_common.selected') }}
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
