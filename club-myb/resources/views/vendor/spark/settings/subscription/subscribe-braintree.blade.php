<spark-subscribe-braintree :user="user" :team="team"
                :plans="plans" :billable-type="billableType" inline-template>

    <div>
        <!-- Common Subscribe Form Contents -->
        @include('spark::settings.subscription.subscribe-common')

        <!-- Billing Information -->
        <div class="panel panel-default" v-show="selectedPlan">
            <div class="panel-heading"><i class="fa fa-btn fa-credit-card"></i>{{ trans('spark.settings.subscription.subscribe_braintree.billing_information') }}</div>

            <div class="panel-body">
                <!-- Generic 500 Level Error Message / Stripe Threw Exception -->
                <div class="alert alert-danger" v-if="form.errors.has('form')">
                	{{ trans('spark.settings.subscription.subscribe_braintree.alert') }}
                </div>

                <form class="form-horizontal" role="form">
                    <!-- Braintree Container -->
                    <div id="braintree-subscribe-container" class="m-b-md"></div>

                    <!-- Subscribe Button -->
                    <div class="form-group m-b-none p-b-none">
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-primary" :disabled="form.busy">
                            <span v-if="form.busy">
                                <i class="fa fa-btn fa-spinner fa-spin"></i>{{ trans('spark.settings.subscription.subscribe_braintree.subscribing') }}
                            </span>

                            <span v-else>
                                {{ trans('spark.settings.subscription.subscribe_braintree.subscribe') }}
                            </span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</spark-subscribe-braintree>
