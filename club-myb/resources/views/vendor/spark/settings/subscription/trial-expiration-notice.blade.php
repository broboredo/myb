<div class="panel panel-warning" v-if="subscriptionIsOnTrial">
    <div class="panel-heading">
        <i class="fa fa-btn fa-clock-o"></i>{{ trans('spark.settings.subscription.trial.free_trial') }}
    </div>

    <div class="panel-body">
        {{ trans('spark.settings.subscription.trial.you_on_trial') }} <strong>@{{ trialEndsAt }}</strong>.
    </div>
</div>
