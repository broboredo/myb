<spark-resume-subscription :user="user" :team="team"
                :plans="plans" :billable-type="billableType" inline-template>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="pull-left" :class="{'btn-table-align': hasMonthlyAndYearlyPlans}">
                {{ trans('spark.settings.subscription.resume.resume') }}
            </div>

            <!-- Interval Selector Button Group -->
            <div class="pull-right">
                <div class="btn-group" v-if="hasMonthlyAndYearlyPlans">
                    <!-- Monthly Plans -->
                    <button type="button" class="btn btn-default"
                            @click="showMonthlyPlans"
                            :class="{'active': showingMonthlyPlans}">

                        {{ trans('spark.settings.subscription.resume.monthly') }}
                    </button>

                    <!-- Yearly Plans -->
                    <button type="button" class="btn btn-default"
                            @click="showYearlyPlans"
                            :class="{'active': showingYearlyPlans}">

                        {{ trans('spark.settings.subscription.resume.yearly') }}
                    </button>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="panel-body table-responsive">
            <!-- Plan Error Message - In General Will Never Be Shown -->
            <div class="alert alert-danger" v-if="planForm.errors.has('plan')">
                @{{ planForm.errors.get('plan') }}
            </div>

            <!-- Cancellation Information -->
            <div class="p-b-lg">
                {{ trans('spark.settings.subscription.resume.you_have_cancelled') }}
                <strong>@{{ activePlan.name }} (@{{ activePlan.interval | capitalize }})</strong> {{ trans('spark.settings.subscription.resume.plan') }}.
            </div>

            <div class="p-b-lg">
            	The benefits of your subscription will continue until your current billing period ends on
                <strong>@{{ activeSubscription.ends_at | date }}</strong>. You may resume your subscription at no
                extra cost until the end of the billing period.
            </div>

            <!-- European VAT Notice -->
            @if (Spark::collectsEuropeanVat())
                <p class="p-b-lg">
                    {{ trans('spark.settings.subscription.resume.vat') }}
                </p>
            @endif

            <table class="table table-borderless m-b-none">
                <thead></thead>
                <tbody>
                    <tr v-for="plan in paidPlansForActiveInterval">
                        <!-- Plan Name -->
                        <td>
                            <div class="btn-table-align" @click="showPlanDetails(plan)">
                                <span style="cursor: pointer;">
                                    <strong>@{{ plan.name }}</strong>
                                </span>
                            </div>
                        </td>

                        <!-- Plan Features Button -->
                        <td>
                            <button class="btn btn-default m-l-sm" @click="showPlanDetails(plan)">
                                <i class="fa fa-btn fa-star-o"></i>{{ trans('spark.settings.subscription.resume.plan_features') }}
                            </button>
                        </td>

                        <!-- Plan Price -->
                        <td>
                            <div class="btn-table-align">
                                @{{ priceWithTax(plan) | currency }} / @{{ plan.interval | capitalize }}
                            </div>
                        </td>

                        <!-- Plan Select Button -->
                        <td class="text-right">
                            <button class="btn btn-plan"
                                    v-bind:class="{'btn-warning-outline': ! isActivePlan(plan), 'btn-warning': isActivePlan(plan)}"
                                    @click="updateSubscription(plan)"
                                    :disabled="selectingPlan">

                                <span v-if="selectingPlan === plan">
                                    <i class="fa fa-btn fa-spinner fa-spin"></i>{{ trans('spark.settings.subscription.resume.resuming_button') }}
                                </span>

                                <span v-if="! isActivePlan(plan) && selectingPlan !== plan">
                                    {{ trans('spark.settings.subscription.resume.switch_button') }}
                                </span>

                                <span v-if="isActivePlan(plan) && selectingPlan !== plan">
                                    {{ trans('spark.settings.subscription.resume.resume_button') }}
                                </span>
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</spark-resume-subscription>
