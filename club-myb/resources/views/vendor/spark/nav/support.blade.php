<li class="dropdown-header">{{ trans('spark.nav.support.support') }}</li>

<!-- Support -->
<li>
    <a @click.prevent="showSupportForm" style="cursor: pointer;">
        <i class="fa fa-fw fa-btn fa-paper-plane"></i>{{ trans('spark.nav.support.email_us') }}
    </a>
</li>

<li class="divider"></li>
