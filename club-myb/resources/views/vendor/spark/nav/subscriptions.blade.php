@if (Auth::user()->onTrial())
    <!-- Trial Reminder -->
    <li class="dropdown-header">{{ trans('spark.nav.subscriptions.trial') }}</li>

    <li>
        <a href="/settings#/subscription">
            <i class="fa fa-fw fa-btn fa-shopping-bag"></i>{{ trans('spark.nav.subscriptions.subscribe') }}
        </a>
    </li>

    <li class="divider"></li>
@endif

@if (Spark::usesTeams() && Auth::user()->currentTeamOnTrial())
    <!-- Team Trial Reminder -->
    <li class="dropdown-header">{{ ucfirst(Spark::teamString()) }} {{ trans('spark.nav.subscriptions.trial') }}</li>

    <li>
        <a href="/settings/{{ str_plural(Spark::teamString()) }}/{{ Auth::user()->currentTeam()->id }}#/subscription">
            <i class="fa fa-fw fa-btn fa-shopping-bag"></i>{{ trans('spark.nav.subscriptions.subscribe') }}
        </a>
    </li>

    <li class="divider"></li>
@endif
