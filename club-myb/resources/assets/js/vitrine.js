$(document).ready(function() {
    refreshCoins();
});

var vitrine = true;

function refreshCoins()
{
    $.get(url_path+'/user/coins', function(response) {
        $('div[coins]').html(response);
    });
}

function addProduct(button)
{
    var id = $(button).parents('ul.product-buttons').attr('data-id');

    ajax(url_path+'/cart', button, id);

    return false;
}

function removeProduct(button)
{
    var _token = $('input[name="_token"]').val();
    var id = $(button).parents('ul.product-buttons').attr('data-id');
    var that = button;

    $.ajax({
        'headers': {
            'X-CSRF-Token': _token
        },
        'url': url_path+'/product-cart/'+id,
        'method': 'DELETE',
        'dataType': 'json',
        'async': true,

        'beforeSend': function () {
            $(that).attr("disabled","disabled");
            $(that).addClass("disabled");
        },

        'success': function (response) {

            if(response.status == 1) {
            	if(vitrine)
        		{
                    $(that).addClass('hidden');
                    $(that).parents('ul.product-buttons').find('.add').removeClass('hidden');            		
        		} else {
                    $(that).parents('ul.product-buttons').find('li.add').removeClass('hidden');   
                    $(that).attr("disabled", "disabled");
                    $(that).addClass("disabled");
                }
                
                successMessage(response.message);
                refreshCoins();

            } else {
                errorMessage(response.message)
            }


        },


        'error': function(jqXHR, text, error) {

            errorMessage("Erro");

        },

        'complete': function(jqXHR, text) {
            $(that).attr("disabled", false);
            $(that).removeClass("disabled");
        }
    })
}

function ajax(url, that, id)
{

    var _token = $('input[name="_token"]').val();

    $.ajax({
        'url': url,
        'data': {_token: _token, id: id},
        'method': 'POST',
        'dataType': 'json',
        'async': true,

        'beforeSend': function () {
            $(that).attr("disabled","disabled");
            $(that).addClass("disabled");
        },

        'success': function (response) {

            if(response.status == 1) {
            	if(vitrine)
            	{ 
	                $(that).addClass('hidden');
	                $(that).parents('ul.product-buttons').find('.remove').removeClass('hidden');
            	} else {
	                $(that).parents('ul.product-buttons').find('li.remove').removeClass('disabled'); 
                    $(that).attr("disabled", "disabled");
                    $(that).addClass("disabled");
            	}
                	
                successMessage(response.message);
                refreshCoins();

            } else {
                errorMessage(response.message)
            }


        },


        'error': function(jqXHR, text, error) {


            errorMessage("Erro");

        },

        'complete': function(jqXHR, text) {
            $(that).attr("disabled", false);
            $(that).removeClass("disabled");

        }
    });
}


function unlikeProduct(button)
{
    var _token = $('input[name="_token"]').val();
    var id = $(button).parents('ul.product-buttons').attr('data-id');
    var that = button;

    $.ajax({
        'url': url_path+'/product/unlike',
        'data': { id: id, _token: _token },
        'method': 'POST',
        'dataType': 'json',
        'async': true,

        'beforeSend': function () {
            $(that).attr("disabled","disabled");
            $(that).addClass("disabled");
        },

        'success': function (response) {

            if(response.status == 1) {
            	if(vitrine)
        		{
	                $(that).addClass('hidden');
	                $(that).parents('ul.product-buttons').find('.love').removeClass('hidden');
        		} else {
	                $(that).parents('li.love').removeClass('active');
	                $(that).parents('li.love a').prop('onclick', null);
	                $(that).parents('li.love').attr('onclick', 'likeProduct(this); vitrine=false;');
        		}
            	
                successMessage(response.message);

            } else {
                errorMessage(response.message)
            }
        },

        'error': function(jqXHR, text, error) {
            errorMessage("Erro");
        },

        'complete': function(jqXHR, text) {
            $(that).attr("disabled", false);
            $(that).removeClass("disabled");
        }
    })
}


function likeProduct(button)
{
    var _token = $('input[name="_token"]').val();
    var id = $(button).parents('ul.product-buttons').attr('data-id');
    var that = button;

    $.ajax({
        'url': url_path+'/product/like',
        'data': { id: id, _token: _token },
        'method': 'POST',
        'dataType': 'json',
        'async': true,

        'beforeSend': function () {
            $(that).attr("disabled","disabled");
            $(that).addClass("disabled");
        },

        'success': function (response) {

            if(response.status == 1) {
            	if(vitrine)
            	{	
	                $(that).addClass('hidden');
	                $(that).parents('ul.product-buttons').find('.unlove').removeClass('hidden');
            	} else {
	                $(that).parents('li.love').addClass('active');
	                $(that).parents('li.love a').prop('onclick', null);
	                $(that).parents('li.love').attr('onclick', 'unlikeProduct(this); vitrine=false;');
            	}
                
                successMessage(response.message);
            } else {
                errorMessage(response.message)
            }
        },

        'error': function(jqXHR, text, error) {
            errorMessage("Erro");
        },

        'complete': function(jqXHR, text) {
            $(that).attr("disabled", false);
            $(that).removeClass("disabled");
        }
    })
}

function likeThisProduct(button)
{
	
	var _token = $('input[name="_token"]').val();
    var id = $(button).parents('ul.product-buttons').attr('data-id');
    var that = button;

    $.ajax({
        'url': url_path+'/product/like-this',
        'data': { id: id, _token: _token },
        'method': 'POST',
        'dataType': 'json',
        'async': true,

        'beforeSend': function () {
            $(that).attr("disabled","disabled");
            $(that).addClass("disabled");
        },

        'success': function (response) {

            if(response.status == 1) {
            	if($(that).parents('li.love.active').length > 0) {
            		$(that).parents('li.love').removeClass('active');
            	} else {
            		$(that).parents('li.love').addClass('active');
            	}
                
                successMessage(response.message);
            	
            } else {
                errorMessage(response.message)
            }
        },

        'error': function(jqXHR, text, error) {
            errorMessage("Erro");
        },

        'complete': function(jqXHR, text) {
            $(that).attr("disabled", false);
            $(that).removeClass("disabled");
        }
    })


}

function loveIt(button)
{
	
	var _token = $('input[name="_token"]').val();
    var id = $(button).attr('data-id');
    var that = button;

    $.ajax({
        'url': url_path+'/product/like-this',
        'data': { id: id, _token: _token },
        'method': 'POST',
        'dataType': 'json',
        'async': true,

        'beforeSend': function () {
            $(that).attr("disabled","disabled");
            $(that).addClass("disabled");
        },

        'success': function (response) {

            if(response.status == 1) {
            	if($(that).hasClass('active')) {
            		$(that).removeClass('active');
            	} else {
            		$(that).addClass('active');
            	}

                
                successMessage(response.message);
                
            } else {
                errorMessage(response.message)
            }
        },

        'error': function(jqXHR, text, error) {
            errorMessage("Erro");
        },

        'complete': function(jqXHR, text) {
            $(that).attr("disabled", false);
            $(that).removeClass("disabled");
        }
    })


}

function productToggle(button)
{
	
	var _token = $('input[name="_token"]').val();
    var id = $(button).attr('data-id');
    var that = button;

    $.ajax({
        'url': url_path+'/product/toggle',
        'data': { id: id, _token: _token },
        'method': 'POST',
        'dataType': 'json',
        'async': true,

        'beforeSend': function () {
            $(that).attr("disabled","disabled");
            $(that).addClass("disabled");
        },

        'success': function (response) {

            if(response.status == 1) {
            	if($(that).hasClass('active')) {
            		$(that).removeClass('active').html('<i class="myb-box-g"></i> Adicionar à Minha Box');
            	} else {
            		$(that).addClass('active').html('<i class="myb-box-g"></i> Remover da Minha Box');
            	}
                
                successMessage(response.message);
                refreshCoins();
            	
            } else {
                errorMessage(response.message)
            }
        },

        'error': function(jqXHR, text, error) {
            errorMessage("Erro");
        },

        'complete': function(jqXHR, text) {
            $(that).attr("disabled", false);
            $(that).removeClass("disabled");
        }
    })


}





