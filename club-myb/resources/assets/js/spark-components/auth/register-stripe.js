var base = require('auth/register-stripe');

Vue.component('spark-register-stripe', {
    mixins: [base],
    mounted: function(){
    	
	},
    methods: {
        findAddress: function (event) {
            var cep = event.target.value;
            var self = this;

            $.ajax({
                url: 'http://api.postmon.com.br/v1/cep/' + cep,
                dataType: 'json',
                crossDomain: true,
                success: function (data) {
                    $('.address').val(data.logradouro);
                    $('.city').val(data.cidade);
                    $('.state').val(data.estado);
                    $('.address2').val(data.complemento);
                    $('.neighborhood').val(data.bairro);
                    $('.country option[value="BR"]').attr('selected', true);
                }
            });
        }
    }
});
