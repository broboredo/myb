<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Spark Language Lines
    |--------------------------------------------------------------------------
    */

	/** 
	 * Settings
	 * 
	 */

	'settings' => [
		'outer' => [
			'settings'	=> 'Configurações',
			'profile'	=> 'Perfil',
			'security'	=> 'Segurança',
			'api'		=> 'API',
			'billing'	=> 'Plano de Pagamento',
			'subscription'	=> 'Assinatura',
			'payment_method'	=> 'Método de Pagamento',
			'invoices'	=> 'Chamados'
		],
		'payment_method' => [
			'current_discount' => "Desconto Atual",
			'currently_receive' => "Atualmente, você recebe um desconto de",
			'for' => "para",
			'redeem' => [
				'redeem_coupon' => "Resgatar Cupom",
				'coupon_accepted' => "Cupom aceito! O desconto será aplicado na próxima fatura.",
				'coupon_code' => "Código do Cupom",
				'redeeming' => "Resgatando",
				'redeem' => "Resgatar"
			],
			'update_payment_address' => [
				'address' => "Endereço",
				'address_2' => "Complemento",
				'city' => "CIdade",
				'zip' => "CEP",
				'country' => "País"
			],
			'update_payment_braintree' => [
				'update_payment' => "Atualizar Método de Pagamento",
				'card_updated' => "Seu cartão foi atualizado.",
				'alert' => "Ocorreu um problema ao atualizar seu método de pagamento. É possível que seu provedor do cartão esteja bloqueando.
                 			Entre em contato com seu provedor do cartão ou suporte ao cliente.",
				'updating' => "Atualizando",
				'update' => "Atualizar"
			],
			'update_payment_stripe' => [
				'update_payment' => "Atualizar Método de Pagamento",
				'payment_updated' => "Seu método de pagamento foi atualizado.",
				'alert' => "Ocorreu um problema ao atualizar seu método de pagamento. É possível que seu provedor do cartão esteja bloqueando.
                 			Entre em contato com seu provedor do cartão ou suporte ao cliente.",
				'billing_address' => "Endereço de Cobrança",
				'credit_card' => "Cartão de Crédito",
				'cardholders_name' => "Nome no Cartão",
				'card_number' => "Número do Cartão",
				'security_code' => "Código de Segurança",
				'expiration' => "Vencimento",
				'zip' => "CEP",
				'updating' => "Atualizando",
				'update' => "Atualizar"
			],
			'update_vat' => [
				'update_vat' => "Atualizar VAT ID",
				'vat_updated' => "Seu VAT ID foi atualizado!",
				'vat_id' => "VAT ID",
				'updating' => "Atualizando",
				'update' => "Atualizar"
			]
		],
			
		'api' => [
			'create_token' => [
				'create_token' => "Criar Token API",
				'token_name' => "Nome do Token",
				'assing_all_abilities' => "Atribuir Tudo",
				'remove_all_abilities' => "Remover Tudo",
				'token_can' => "Token Can",
				'create' => "Criar",
				'api_token' => "Token API",
				'alert' => "Aqui está seu Token API. <strong>Esta é a única vez que seu Token API será exibido,
							então não o perca!</strong> Você pode sobrescrevê-lo futuramente em Configurações.",
				'copy' => "Copiar",
				'select_all' => "Selecionar Todos",
				'close' => "Fechar"
			],
			'tokens' => [
				'api_tokens' => "Tokens API",
				'name' => "Nome",
				'last_used' => "Última Vez Usado",
				'never' => "Nunca",
				'edit_token' => "Editar Token",
				'token_name' => "Nome do Token",
				'token_can' => "Token Can",
				'close' => "Fechar",
				'update_button' => "Atualizar",
				'delete_token' => "Deletar Token",
				'body' => 	"Você tem certeza que quer deletar este token? Se excluído, as requisições de API que necessitam desse Token não serão mais aceitas.",
				'no' => "Não",
				'yes' => "Sim"
			]
		],
		'invoices' => [
			'invoice_list' => [
				'invoices' => "Chamados"
			],
			'update_extra' => [
				'extra_billing' => "Informações Extras do Pagamento",
				'alert' => "Essas informações aparecerão em todos os seus recibos e é um ótimo lugar para adicionar:
							Nome da empresa, endereço, etc. Não inclua informações confidenciais ou informações financeiras, 
							como números de cartão de crédito.",
				'billing_updated' => "Suas informações de faturamento foram atualizadas.",
				'update_button' => "Atualizar"
			]
		],
		'security' => [
			'modals' => [
				'two_factor' => [
					'two_factor' => "Redefinir Código de Autenticação Two-Factor",
					'alert' => "Se você perder o dispositivo de autenticação two-factor, você pode usar isso
			                    para desativar a Autenticação Two-Factor da sua conta.
			                    <strong>Esta é a única vez que este token será exibido, portanto não o perca!</strong>",
					'close' => "Fechar"
				]
			],
			'disable' => [
				'disabling' => "Desabilitando",
				'disable' => "Desabilitar Autenticação Two-Factor"
			],
			'enable' => [
				'two_factor' => "Autenticação Two-Factor",
				'alert' => "Para usar a autenticação Two-Factor, você <strong> deve </ strong> instalar o
							<Strong> <a href='https://authy.com' target='_blank'> Authy </a> </ strong>
							No smartphone. O Authy está disponível para iOS e Android.",
				'country_code' => "Código do País",
				'phone_number' => "Telefone",
				'enabling' => "Habilitando",
				'enable' => "Habilitar"
			],
			'update' => [
				'update_password' => "Atualizar Senha",
				'password_updated' => "Sua senha foi atualizada!",
				'current_password' => "Senha Atual",
				'password' => "Senha",
				'confirm_password' => "Confirme a Senha",
				'update' => "Atualizar"
			]
		],
		'subscription' => [
			'cancel' => [
				'cancel' => "Cancelar Assinatura",
				'are_you_sure' => "Você tem certeza que deseja cancelar a assinatura?",
				'no' => "Não",
				'yes' => "Sim",
				'cancelling' => "Cancelando"
			],
			'resume' => [
				'resume' => 'Assinar',
				'monthly' => "Mensalmente",
				'yearly' => "Anualmente",
				'you_have_cancelled' => "Cancelado a assinatura para o plano ",
				'plan' => "",
				'benefits' => 	"",
				'vat' => "Todos os preços de assinaturas incluem IOF aplicados.",
				'plan_features' => "Recursos do Plano",
				'resuming_button' => "Assinando",
				'switch_button' => "Trocar",
				'resume_button' => "Assinar"
			],
			'trial' => [
				'free_trial' => "Teste Grátis",
				'you_on_trial' => "Atualmente você está no seu período de Teste Grátis. Seu Teste Grátis expirará em"
			],
			'update' => [
				'update' => "Atualizar Assinatura",
				'monthly' => "Mensalmente",
				'yearly' => "Anualmente",
				'you_are_currently' => "Você atualmente assina o plano ",
				'plan' => "",
				'vat_notice' => "Todos os preços de assinaturas incluem IOF aplicados.",
				'plan_features' => "Recursos do Plano",
				'free' => "Grátis",
				'current_plan' => "Plano Atual",
				'switch' => "Trocar",
				'updating' => "Atualizando",
				'update_subscription' => "Atualizar Assinatura",
				'are_you_want_switch' => "",
				'no' => "Não",
				'yes' => "Sim"
			],
			'subscribe_stripe' => [
				'billing_information' => "Informação de Pagamento",
				'alert' => "Tivemos problemas para validar o seu cartão. É possível que o seu provedor de cartão esteja nos impedindo
						de carregar o cartão. Entre em contato com o seu provedor de cartão ou suporte ao cliente.",
				'billing_address' => "Endereço de Cobrança",
				'credit_card' => "Cartão de Crédito",
				'cardholders_name' => "Nome no Cartão",
				'card_number' => "Número do Cartão",
				'security_code' => "Código de Segurança",
				'expiration' => "Validade",
				'zip' => "CEP",
				'coupon' => "Cupom",
				'tax' => "Taxa",
				'total_tax' => "Valor total incluindo taxa",
				'subscribing' => "Assinando",
				'subscribe' => "Assinar"
			],
			'subscribe_common' => [
				'subscribe' => "Assinar",
				'monthly' => "Mensalmente",
				'yearly' => "Anualmente",
				'not_subscribed' => "Você não está inscrito em nenhum plano. Escolha entre os planos abaixo para começar.",
				'vat_notice' => "Todos os preços de assinaturas incluem IOF aplicados.",
				'plan_features' => "Recursos do Plano",
				'day_trial' => "Dia de Teste",
				'select' => "Selecione",
				'selected' => "Selecionado"
			],
			'subscribe_braintree' => [
				'billing_information' => "Informação de Pagamento",
				'alert' => "Tivemos problemas para validar o seu cartão. É possível que o seu provedor de cartão esteja nos impedindo
						de carregar o cartão. Entre em contato com o seu provedor de cartão ou suporte ao cliente.",
				'subscribing' => "Assinando",
				'subscribe' => "Assinar"
			],
			'subscribe_address' => [
				'address' => "Endereço",
				'address_2' => "Complemento",
				'city' => "Cidade",
				'zip' => "ZIP",
				'state' => "Estado",
				'country' => "País",
				'vat_id' => "VAT ID"
			]
		]
	],
		

	/**
	 * Profile
	 *
	 */
	
	'profile'		=> [
		'profile_photo'	=>	'Foto de Perfil',
		'select_new_photo'	=> 	'Selecionar Nova Foto',
		'contact_information'	=> 'Contato',
		'contact_information_has_been_updated'	=> 	'Seu contato precisa ser atualizado!',
		'name' => 'Nome',
		'email_address'	=>	'E-mail',
		'update'	=>	'Atualizar'
	],
		

	/**
	 * Auth
	 *
	 */
	
	'auth'		=>	[
		'passwords' => [
			'email'	=> [
				'reset_password' => "Redefinir Senha",
				'email_address' => "E-mail",
				'send_password_reset' => "Enviar link de redefinição de senha"
			],
			'reset' => [
				'reset_password' => "Redefinir Senha",
				'email_address' => "E-Mail",
				'password' => "Senha",
				'confirm_password' => "Confirme a Senha"
			]
		],
			
		'login_emergency' => [
			'login_emergency' => "Login Via Token de Emergência",
			'alert' =>	'Após logado através do seu token de emergência, a autenticação two-factor será desabilitado para sua conta.
						Se quiser manter a segurança da autenticação two-factor, você deve reativá-lo após logar.',
			'token_emergency' => "Token de Emergência",
			'login' => "Entrar"
		],
			
		'login' => [
			'login' => "Login",
			'email_address' => 'E-mail',
			'password' => "Senha",
			'remember_me' => "Lembrar",
			'forgot_your_password' => "Esqueceu sua Senha?"
		],
			
		'braintree' => [
			'billing' => "Billing",
			'alert' => "Tivemos problemas para validar o seu cartão. É possível que o seu provedor de cartão esteja nos impedindo
						de carregar o cartão. Entre em contato com o seu provedor de cartão ou suporte ao cliente.",
			'coupon_code' => "Código do Cupom",
			'accept' => "Eu Aceito Os",
			'terms_of_service' => "Termos De Serviço",
			'registring' => 'Registrando',
			'register' => "Registrar"
		],
			
		'token' => [
			'two_factor_authentication' => "Autenticação Two-Factor",
			'authentication_token' => "Token de Autenticação",
			'verify' => "Verificar",
			'lost_device' => "Perdeu seu Dispositivo?"
		],
			
			
			
			
		'register'		=> [
			'coupons'	=>	[
				'discount'	=>	'Desconto',
				'discount_message'	=>	"O cupom de :discount de desconto será aplicado à sua assinatura!",
				'invalid'	=>	'Whoops! Este código de cupom é inválido.',
				'invitation'	=>	'Encontramos o seu convite',
				'invitation_invalid'	=>	'Whoops! Este código de convite é inválido.'
			],
			'subscription'	=> 'Assinatura',
			'monthly'	=> 'Mensalmente',
			'yearly'	=> 'Anualmente',
			'vat'	=> 'All subscription plan prices are excluding applicable VAT.',
			'plan_features'	=>	'Recursos',
			'free'	=>	'Grátis',
			'day_trial'	=> 	'Dia de Teste',
			'selected'	=> 'Selecionado',
			'select'	=> 'Selecione',
			'profile'	=> 'Perfil',
			'register'	=> 'Registrar',
				
			'team_name'	=>	'Equipe',
			'slug'	=>	'Slug',
			'slug_defination'	=>	'Este "slug" é usado para identificar a URL da sua equipe.',
			'name'	=>	'Nome',
			'email'	=>	'E-mail',
			'password'	=>	'Senha',
			'confirm_password'	=>	'Confirme a Senha',
			'accept'	=>	'Eu aceito os',
			'terms_of_service'	=> 'Termos De Serviço',
			'registering'	=>	'Registrando'
		],	
			
			
		'register_address'		=>	[
			'address'	=>	'Endereço',
			'address_line_2'	=>	'Complemento',
			'city'	=>	'Cidade',
			'zip'	=>	'Estado / CEP',
			'country'	=>	'País',
			'vat_id'	=>	'VAT ID',
		],
			
		'stripe'	=>	[
			'billing_information'	=>	'Informação de Pagamento',
			'error_500'	=>	"Tivemos problemas para validar o seu cartão. Entre em contato com o seu Banco ou conosco.",
			'billing_address'	=>	'Endereço de Cobrança',
			'credit_card'	=>	'Cartão de Crédito',
			'credit_card_name'	=>	"Nome no Cartão",
			'credit_card_number'	=>	'Número do Cartão',
			'credit_card_security_code'	=>	'Código de Segurança',
			'credit_card_expiration'	=>	'Expiração',
			'zip'	=>	'CEP',
			'coupon_code'	=>	'Código do Cupom',
			'accept'	=>	'Eu aceito os',
			'terms_of_service'	=> 'Termos de Serviço',
			'tax'	=>	'Taxa:',
			'tax_total'	=>	'Valor Total Incluindo Taxa:',
			'registering'	=>	'Registrando',
			'register'	=>	'Registrar'
		],
	],
		
	'kiosk' => [
		'modals' => [
			'add_discount' => [
				'add_discount' => "Add Desconto",
				'user_has_discount' => "Este usuário tem um desconto de",
				'user_has_discount_for' => "para",
				'discount_type' => "Tipo de Desconto",
				'amount' => "Valor",
				'percentage' => "Porcentagem",
				'discount_duration' => "Duração do Desconto",
				'once' => "Uma vez",
				'forever' => "Sempre",
				'multiple_months' => "Múltiplos Meses",
				'months' => "Meses",
				'applying' => "Aplicando",
				'apply' => "Aplicar Desconto"
			]
		],
		'announcements' => [
			'create' => "Criar Anúncio",
			'alert' => 'Os anúncios criados aqui serão enviados para a seção de "Anúncios de Produtos", informando seus assinantes sobre novos recursos e melhorias.',
			'announcement' => "Anúncio",
			'action_button_text' => "Botão para Texto",
			'action_button_url' => "Botão para Link",
			'recent_announcement' => "Anúncios Recentes",
			'create_button' => "Criar",
			'date' => "Data",
			'update' => "Editar Anúncio",
			'close' => "Fechar",
			'update_button' => "Editar",
			'delete' => "Deletar Anúncio",
			'are_you_sure' => "Você tem certeza que deseja deletar este anúncio?",
			'no' => "Não",
			'yes' => "Sim"
		],
		
			
		'metrics' => [
			'monthly_recurring_revenue' => "Receita Mensal Recorrente",
			'from_last_month' => "do último mês",
			'from_last_year' => "do último ano",
			'yearly_recurring_revenue' => "Receita Mensal Anualmente",
			'total_volume' => "Total",
			'users_currently_trialing' => "Usuário em Vigor",
			'daily_volume' => "Volume Diário",
			'new_users' => "Novos Usuários",
			'subscribers' => "Assinantes",
			'name' => "Nome",
			'trialing' => "Testando",
			'on_generic_trial' => "Em Teste Genérico",
			'n_a' => "N/A"
		],
		'profile' => [
			'loading' => "Carregando",
			'email_address' => "E-mail",
			'joined' => "Ingressou",
			'subscription' => "Inscrição",
			'none' => "Nenhum",
			'total_revenue' => "Receita Total",
			'name' => "Nome"
		],
		'users' => [
			'search' => "Procure por Nome ou E-mail...",
			'search_results' => "Procurar Resultados",
			'searching' => "Procurando",
			'no_users_matched' => "Não foi encontrado usuários para essa pesquisa.",
			'name' => "Nome",
			'email_address' => "E-mail"
		],
		'outer' => [
			'announcements' => "Anúncios",
			'metrics' => "Metricas",
			'users' => "Usuários"
		] 
	],
		
	'modals' => [
		'support' => [
			'email_address' => "E-mail",
			'subject' => "Assunto",
			'message' => "Mensagem",
			'send' => "Enviar"
		],
		'session_expired' => [
			'session_expired' => "Sessão Expirada",
			'please_login_agin' => "Sua sessão expirou. Por favor entre novamente para continuar.",
			'go_to_login' => "Entrar"
		],
		'plain_details' => [
			'close' => "Fechar"
		],
		'notifications' => [
			'notifications' => "Notificações",
			'announcements' => "Anúncios",
			'loading_notifications' => "Carregando Notificações",
			'alert' => "Não temos novidades por agora! E pode deixar que quando tivermos você será o primeiro a saber!",
			'close' => "Fechar"
		]
	],
		
	'nav' => [
		'guest' => [
			'login' => "Entrar",
			'register' => "Registrar"
		],
		'subscriptions' => [
			'trial' => "Trial",
			'subscribe' => "Inscreva-se"
		],
		'support' => [
			'support' => "Suporte",
			'email_us' => "Nos mande um e-mail"
		],
		'user' => [
			'impersonation' => "Representação",
			'back_to_my_acc' => "Voltar Para Minha Conta",
			'settings' => "Configurações",
			'your_settings' => "Suas Configurações",
			'logout' => "Sair"
		]
	],
		
	'shared' => [
		'errors' => [
			'whoops' => "Opa",
			'something_went_wrong' => "Algo deu errado"
		]
	],
		
	
	'missing_team' => [
		'wheres_your' => "Onde está seu",
		'it_looks' => "Parece que você não faz parte de nenhum",
		'you_can_create' => "Você pode criar um em suas",
		'settings' => "configurações"
	],
		
	'terms' => [
		'terms_of_service' => "Termos De Serviço"
	]
];
