<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Spark Language Lines
    |--------------------------------------------------------------------------
    */

	/** 
	 * Settings
	 * 
	 */
		
	'settings'			=> [
		'outer' => [
			'settings'	=> 'Settings',
			'profile'	=> 'Profile',
			'security'	=> 'Security',
			'api'		=> 'API',
			'billing'	=> 'Billing',
			'subscription'	=> 'Subscription',
			'payment_method'	=> 'Payment Method',
			'invoices'	=> 'Invoices'
		],
		'payment_method' => [
			'current_discount' => "Current Discount",
			'currently_receive' => "You currently receive a discount of",
			'for' => "for",
			'redeem' => [
				'redeem_coupon' => "Redeem Coupon",
				'coupon_accepted' => "Coupon accepted! The discount will be applied to your next invoice.",
				'coupon_code' => "Coupon Code",
				'redeeming' => "Redeeming",
				'redeem' => "Redeem"
			],
			'update_payment_address' => [
				'address' => "Address",
				'address_2' => "Address Line 2",
				'city' => "City",
				'zip' => "State & ZIP / Postal Code",
				'country' => "Country"
			],
			'update_payment_braintree' => [
				'update_payment' => "Update Payment Method",
				'card_updated' => "Your card has been updated.",
				'alert' => "We had trouble updating your payment method. It's possible your payment provider is preventing
                			us from charging the payment method. Please contact your payment provider or customer support.",
				'updating' => "Updating",
				'update' => "Update"
			],
			'update_payment_stripe' => [
				'update_payment' => "Update Payment Method",
				'payment_updated' => "Your payment method has been updated.",
				'alert' => "We had trouble updating your payment method. It's possible your payment provider is preventing
                			us from charging the payment method. Please contact your payment provider or customer support.",
				'billing_address' => "Billing Address",
				'credit_card' => "Credit Card",
				'cardholders_name' => "Cardholder's Name",
				'card_number' => "Card Number",
				'security_code' => "Security Code",
				'expiration' => "Expiration",
				'zip' => "ZIP / Postal Code",
				'updating' => "Updating",
				'update' => "Update"
			],
			'update_vat' => [
				'update_vat' => "Update VAT ID",
				'vat_updated' => "Your VAT ID has been updated!",
				'vat_id' => "VAT ID",
				'updating' => "Updating",
				'update' => "Update"
			]
		],
			
			
			
			
			
			
			
		'api' => [
			'create_token' => [
				'create_token' => "Create API Token",
				'token_name' => "Token Name",
				'assing_all_abilities' => "Assign All Abilities",
				'remove_all_abilities' => "Remove All Abilities",
				'token_can' => "Token Can",
				'create' => "Create",
				'api_token' => "API Token",
				'alert' => "Here is your new API token. <strong>This is the only time the token will ever
                            be displayed, so be sure not to lose it!</strong> You may revoke the token
                            at any time from your API settings.",
				'copy' => "Copy To Clipboard",
				'select_all' => "Select All",
				'close' => "Close"
			],
			'tokens' => [
				'api_tokens' => "API Tokens",
				'name' => "Name",
				'last_used' => "Last Used",
				'never' => "Never",
				'edit_token' => "Edit Token",
				'token_name' => "Token Name",
				'token_can' => "Token Can",
				'close' => "Close",
				'update_button' => "Update",
				'delete_token' => "Delete Token",
				'body' => 	"Are you sure you want to delete this token? If deleted, API requests that attempt to
                        	authenticate using this token will no longer be accepted.",
				'no' => "No, Go Back",
				'yes' => "Yes, Delete"
			]
		],
		'invoices' => [
			'invoice_list' => [
				'invoices' => "Invoices"
			],
			'update_extra' => [
				'extra_billing' => "Extra Billing Information",
				'alert' => "This information will appear on all of your receipts, and is a great place to add your full
			                business name, VAT number, or address of record. Do not include any confidential or
			                financial information such as credit card numbers.",
				'billing_updated' => "Your billing information has been updated",
				'update_button' => "Update"
			]
		],
		'security' => [
			'modals' => [
				'two_factor' => [
					'two_factor' => "Two-Factor Authentication Reset Code",
					'alert' => "If you lose your two-factor authentication device, you may use this
			                    emergency reset token to disable two-factor authentication on your account.
			                    <strong>This is the only time this token will be displayed, so be sure not
			                    to lose it!</strong>",
					'close' => "Close"
				]
			],
			'disable' => [
				'disabling' => "Disabling",
				'disable' => "Disable Two-Factor Authentication"
			],
			'enable' => [
				'two_factor' => "Two-Factor Authentication",
				'alert' => "In order to use two-factor authentication, you <strong>must</strong> install the
			                <strong><a href='https://authy.com' target='_blank'>Authy</a></strong> application
			                on your smartphone. Authy is available for iOS and Android.",
				'country_code' => "Country Code",
				'phone_number' => "Phone Number",
				'enabling' => "Enabling",
				'enable' => "Enable"
			],
			'update' => [
				'update_password' => "Update Password",
				'password_updated' => "Your password has been updated!",
				'current_password' => "Current Password",
				'password' => "Password",
				'confirm_password' => "Confirm Password",
				'update' => "Update"
			]
		],
		'subscription' => [
			'cancel' => [
				'cancel' => "Cancel Subscription",
				'are_you_sure' => "Are you sure you want to cancel your subscription?",
				'no' => "No, Go Back",
				'yes' => "Yes, Cancel",
				'cancelling' => "Cancelling"
			],
			'resume' => [
				'resume' => 'Resume Subscription',
				'monthly' => "Monthly",
				'yearly' => "Yearly",
				'you_have_cancelled' => "You have cancelled your subscription to the",
				'plan' => "plan",
				'benefits' => 	"",
				'vat' => "All subscription plan prices include applicable VAT.",
				'plan_features' => "Plan Features",
				'resuming_button' => "Resuming",
				'switch_button' => "Switch",
				'resume_button' => "Resume"
			],
			'trial' => [
				'free_trial' => "Free Trial",
				'you_on_trial' => "You are currently within your free trial period. Your trial will expire on"
			],
			'update' => [
				'update' => "Update Subscription",
				'monthly' => "Monthly",
				'yearly' => "Yearly",
				'you_are_currently' => "You are currently subscribed to the",
				'plan' => "plan",
				'vat_notice' => "All subscription plan prices include applicable VAT.",
				'plan_features' => "Plan Features",
				'free' => "Free",
				'current_plan' => "Current Plan",
				'switch' => "Switch",
				'updating' => "Updating",
				'update_subscription' => "Update Subscription",
				'are_you_want_switch' => "",
				'no' => "No, Go Back",
				'yes' => "Yes, I'm Sure."
			],
			'subscribe_stripe' => [
				'billing_information' => "Billing Information",
				'alert' => "We had trouble validating your card. It's possible your card provider is preventing
                    		us from charging the card. Please contact your card provider or customer support.",
				'billing_address' => "Billing Address",
				'credit_card' => "Credit Card",
				'cardholders_name' => "Cardholder's Name",
				'card_number' => "Card Number",
				'security_code' => "Security Code",
				'expiration' => "Expiration",
				'zip' => "ZIP / Postal Code",
				'coupon' => "Coupon",
				'tax' => "Tax",
				'total_tax' => "Total Price Including Tax",
				'subscribing' => "Subscribing",
				'subscribe' => "Subscribe"
			],
			'subscribe_common' => [
				'subscribe' => "Subscribe",
				'monthly' => "Monthly",
				'yearly' => "Yearly",
				'not_subscribed' => "You are not subscribed to a plan. Choose from the plans below to get started.",
				'vat_notice' => "All subscription plan prices are excluding applicable VAT.",
				'plan_features' => "Plan Features",
				'day_trial' => "Day Trial",
				'select' => "Select",
				'selected' => "Selected"
			],
			'subscribe_braintree' => [
				'billing_information' => "Billing Information",
				'alert' => "We had trouble validating your card. It's possible your card provider is preventing
                    		us from charging the card. Please contact your card provider or customer support.",
				'subscribing' => "Subscribing",
				'subscribe' => "Subscribe"
			],
			'subscribe_address' => [
				'address' => "Address",
				'address_2' => "Address Line 2",
				'city' => "City",
				'zip' => "State & ZIP / Postal Code",
				'state' => "State",
				'country' => "Country",
				'vat_id' => "VAT ID"
			]
		]
	],
		

	/**
	 * Profile
	 *
	 */
	
	'profile'		=> [
		'profile_photo'	=>	'Profile Photo',
		'select_new_photo'	=> 	'Select New Photo',
		'contact_information'	=> 'Contact Information',
		'contact_information_has_been_updated'	=> 	'Your contact information has been updated!',
		'name' => 'Name',
		'email_address'	=>	'E-Mail Address',
		'update'	=>	'Update'
	],
		

	/**
	 * Auth
	 *
	 */
	
	'auth'		=> [
		'passwords' => [
			'email'	=> [
				'reset_password' => "Reset Password",
				'email_address' => "E-mail Address",
				'send_password_reset' => "Send Password Reset Link"
			],
			'reset' => [
				'reset_password' => "Reset Password",
				'email_address' => "E-Mail Address",
				'password' => "Password",
				'confirm_password' => "Confirm Password"
			]
		],
			
		'login_emergency' => [
			'login_emergency' => "Login Via Emergency Token",
			'alert' =>	"After logging in via your emergency token, two-factor authentication will be
					disabled for your account. If you would like to maintain two-factor
					authentication security, you should re-enable it after logging in.",
			'token_emergency' => "Emergency Token",
			'login' => "Login"
		],
			
		'login' => [
			'login' => "Login",
			'email_address' => 'E-mail Address',
			'password' => "Password",
			'remember_me' => "Remember Me",
			'forgot_your_password' => "Forgot Your Password?"
		],
			
		'braintree' => [
			'billing' => "Billing",
			'alert' => "We had trouble validating your card. It's possible your card provider is preventing
						us from charging the card. Please contact your card provider or customer support.",
			'coupon_code' => "Coupon Code",
			'accept' => "I Accept The",
			'terms_of_service' => "Terms Of Service",
			'registring' => 'Registring',
			'register' => "Register"
		],
			
		'token' => [
			'two_factor_authentication' => "Two-Factor Authentication",
			'authentication_token' => "Authentication Token",
			'verify' => "Verify",
			'lost_device' => "Lost Your Device?"
		],
		
		'register'		=> [
			'coupons'	=>	[
				'discount'	=>	'Discount',
				'discount_message'	=>	"The coupon's :discount discount will be applied to your subscription!",
				'invalid'	=>	'Whoops! This coupon code is invalid.',
				'invitation'	=>	'We found your invitation to the',
				'invitation_invalid'	=>	'Whoops! This invitation code is invalid.'
			],
			'subscription'	=> 'Subscription',
			'monthly'	=> 'Monthly',
			'yearly'	=> 'Yearly',
			'vat'	=> 'All subscription plan prices are excluding applicable VAT.',
			'plan_features'	=>	'Plan Features',
			'free'	=>	'Free',
			'day_trial'	=> 	'Day Trial',
			'selected'	=> 'Selected',
			'select'	=> 'Select',
			'profile'	=> 'Profile',
			'register'	=> 'Register',
				
			'team_name'	=>	'Name',
			'slug'	=>	'Slug',
			'slug_defination'	=>	'This slug is used to identify your team in URLs.',
			'name'	=>	'Name',
			'email'	=>	'E-mail Address',
			'password'	=>	'Password',
			'confirm_password'	=>	'Confirm Password',
			'accept'	=>	'I Accept The',
			'terms_of_service'	=> 'Terms Of Service',
			'registering'	=>	'Registering'
		],
			
			
		'register_address'		=>	[
			'address'	=>	'Address',
			'address_line_2'	=>	'Address Line 2',
			'city'	=>	'City',
			'zip'	=>	'State & ZIP / Postal Code',
			'country'	=>	'Country',
			'vat_id'	=>	'VAT ID',
		],
			
		'stripe'	=>	[
			'billing_information'	=>	'Billing Information',
			'error_500'	=>	"We had trouble validating your card. It's possible your card provider is preventing us from charging the card. Please contact your card provider or customer support.",
			'billing_address'	=>	'Billing Address',
			'credit_card'	=>	'Credit Card',
			'credit_card_name'	=>	"Cardholder's Name",
			'credit_card_number'	=>	'Card Number',
			'credit_card_security_code'	=>	'Security Code',
			'credit_card_expiration'	=>	'Expiration',
			'zip'	=>	'ZIP / Postal Code',
			'coupon_code'	=>	'Coupon Code',
			'accept'	=>	'I Accept The',
			'terms_of_service'	=> 'Terms Of Service',
			'tax'	=>	'Tax:',
			'tax_total'	=>	'Total Price Including Tax:',
			'registering'	=>	'Registering',
			'register'	=>	'Register'
		]
	],
		
	'kiosk' => [
		'modals' => [
			'add_discount' => [
				'add_discount' => "Add Discount",
				'user_has_discount' => "This user has a discount of",
				'user_has_discount_for' => "for",
				'discount_type' => "Discount Type",
				'amount' => "Amount",
				'percentage' => "Percentage",
				'discount_duration' => "Discount Duration",
				'once' => "Once",
				'forever' => "Forever",
				'multiple_months' => "Multiple Months",
				'months' => "Months",
				'applying' => "Applying",
				'apply' => "Apply Discount"
			]
		],
		'announcements' => [
			'create' => "Create Announcement",
			'alert' => 'Announcements you create here will be sent to the "Product Announcements" section of
                    the notifications modal window, informing your users about new features and improvements
                    to your application.',
			'announcement' => "Announcement",
			'action_button_text' => "Action Button Text",
			'action_button_url' => "Action Button URL",
			'recent_announcement' => "Recent Announcements",
			'create_button' => "Create",
			'date' => "Date",
			'update' => "Update Announcement",
			'close' => "Close",
			'update_button' => "Update",
			'delete' => "Delete Announcement",
			'are_you_sure' => "Are you sure you want to delete this announcement?",
			'no' => "No, Go Back",
			'yes' => "Yes, Delete"
		],
		
			
		'metrics' => [
			'monthly_recurring_revenue' => "Monthly Recurring Revenue",
			'from_last_month' => "From Last Month",
			'from_last_year' => "From Last Year",
			'yearly_recurring_revenue' => "Yearly Recurring Revenue",
			'total_volume' => "Total Volume",
			'users_currently_trialing' => "Users Currently Trialing",
			'daily_volume' => "Daily Volume",
			'new_users' => "New Users",
			'subscribers' => "Subscribers",
			'name' => "Name",
			'trialing' => "Trialing",
			'on_generic_trial' => "On Generic Trial",
			'n_a' => "N/A"
		],
		'profile' => [
			'loading' => "Loading",
			'email_address' => "E-mail Address",
			'joined' => "Joined",
			'subscription' => "Subscription",
			'none' => "None",
			'total_revenue' => "Total Revenue",
			'name' => "Name"
		],
		'users' => [
			'search' => "Search By Name Or E-Mail Address...",
			'search_results' => "Search Results",
			'searching' => "Searching",
			'no_users_matched' => "No users matched the given criteria.",
			'name' => "Name",
			'email_address' => "E-mail Address"
		],
		'outer' => [
			'announcements' => "Announcements",
			'metrics' => "Metrics",
			'users' => "Users"
		] 
	],
		
	'modals' => [
		'support' => [
			'email_address' => "Your Email Address",
			'subject' => "Subject",
			'message' => "Message",
			'send' => "Send"
		],
		'session_expired' => [
			'session_expired' => "Session Expired",
			'please_login_agin' => "Your session has expired. Please login again to continue.",
			'go_to_login' => "Go To Login"
		],
		'plain_details' => [
			'close' => "Close"
		],
		'notifications' => [
			'notifications' => "Notifications",
			'announcements' => "Announcements",
			'loading_notifications' => "Loading Notifications",
			'alert' => "We don't have anything to show you right now! But when we do,
						we'll be sure to let you know. Talk to you soon!",
			'close' => "Close"
		]
	],
		
	'nav' => [
		'guest' => [
			'login' => "Login",
			'register' => "Register"
		],
		'subscriptions' => [
			'trial' => "Trial",
			'subscribe' => "Subscribe"
		],
		'support' => [
			'support' => "Support",
			'email_us' => "Email us"
		],
		'user' => [
			'impersonation' => "Impersonation",
			'back_to_my_acc' => "Back To My Account",
			'settings' => "Settings",
			'your_settings' => "Your Settings",
			'logout' => "Logout"
		]
	],
		
	'shared' => [
		'errors' => [
			'whoops' => "Whoops",
			'something_went_wrong' => "Something went wrong"
		]
	],
		
	
	'missing_team' => [
		'wheres_your' => "Where's Your",
		'it_looks' => "It looks like you're not part of any",
		'you_can_create' => "You can create one in your",
		'settings' => "settings"
	],
		
	'terms' => [
		'terms_of_service' => "Terms Of Service"
	]
];
