<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;
use Laravel;
use Carbon\Carbon;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'Make Your Box',
        'product' => 'Assinatura Mensal',
        'street' => 'Rua Dr. Mario Vianna, 277, apto 1701',
        'location' => 'Niterói, RJ 24241-002',
        'phone' => '(21) 99673-2848',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = null;

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'roboredo.bruno@gmail.com',
    	'makeyourbox1@gmail.com'
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
    	
        Laravel\Cashier\Cashier::useCurrency("brl", "R$ "); //setando a moeda
        
        Spark::collectBillingAddress(true);

        Spark::plan('Insane', 'plan-insane')
        ->price("169.90")
        ->features([
			'75 ¢oins',
        	'Acesso à vitrine',
			'Produtos exclusivos na vitrine',
			'Descontos exclusivos em lojas parceiras',
        	'Plano fidelidade'
        ]);

		Spark::plan('Survival', 'plan-survival')
		->price("85.90")
		->features([
			'30 ¢oins',
        	'Acesso à vitrine',
			'Descontos em lojas parceiras'
		]);
        
		/** 
		 * Validate Users
		 */
		Spark::validateUsersWith(function () {
			return [
				'name' => 'required|max:255',
				'cpf' => 'required|max:15',
				'email' => 'required|email|max:255|unique:users',
				'password' => 'required|confirmed|min:6',
				'terms' => 'required|accepted',
			];
		});

		/**
		 * Create Users
		 */
		Spark::createUsersWith(function ($request) {
			$user = Spark::user();
			
			$data = $request->all();
			
			$user->forceFill([
				'name' => $data['name'],
				'email' => $data['email'],
                'billing_address' => $data['address'],
                'billing_number' => $data['number'],
                'billing_address_line_2' => $data['address_line_2'],
                'billing_zip' => $data['zip'],
                'billing_city' => $data['city'],
                'billing_neighborhood' => $data['neighborhood'],
                'billing_state' => $data['state'],
                'billing_country' => $data['country'],
				'cpf' => $data['cpf'],
				'password' => bcrypt($data['password']),
				'last_read_announcements_at' => Carbon::now(),
				'trial_ends_at' => Carbon::now()->addDays(Spark::trialDays()),
			])->save();
			
			return $user;
		});
    }
}
