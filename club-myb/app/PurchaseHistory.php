<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseHistory extends Model
{

    public function products()
    {
        return $this->hasMany(PurchaseHistoriesProduct::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function amount()
    {
        $products = $this->products();
        $total = 0;

        foreach ($products->get() as $product)
        {
            $total += $product->product()->first()->coin_value;
        }

        return $total;
    }
}
