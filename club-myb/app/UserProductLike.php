<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProductLike extends Model
{
    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function product()
    {
        $this->belongsTo(Product::class);
    }
}
