<?php

namespace App;

use App\Myb\User as MybUser;
use Illuminate\Support\Facades\Auth;
use Laravel\Spark\Subscription;
use App\Myb\ExtractCoins;

class User extends MybUser
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
    	'cpf'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'authy_id',
        'country_code',
        'phone',
        'card_brand',
        'card_last_four',
        'card_country',
        'billing_address',
        'billing_address_line_2',
        'billing_city',
        'billing_zip',
        'billing_country',
        'extra_billing_information',
    	'cpf'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trial_ends_at' => 'date',
        'uses_two_factor_auth' => 'boolean',
    ];
	
    /**
     * Retorna valor de coins que o usuário possui
     * 
     * @return integer
     */
	public function coins()
	{
		$coins = $this->extract()->where('active', true)->sum('coin_value');

		
		$productsInBox = \App\Cart::where('user_id', $this->id);

		if(count($productsInBox->get()) > 0)
		{
			foreach ($productsInBox->get() as $productInBox)
			{
				$coins -= $productInBox->product->coin_value;
			}
		}
		
		return $coins;
	}
	
	/**
	 * Retorna collection de linhas do extrato do usuário
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function extract()
	{
		return $this->hasMany(ExtractCoins::class);
	}
	
	/**
	 * Verificar se usuário está vinculado ao Plano Insane
	 * 
	 * @return boolean
	 */
    public function isInsane()
    {
        return (strpos($this->subscription()->getProviderPlanAttribute(), 'insane') === false)  ? false : true;
    }

    public function isPlanYearly()
    {
        return (strpos($this->subscription()->provider_plan, 'yearly') === false)  ? false : true;
    }

	public function age()
    {
        return 18;
        //return $this->
    }

    public function myCart()
    {
        return $this->hasMany(Cart::class);
    }

    public function myCartTotal()
    {
        $items = $this->hasMany(Cart::class)->get();
        $total = 0;

        foreach ($items as $item) {
            $total += $item->product()->first()->coin_value;
        }

        return $total;
    }
    
    public function sobDemanda()
    {
    	$products = $this->hasMany(Cart::class)->get();
    	$return = false;
    	
    	foreach($products as $product) {
    		if($product->craft_product) {
    			$return = true;
    		}
    	}
    	
    	return $return;
    	
    }
}
