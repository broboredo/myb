<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseHistoriesProduct extends Model
{
    public function purchase()
    {
        return $this->belongsTo(PurchaseHistory::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
