<?php

namespace App;

use App\Myb\Product as MybProduct;
use App\Myb\Partner as MybPartner;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Product extends MybProduct
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'name',
			'description',
			'price',
			'img',
			'quantity',
			'purchase_price',
			'adults',
			'enable',
			'media',
            'vip',
            'craft_product',
            'release',
            'subscriber_details',
            'partner',
            'coin_value',
            'slug',
            'origin'
	];
	
	
	public function partners()
	{
		return $this->hasOne(MybPartner::class);
	}

	public function calculateQuantity()
    {
        return $this->quantity;
    }

    public function productInCart()
    {
        $user = Auth::user();

        $productInCart = Cart::where('user_id', $user->id)->where('product_id', $this->id)->get();

        if(count($productInCart) > 0)
            return true;
        else
            return false;
    }

    public function productLike()
    {
        $product = UserProductLike::where('user_id', Auth::user()->id)->where('product_id', $this->id)->first();

        if(is_null($product)) {
            return false;
        }

        return true;
    }

    public static function getActiveProducts()
    {
        $dateAfter = Carbon::now()->day(5)->setTime(0, 0, 0);
        $dateBefore = Carbon::now()->addMonth(1)->day(4)->setTime(0, 0, 0);
        if(Carbon::now()->day <= 5) {
            $dateAfter = Carbon::now()->addMonth(-1)->day(5);
            $dateBefore = Carbon::now()->day(4);
        }

        return Product::where('enable', true)
            ->where('release', '>=', $dateAfter)
            ->where('release', '<=', $dateBefore)->get();
    }
}
