<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'user_id',
			'product_id'
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [
			
	];
	
	public function product()
	{
		return $this->belongsTo(\App\Product::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\User::class);
	}
}
