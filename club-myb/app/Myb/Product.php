<?php

namespace App\Myb;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'vip',
			'craft_product',
			'release',
			'subscriber_details',
			'partner',
			'coin_value'
	];
}
