<?php

namespace App\Myb;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Partner extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'name',
			'company_name',
			'url',
			'address',
			'contract_end'
			
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [
			'cnpj',
			'partnership_type'
	];
	
	public function products()
	{
		return $this->hasMany(Product::class);
	}
}
