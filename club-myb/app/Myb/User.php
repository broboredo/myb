<?php

namespace App\Myb;

use Laravel\Spark\User as SparkUser;

class User extends SparkUser
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
			'date_birth',
			'gender',
			'called',
			'size_tshirt',
			'size_sandal',
			'console',
			'smartphone'
	];
}
