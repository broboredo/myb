<?php

namespace App\Myb;

use Illuminate\Database\Eloquent\Model;

class ExtractCoins extends Model
{
	
	public function user()
	{
		return $this->belongsTo(\App\User::class);
	}

	protected $fillable = ['coin_value', 'debit', 'user_id', 'details'];

	public static function values()
    {
        return [
            '5' => '5',
            '10' => '10',
            '15' => '15',
            '20' => '20',
            '25' => '25',
            '30' => '30',
            '35' => '35',
            '40' => '40',
            '45' => '45',
            '50' => '50',
            '55' => '55',
            '60' => '60',
            '65' => '65',
            '70' => '70',
            '75' => '75'
        ];
    }
}
