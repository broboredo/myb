<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Newsletter;
use Illuminate\Support\Facades\Session;

class NewsletterController extends Controller
{
	/**
	 * 
	 * @param Request $request
	 */
    public function store(Request $request)
    {	
    	$this->validate($request, [
			'email' => 'required|email'
    	]);
    	
    	Newsletter::create($request->except('_token'));
    	 
    	Session::flash('message', 'Aha! Então você está de olho no plano '.$request->prefer.'? Você não perde por esperar só mais um pouquinho.');
    	
    	return redirect()->back();
    }
}
