<?php

namespace App\Http\Controllers;

use App\Myb\Configuration;
use App\WaitingList;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Cashier\Subscription;
use Laravel\Spark\Plan;
use Laravel\Spark\Spark;
use Illuminate\Support\Facades\Input;
use App\Product;

class WelcomeController extends Controller
{

    public function wait() 
    {
        return view('wait');
    }

    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function show()
    {
        $products = Product::where('enable', true)
            ->where('adults',false)
            ->where('quantity', '>', 0)
            ->limit(4)
            ->inRandomOrder()
            ->get();

        return view('welcome')
        ->with('products', $products);
    }

    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function contact()
    {
        return view('site.contact');
    }

    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function questions()
    {
        return view('site.questions');
    }

    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function terms()
    {
        return view('site.terms');
    }

    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function returns()
    {
        return view('site.returns');
    }

    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function privacy()
    {
        return view('site.privacy');
    }

    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function about()
    {
        return view('site.about_us');
    }

    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function friends()
    {
        return view('site.friends');
    }

    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function register()
    {
        if(Configuration::all()->first()->waiting_lists == false) {
            if (Auth::check()) {
                return view('profile.dashboard');
            }

            return view('site.register');
        } else {
            return view('site.waiting_list');
        }
    }

    public function addToWaitingList(\Illuminate\Http\Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'plan_name' => 'required',
        ]);

        $found = WaitingList::where('email', $request->email)->first();

        if(is_null($found))
        {
            WaitingList::create($request->except('_token'));

            Session::flash('message', 'AHA, que legal! Você será um novo myber dentro de alguns dias');
        }else {
            $found->times = $found->times+1;
            $found->plan_name = $request->plan_name;
            $found->save();

            Session::flash('message', 'Seu e-mail já se encontra na lista. Aguarda só mais um pouquinho.');
        }


        return view('site.waiting_list');
    }

    /**
     * Show the application splash screen.
     *
     * @return Response
     */
    public function login()
    {
        if(Auth::check())
        {
            return view('profile.dashboard');
        }

        return view('site.login');
    }
    
    public function forgot()
    {
    	return view('site.forgot');
    }
    
    public function reset()
    {
    	$token = isset(Input::all()['token']) ? Input::all()['token'] : null;
    	
    	if(!is_null($token)) {
    		return view('site.reset')->with('token', $token);
    	} 
    	
    	return redirect()->back();
    }
    
    public function vitrine() 
    {
    	$products = Product::where('enable', true)
    	->where('adults',false)
    	->orderBy('coin_value', 'asc')
    	->inRandomOrder()
    	->get()
    	;
    	
    	return view('site.vitrine')
    	->with('products', $products);
    }
}
