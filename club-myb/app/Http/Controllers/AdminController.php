<?php

namespace App\Http\Controllers;

use App\Myb\Configuration;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        return view('admin.index');
    }

    public function product()
    {
        $years = ['2017' => '2017', '2018' => '2018', '2019' => '2019'];
        $months = ['1' => 'Janeiro', '2' => 'Fevereiro', '3' => 'Março', '4' => 'Abril', '5' => 'Maio', '6' => 'Junho',
        '7' => 'Julho', '8' => 'Agosto', '9' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro'];

        return view('admin.products.product')
        ->with('product', null)
        ->with('months', $months)
        ->with('years', $years);
    }

    public function editProduct($id)
    {
        $years = ['2017' => '2017', '2018' => '2018', '2019' => '2019'];
        $months = ['1' => 'Janeiro', '2' => 'Fevereiro', '3' => 'Março', '4' => 'Abril', '5' => 'Maio', '6' => 'Junho',
            '7' => 'Julho', '8' => 'Agosto', '9' => 'Setembro', '10' => 'Outubro', '11' => 'Novembro', '12' => 'Dezembro'];

        $product = Product::findOrFail($id);

        return view('admin.products.product')
        ->with('product', $product)
        ->with('months', $months)
        ->with('years', $years);
    }

    public function products()
    {
        $allProducts = Product::all();
        $activeProducts = Product::getActiveProducts();

        return view('admin.products.products')
        ->with('allProducts', $allProducts)
        ->with('activeProducts', $activeProducts);
    }

    public function config()
    {
        return view('admin.config.config');
    }

    public function enableWaitingLists()
    {
        $c = Configuration::all()->first();
        $c->waiting_lists = true;

        if ($c->save()) {
            return view('admin.config.config')
            ->with('success', "Lista de Espera Habilitada");
        }

        return view('admin.config.config')
        ->withErrors(['Houve um erro']);
    }

    public function disableWaitingLists()
    {
        $c = Configuration::all()->first();
        $c->waiting_lists = false;

        if ($c->save()) {
            return view('admin.config.config')
                ->with('success', "Lista de Espera Desabilitada");
        }

        return view('admin.config.config')
            ->withErrors(['Houve um erro']);
    }

    public function openShop()
    {
        $users = User::where('purchased', true)->get();

        foreach ($users as $user)
        {
            $user->purchased = false;
            $user->save();
        }

        return view('admin.config.config')
            ->with('success', "Mercado Aberto");
    }
}
