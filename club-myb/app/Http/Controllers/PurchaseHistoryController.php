<?php

namespace App\Http\Controllers;

use App\PurchaseHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;

class PurchaseHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.box.purchase_histories')
        ->with('purchases', PurchaseHistory::all());
    }

    public function addNumberShipping($id)
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = PurchaseHistory::find($id);

        return view('admin.box.add_number_ship')
            ->with('purchase', $purchase);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(!is_null($request->number_ship)) {
            $purchase = \App\PurchaseHistory::find($request->id);
            $purchase->status = "Enviado";
            $purchase->number_ship = $request->number_ship;

            if ($purchase->save()) {

                \App\Cart::where('user_id', $purchase->user_id)->delete();

                Mail::send('mail.send_box', ['purchase' => $purchase], function ($message) use ($purchase) {
                    $message
                        ->from(env('MAIL_FROM_ADDRESS'))
                        ->to($purchase->user()->first()->email)
                        ->subject('Make Your Box | Sua box foi enviada!')
                    ;
                });

                Session::flash('message', 'Alterado status com sucesso!');
                return redirect('purchase.edit');
            }
        } else {
            return redirect()->back()->withErrors(['O Código de Rastreio não pode ser vazio']);
        }

        return $this->index();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
