<?php

namespace App\Http\Controllers;

use App\UserProductLike;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Types\Integer;
use App\Product;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* TODO:
         * Produto "não pode" ser exibido/clicavel ser estiver inativo;
         * Produto VIP só pode ser exibido para assinantes VIP (pode ser
         * exibido, porém ficará com um "opacity");
         * Produto não pode ser exibido se estiver com quantidade <= 0
         * (pode ser exibido, porém com uma "tarja de esgotado" e com "botão que queria");
         * 
         */
    	
    	return view('admin.products.products')->with('products', Product::getActiveProducts());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	//method to render the product form's
    	
    	return view('producst.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$request->request->add(['coin_value' => $this->generateCoin($request->purchase_price)]);

    	$request->request->add(['release' => Carbon::now()->day(5)->month($request->month)->year($request->year)->setTime(0, 0, 0)]);

    	if(is_null($request)) {
            $this->validate($request, [
                'name' => 'required|max:255|unique:products',
                'price' => 'required|numeric',
                'purchase_price' => 'required|numeric',
                'quantity' => 'numeric',
                'adults' => 'boolean',
                'enable' => 'boolean',
                'subscriber_details' => 'boolean',
                'coin_value' => 'required|numeric|min:5',
                'vip' => 'boolean',
                'craft_product' => 'boolean',
                'release' => 'required|date',
            ]);
        } else {
            $this->validate($request, [
                'price' => 'required|numeric',
                'purchase_price' => 'required|numeric',
                'quantity' => 'numeric',
                'adults' => 'boolean',
                'enable' => 'boolean',
                'subscriber_details' => 'boolean',
                'coin_value' => 'required|numeric|min:5',
                'vip' => 'boolean',
                'craft_product' => 'boolean',
                'release' => 'required|date',
            ]);
        }

        $request->request->add(['slug' => str_slug($request->name, '-')]);

    	$path = storage_path() . '/app/public/products/';
    	$fileName = md5($request->file('img')->getClientOriginalName()) . '_' . $request->file('img')->getClientOriginalName();
    	$request->file('img')->move($path, $fileName);

    	$newRequest = $request->except('_token');
        $newRequest['img'] = $fileName;

		Product::create($newRequest);

		Session::flash('message', 'Successfully created nerd!');

		return redirect()->route('admin-products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //method to render the product edit form's
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //method to update the data
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = \App\Product::find($id);
        $product->delete();
        
        Session::flash('message', 'Deletado com sucesso');
        redirect('products');
    }
    
    /**
     * @param float $price
     * @return Integer
     */
    public function generateCoin(float $price)
    {
    	if($price <= (float)7.50) {
    		return 5;
    	} else if(($price >= (float)7.51) && ($price <= (float) 15.50)) {
    		return 10;
    	} else if(($price >= (float)15.51) && ($price <= (float)23.00)) {
    		return 15;
		} else if(($price >= (float)23.01) && ($price <= (float)30.75)) {
			return 20;
		} else if(($price >= (float)30.76) && ($price <= (float)38.45)) {
			return 25;
		} else if(($price >= (float)38.46) && ($price <= (float)46.15)) {
			return 30;
		} else if(($price >= (float)46.16) && ($price <= (float)53.80)) {
			return 35;
		} else if(($price >= (float)53.81) && ($price <= (float)61.50)) {
			return 40;
		} else if(($price >= (float)61.51) && ($price <= (float)69.20)) {
			return 45;
		} else if(($price >= (float)69.21) && ($price <= (float)76.90)) {
			return 50;
		} else if(($price >= (float)76.91) && ($price <= (float)84.60)) {
			return 55;
		} else if(($price >= (float)84.61) && ($price <= (float)92.30)) {
			return 60;
		} else if(($price >= (float)92.31) && ($price <= (float)100.00)) {
			return 65;
		} else if(($price >= (float)100.01) && ($price <= (float)107.65)) {
			return 70;
		} else if(($price >= (float)107.66) && ($price <= (float)115.00)) {
			return 75;
		} else {
			return 80;
		}
    }
    
    /**
     * Remove 1 quantity from product
     * 
     * @param $id
     * @return void
     */
    public static function removeQuantity($id)
    {
    	$product = \App\Product::find($id);
    	$product->quantity -= 1;
    	$product->update();
    }

    /**
     * Add 1 quantity for product
     *
     * @param $id
     * @return void
     */
    public static function addQuantity($id)
    {
    	$product = \App\Product::find($id);
    	$product->quantity += 1;
    	$product->update();
    }

    public function likeProduct()
    {
        $id = Input::all()['id'];

        if(is_null(UserProductLike::where('product_id', $id)->where('user_id', Auth::user()->id)->first())) {

            $likeProduct = new UserProductLike();
            $likeProduct->user_id = Auth::user()->id;
            $likeProduct->product_id = $id;

            if ($likeProduct->save()) {
                return json_encode(['status' => 1, 'message' => 'Este produto agora está na sua lista de desejos!']);
            }

            return json_encode(['status' => 0, 'message' => 'Ocorreu um erro ao enviar este produto à sua lista de desejos']);
        }

        return json_encode(['status' => 0, 'message' => 'Esse produto já se encontra na sua lista de desejos']);

    }

    public function unlikeProduct()
    {
        $id = Input::all()['id'];
		$unlikeProduct = UserProductLike::where('product_id', $id)->where('user_id', Auth::user()->id)->first();
        
        if($unlikeProduct->delete()) {
            return json_encode(['status' => 1, 'message' => 'Este produto foi removido da sua lista de desejos!']);
        }

        return json_encode(['status' => 0, 'message' => 'Ocorreu um erro ao remover este produto da sua lista de desejos']);
    }

    public function like()
    {
        $id = Input::all()['id'];
		$product = UserProductLike::where('product_id', $id)->where('user_id', Auth::user()->id)->first();
        
        if(is_null($product)) {
        	$likeProduct = new UserProductLike();
        	$likeProduct->user_id = Auth::user()->id;
        	$likeProduct->product_id = $id;
        	
        	if ($likeProduct->save()) {
        		return json_encode(['status' => 1, 'message' => 'Este produto agora está na sua lista de desejos!']);
        	}
        } else {
        	if($product->delete()) {
        		return json_encode(['status' => 1, 'message' => 'Este produto foi removido da sua lista de desejos!']);
        	}
        }

        return json_encode(['status' => 0, 'message' => 'Ocorreu um erro ao remover este produto da sua lista de desejos']);
    }
}
