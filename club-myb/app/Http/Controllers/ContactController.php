<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response[
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'max:255',
            'phone' => 'max:50',
            'email' => 'required',
            'message' => 'required',
            'subject' => 'required',
        ]);

        Contact::create($request->except('_token'));
        
		$this->sendMail($request->except('_token'));

        Session::flash('message', 'Seu contato foi enviado com sucesso! Retornaremos o mais breve possível.');
        return view('site.contact');
    }
    
    public function sendMail($request)
    {
    	Mail::send('mail.admin', ['nome' => $request['name'], 'email' => $request['email'], 'msg' => $request['message'], 'assunto' => $request['subject'], 'telefone' => $request['phone']], function ($message) use ($request) {
			$message
			->from($request['email'])
			->to("makeyourbox1@gmail.com")
			->subject("CONTATO | ".$request['subject']);
    	});
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
