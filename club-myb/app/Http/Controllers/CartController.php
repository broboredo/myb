<?php

namespace App\Http\Controllers;

use App\PurchaseHistoriesProduct;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Cart;
use App\User;
use App\PurchaseHistory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

class CartController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('subscribed');
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productsInCart = \App\Cart::where('user_id', Auth::user()->id);
    	
    	return view('user.cart')>with('cart', $productsInCart);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	try {
    		
    		$user = Auth::user();
    		$product = \App\Product::find($request->id);
    		
    		if($this->checkValidConditions($product))
    		{	
    			$cart = new Cart();
    			$cart->user_id = $user->id;
    			$cart->product_id = $request->id;
    			$cart->save();
    	
    			//removendo quantidade do produto
    			\App\Http\Controllers\ProductController::removeQuantity($product->id);
    			 
    			return json_encode(["status" => 1, "message" => "Produto inserido na box"]);
    		} else {

    		    $errorMessage = "O item não foi inserido na box. <br /> 
                                Verifique: <br />
                                - Se o item está esgotado; <br />
                                - Se você possui coins suficiente para a compra; <br />
                                - Se você já fechou a box desse mês;";

                return json_encode(["status" => 0, "message" => $errorMessage]);
    		}
    	} catch(\Exception $e) {  
    		return json_encode("Shiii! Algum erro cabrero! Entre em contato com a administração");
    	}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	if(Auth::user())
    	{	
	    	$productInCart = \App\Cart::find($id);
            $product = $productInCart->product_id;

	    	if($productInCart->delete())
	    	{
				\App\Http\Controllers\ProductController::addQuantity($product);

				Session::flash('message', 'Produto removido da box');
	    		return view('cart');
	    		
	    	} else {
	    		Session::flash('message', 'Ocorreu um erro');
	    		return view('cart.edit');
	    	}
    	} else {
    		Session::flash('message', "Sem permissão para esta requisição");
    		return view('cart');
    	}
    }

    public function removeAll()
    {
        if(Auth::user())
        {
            $productsInCart = \App\Cart::where('user_id', Auth::user()->id)->get();

            foreach ($productsInCart as $productInCart)
            {

                if($productInCart->delete())
                {
                    \App\Http\Controllers\ProductController::addQuantity($productInCart->product()->first()->id);

                } else {
                    Session::flash('message', 'Ocorreu um erro');
                    return redirect()->route('my-box');
                }
            }

            if(count(\App\Cart::where('user_id', Auth::user()->id)->get()) <= 0)
            {
                Session::flash('message', 'Produto removido da box');
                return redirect()->route('my-box');
            }

        } else {
            Session::flash('message', "Sem permissão para esta requisição");
            return redirect()->route('my-box');
        }
    }


    public function removeProduct($id)
    {
        if(Auth::user())
        {
            $productInCart = \App\Cart::where('product_id', $id)->where('user_id', Auth::user()->id);
            $product = $productInCart->first()->product_id;

            if($productInCart->delete())
            {

                \App\Http\Controllers\ProductController::addQuantity($product);

                return json_encode(['status' => 1, 'message' => 'Produto removido da box']);

            } else {
                return json_encode(['status' => 0, 'message' => 'Ocorreu um erro']);
            }
        } else {
            return json_encode(['status' => 0, 'message' => 'Sem permissão para esta requisição']);
        }
    }


    public function removeProductMyBox($id)
    {
        if(Auth::user())
        {
            $productInCart = \App\Cart::where('product_id', $id)->where('user_id', Auth::user()->id);
            $product = $productInCart->first()->product_id;

            if($productInCart->delete())
            {

                \App\Http\Controllers\ProductController::addQuantity($product);

                Session::flash('message', 'Produto removido da box');
                return redirect()->route('my-box');

            } else {
                Session::flash('message', 'Ocorreu um erro');
                return redirect()->route('my-box');
            }
        } else {
            Session::flash('message', "Sem permissão para esta requisição");
            return redirect()->route('my-box');
        }
    }
    
    
    public function productToggle()
    {
    	$id = Input::all()['id'];
    	
    	if(Auth::user())
    	{
    		$productInCart = \App\Cart::where('product_id', $id)->where('user_id', Auth::user()->id);
    		if(!is_null($productInCart->first())) { 
	    		$product = $productInCart->first()->product_id;
	    	
	    		if($productInCart->delete())
	    		{
	    	
	    			\App\Http\Controllers\ProductController::addQuantity($product);
	    			
	    			return json_encode(['status' => 1, 'message' => 'Produto removido da box']);
	    			
    			} else {
    				return json_encode(['status' => 0, 'message' => 'Ocorreu um erro']);
    			}
    		} else {
    			
    			$product = \App\Product::find($id);
    			
    			if($this->checkValidConditions($product))
    			{
    				$cart = new Cart();
    				$cart->user_id = Auth::user()->id;
    				$cart->product_id = $product->id;
    				$cart->save();
    				 
    				//removendo quantidade do produto
    				\App\Http\Controllers\ProductController::removeQuantity($product->id);
    			
    				return json_encode(["status" => 1, "message" => "Produto inserido na box"]);
    			} else {
    			
    				$errorMessage = "O item não foi inserido na box. <br />
                                Verifique: <br />
                                - Se o item está esgotado; <br />
                                - Se você possui coins suficiente para a compra; <br />
                                - Se você já fechou a box desse mês;";
    			
    				return json_encode(["status" => 0, "message" => $errorMessage]);
    			}
    			
    			
    		}
    	} else {
    		Session::flash('message', "Sem permissão para esta requisição");
    		return redirect()->route('my-box');
    	}
    	
    }
    
    /**
     * 
     * Check if product exists in cart
     * 
     * return true if exists product in cart
     * return false if don't exists product in cart
     * 
     * @param $productId
     * @return boolean
     */
    public function checkProductInCart($productId)
    {
    	$user = Auth::user();
    	
    	$productInCart = \App\Cart::where('user_id', $user->id)->where('product_id', $productId)->get();
    	
    	if(count($productInCart) > 0)
    		return true;
    	else
    		return false;
    }
    
    /**
     * Checkout the purchase of items contained in the cart
     * 
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function checkoutCart()
    {
        Session::clearResolvedInstances();

    	$user = Auth::user();
    	
    	$productsInCart = \App\Cart::where('user_id', $user->id)->get();

    	if(count($productsInCart) > 0) {

            $date = Carbon::now()->format('d/m/Y H:i');


            $purchaseHistory = new PurchaseHistory();
            $purchaseHistory->user_id = $user->id;
            $purchaseHistory->status = "Processando";
            $purchaseHistory->save();

            foreach ($productsInCart as $productInCart) {
                $purchaseHistoryProduct = new PurchaseHistoriesProduct();
                $purchaseHistoryProduct->product_id = $productInCart->product_id;
                $purchaseHistoryProduct->purchase_history_id = $purchaseHistory->id;
                $purchaseHistoryProduct->save();

                $product = $productInCart->product()->first();

                $extract = new \App\Myb\ExtractCoins();
                $extract->user_id = $productInCart->user_id;
                $extract->coin_value = $product->coin_value * -1;
                $extract->debit = false;
                $extract->details = "Compra do item " . $product->name . ", no valor de ¢ " . $product->coin_value . ". No dia " . $date;
                $extract->save();

                $productInCart->delete();
            }

            $user = User::find(Auth::user()->id);
            $user->purchased = true;
            $user->save();
            

            	Mail::send('mail.checkout', ['nome' => $user->name, 'email' => $user->email, 'products' => $productsInCart, 'user'=>$user], function ($message) use ($user) {
            		$message
            		->from($user->email)
            		->to("makeyourbox1@gmail.com")
            		->subject("!!! FECHAMENTO DE BOX !!! ");
            	});
            

            	Mail::send('mail.user_checkout', ['nome' => $user->name, 'email' => $user->email, 'products' => $productsInCart, 'user'=>$user], function ($message) use ($user) {
            		$message
            		->from(env('MAIL_FROM_ADDRESS'))
            		->to($user->email)
            		->subject("Make Your Box | FECHAMENTO DE BOX");
            	});
            
        } else {
            Session::flash('message', 'Você não têm produtos no carrinho');
        }

        return redirect()->route('history');
    }
    
    /**
     * 
     * TODO: Melhorar o "came-came-há" (melhorar o formato das condições)
     * 
     * Valida a inclusão de produto no carrinho
     * 
     * @param $product
     * @return boolean
     */
    private function checkValidConditions($product)
    {
        $user = Auth::user();

        if (!$user->purchased) {
            if ($product->enable) {
                if ($product->quantity > 0) {
                    if ($user->coins() >= $product->coin_value) {
                        if (!$this->checkProductInCart($product->id)) {
                            if ($product->vip) {
                                if ($user->isInsane()) {
                                    return true;
                                }

                            } else {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    	 
    	return false;
    }
}
