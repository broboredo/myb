<?php

namespace App\Http\Controllers\Myb;

use App\Myb\ExtractCoins;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExtractCoinsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.coins.extract')
        ->with('coins', ExtractCoins::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coins.add_coins')
        ->with('values', ExtractCoins::values())
        ->with('users', User::all()->pluck('name', 'id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['debit' => true]);

        $this->validate($request, [
            'details' => 'required',
            'user_id' => 'required',
            'coin_value' => 'required',
        ]);

        $newRequest = $request->except('_token');
        ExtractCoins::create($newRequest);

        return redirect()->route('admin-coins');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit()
    {
        return view('admin.coins.remove_coins')
            ->with('values', ExtractCoins::values())
            ->with('users', User::all()->pluck('name', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function removeCoins(Request $request)
    {
        $request->request->add(['debit' => false]);

        $creditCoinValue = $request->coin_value * -1;
        $request->request->remove('coin_value');
        $request->request->add(['coin_value' => $creditCoinValue]);

        $this->validate($request, [
            'details' => 'required',
            'user_id' => 'required',
            'coin_value' => 'required',
        ]);

        $newRequest = $request->except('_token');
        ExtractCoins::create($newRequest);

        return redirect()->route('admin-coins');
    }
}
