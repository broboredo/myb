<?php

namespace App\Http\Controllers;

use App\Product;
use App\PurchaseHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('subscribed')->except('changePlan');
    }

    public function profile()
    {
        /* $msg = "Parabéns! Você acaba de desbloquear sua primeira conquitas, por ter feito seu primeiro acesso. 
        Complete o seu cadastro. Nos ajude a te dar uma melhor experiência em nossa plataforma!";
        Session::flash('alert-message', $msg);
        Session::flash('alert', "warning"); */

        return view('profile.dashboard');
    }

    public function vitrine()
    {
        $products = Product::getActiveProducts();

        return view('profile.vitrine')
            ->with('products', $products);
    }

    public function productPage($slug)
    {
        $product = Product::where('slug', $slug)
            ->where('enable', true)
            ->first();

        if(!is_null($product)) {
            return view('profile.product')
                ->with('product', $product);
        }

        return redirect()->back();
    }

    public function editProfile()
    {

        return view('profile.edit-profile');
    }

    public function cancel()
    {

        return view('profile.cancel');
    }

    public function changePlan()
    {

        return view('profile.change-plan');
    }

    public function myBox()
    {

        $box = Auth::user()->myCart()->get();
        $status = PurchaseHistory::where('user_id', Auth::user()->id)
            ->limit(1)
            ->orderBy('id', 'desc')
            ->first();

        if(!is_null($status)) {
            $status = $status->status;
        } else {
            $status = "ABERTO";
        }

        return view('profile.my-box')
            ->with('items', $box)
            ->with('status', $status);
    }

    public function history()
    {

        $item = PurchaseHistory::where('user_id', Auth::user()->id)->get();

        return view('profile.history')->with('purchase', $item);
    }

    public function planDetails()
    {
        $subscription = Auth::user()->subscription();

        return view('profile.details-plan')->with('subscription', $subscription);
    }

    public function fidelity()
    {
    	if(Auth::user()->isInsane())
    	{
        	return view('profile.fidelity');
    	}
    	
    	return redirect()->back();
    }
}
