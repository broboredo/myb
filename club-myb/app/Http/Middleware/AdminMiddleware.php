<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->email === 'makeyourbox1@gmail.com' || Auth::user()->email === "roboredo.bruno@gmail.com") {
            return $next($request);
        }

        return redirect()->back();
    }
}
