<?php

namespace App\Listeners\Myb;

use Illuminate\Support\Facades\Mail;
use Laravel\Spark\Events\Subscription\SubscriptionUpdated;
use App\Mail\UserSubscribedUpdated;

class MybUserSubscribedUpdated
{

    /**
     * Handle the event.
     *
     * @param  SubscriptionUpdated  $event
     * @return void
     */
    public function handle(SubscriptionUpdated $event)
    {
        Mail::send(new UserSubscribedUpdated($event->user));
    }
}
