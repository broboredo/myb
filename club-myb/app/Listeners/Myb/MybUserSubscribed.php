<?php

namespace App\Listeners\Myb;

use Laravel\Spark\Events\Subscription\UserSubscribed;
use App\Myb\ExtractCoins;
use Illuminate\Support\Facades\Mail;

class MybUserSubscribed
{
    /**
     * Handle the event.
     *
     * @param  UserSubscribed  $event
     * @return void
     */
    public function handle(UserSubscribed $event)
    {    	 
    	$user = $event->user;    	
    	$plan = $event->plan;

    	$coins = 30;
    	$name_plan = "Survival";
    	if(strpos(strtolower($plan->id), strtolower("insane"))
        || strtolower($plan->name) ==  strtolower("insane")) {
            $coins = 75;
            $name_plan = "Insane";
        }
    	 
    	$newExtract = new ExtractCoins();
    	$newExtract->user_id = $user->id;
    	$newExtract->coin_value = $coins;
    	$newExtract->debit = true;
    	$newExtract->details = "Assinatura do Plano ".$name_plan;    	
    	
    	if($newExtract->save()) {

    		try{ 
		    	Mail::send('mail.welcome', ['address' => $event->user->email, 'user' => $event->user, 'plan' => $name_plan], function ($message) use ($event) {
		    		$message
		    		->from(env('MAIL_FROM_ADDRESS'))
		    		->to($event->user->email)
		    		->subject('Make Your Box | Seja Bem Vindo, Myber')
		    		;
		    	});
		    		 
		    		Mail::send('mail.welcome_admin', ['address' => $event->user->email, 'user' => $event->user, 'plan' => $name_plan], function ($message) use ($event) {
		    			$message
		    			->from($event->user->email)
		    			->to("makeyourbox1@gmail.com")
		    			->subject('!!! NOVO MYBER !!!')
		    			;
		    		});
    		} catch (\Exception $e) {
    			//
    		}
    	}
    }
}
