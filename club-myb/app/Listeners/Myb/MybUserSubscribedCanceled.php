<?php

namespace App\Listeners\Myb;

use Illuminate\Support\Facades\Mail;
use Laravel\Spark\Events\Subscription\SubscriptionCancelled;
use App\Cart;

class MybUserSubscribedCanceled
{
	/**
	 * Handle the event.
	 *
	 * @param  SubscriptionUpdated  $event
	 * @return void
	 */
	public function handle(SubscriptionCancelled $event)
	{
		//Mail::send(new UserSubscribedUpdated($event->user));
		
		$products = Cart::where('user_id', $event->user->id)->get();
		if(!empty($products)) {
			foreach ($products as $product) {
				$product->remove();
			}
		}
		
		
		Mail::send('mail.cancel', ['user' => $event->user], function ($message) use ($event) {
			$message
			->from(env('MAIL_FROM_ADDRESS'))
			->to($event->user->email)
			->subject('Cancelamento de Plano')
			;
		});
			 
		Mail::send('mail.cancel_admin', ['user' => $event->user], function ($message) use ($event) {
			$message
			->from($event->user->email)
			->to("makeyourbox1@gmail.com")
			->subject('Cancelamento de Plano')
			;
		});
	}
}
