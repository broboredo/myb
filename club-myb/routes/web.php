<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/login', 'WelcomeController@login')->name('login');
Route::get('/*', 'WelcomeController@wait');

/* SITE */
Route::get('/', 'WelcomeController@show')->name('home');
Route::get('/contato', 'WelcomeController@contact')->name('contact');
Route::post('/contato/add', 'ContactController@store');
Route::get('/termos-de-uso', 'WelcomeController@terms')->name('terms');
Route::get('/perguntas-frequentes', 'WelcomeController@questions')->name('questions');
Route::get('/trocas-e-devolucoes', 'WelcomeController@returns')->name('returns');
Route::get('/politica-de-privacidade', 'WelcomeController@privacy')->name('privacy');
Route::get('/sobre-nos', 'WelcomeController@about')->name('about');
Route::get('/aliados', 'WelcomeController@friends')->name('friends');
Route::get('/assinar', 'WelcomeController@register')->name('register');
Route::post('/assinar/aguarde', 'WelcomeController@addToWaitingList');
Route::get('/login', 'WelcomeController@login')->name('login');
Route::get('/register', 'WelcomeController@register')->name('spark-register');
Route::get('/esqueci-a-senha', 'WelcomeController@forgot')->name('forgot');
Route::get('/redefinir-senha/{token?}', 'WelcomeController@reset')->name('reset');
Route::post('/newsletter', 'NewsletterController@store');
Route::get('/vitrine', 'WelcomeController@vitrine')->name('site-vitrine');

/* PROFILE */
Route::get('/home', 'DashboardController@profile')->middleware('auth')->middleware('subscribed');
Route::get('/profile', 'DashboardController@profile')->name('profile')->middleware('auth')->middleware('subscribed');
Route::get('/profile/vitrine', 'DashboardController@vitrine')->name('vitrine')->middleware('auth')->middleware('subscribed');
Route::get('/profile/editar-perfil', 'DashboardController@editProfile')->name('edit-profile')->middleware('auth')->middleware('subscribed');
Route::get('/profile/cancelar-assinatura', 'DashboardController@cancel')->name('cancel')->middleware('auth')->middleware('subscribed');
Route::get('/profile/trocar-de-plano', 'DashboardController@changePlan')->name('change-plan')->middleware('auth');
Route::get('/profile/vitrine/produto/{slug}', 'DashboardController@productPage')->name('product-page')->middleware('auth')->middleware('auth')->middleware('subscribed');
Route::get('/profile/minha-box', 'DashboardController@myBox')->name('my-box')->middleware('auth')->middleware('auth')->middleware('subscribed');
Route::get('/profile/my-box/checkout', 'CartController@checkoutCart')->name('checkout')->middleware('auth')->middleware('subscribed');
Route::get('/profile/historico', 'DashboardController@history')->name('history')->middleware('auth')->middleware('subscribed');
Route::get('/profile/beneficios-do-plano', 'DashboardController@planDetails')->name('plan-details')->middleware('auth')->middleware('subscribed');
Route::get('/profile/controle-de-fidelidade', 'DashboardController@fidelity')->name('fidelity')->middleware('auth')->middleware('subscribed');//->middleware('insane');


/* ADMIN */
Route::get('/admin', 'AdminController@index')->name('admin')->middleware('admin');
//admin-PRODUCT
Route::get('/admin/product', 'AdminController@product')->name('admin-product')->middleware('admin');
Route::get('/admin/product/{id}', 'AdminController@editProduct')->name('admin-edit-product')->middleware('admin');
Route::get('/admin/products', 'AdminController@products')->name('admin-products')->middleware('admin');
//admin-COINS
Route::get('/admin/coins', 'Myb\ExtractCoinsController@index')->name('admin-coins')->middleware('admin');
Route::get('/admin/add-coins', 'Myb\ExtractCoinsController@create')->name('admin-add-coins')->middleware('admin');
Route::get('/admin/remove-coins', 'Myb\ExtractCoinsController@edit')->name('admin-remove-coins')->middleware('admin');
Route::post('/admin/coins/remove', 'Myb\ExtractCoinsController@removeCoins')->middleware('admin');
//admin-CONFIG
Route::get('/admin/config', 'AdminController@config')->name('admin-general-config')->middleware('admin');
Route::post('/admin/config/enable-waiting-lists', 'AdminController@enableWaitingLists')->middleware('admin');
Route::post('/admin/config/disable-waiting-lists', 'AdminController@disableWaitingLists')->middleware('admin');
Route::post('/admin/config/open-shop', 'AdminController@openShop')->middleware('admin');
//admin-BOX
Route::get('/admin/purchase', 'PurchaseHistoryController@index')->name('admin-purchase')->middleware('admin');
Route::get('/admin/purchase/{id}', 'PurchaseHistoryController@show')->name('admin-purchase-add-number-ship')->middleware('admin');
Route::post('/admin/purchase/number-ship', 'PurchaseHistoryController@update')->name('admin-purchase-edit-add-number-ship')->middleware('admin');


Route::get('/user/coins', function (){
    return Auth::user()->coins();
});

Route::group(['namespace' => 'Myb'], function() {
	Route::resource('partners', 'PartnerController');
});

Route::group(['middleware' => 'auth'], function() {
    Route::resource('products', 'ProductController');

    Route::get('products/list', 'ProductController@list');
});

Route::group(['middleware' => 'auth'], function() {
    Route::resource('coins', 'Myb\ExtractCoinsController');
});

Route::post('product/like', 'ProductController@likeProduct')->name('like-product')->middleware('auth')->middleware('subscribed');
Route::post('product/unlike', 'ProductController@unlikeProduct')->name('unlike-product')->middleware('auth')->middleware('subscribed');
Route::post('product/like-this', 'ProductController@like')->name('like')->middleware('auth')->middleware('subscribed');

Route::group(['middleware' => ['auth']], function() {
	Route::resource('cart', 'CartController');
});
Route::get('/profile/my-box/cart/product/{id}', 'CartController@removeProductMyBox')->name('remove-product-cart-mybox')->middleware('auth')->middleware('subscribed');
Route::delete('/product-cart/{id}', 'CartController@removeProduct')->name('remove-product-cart')->middleware('auth')->middleware('subscribed');
Route::post('/product/toggle', 'CartController@productToggle')->name('product-toggle')->middleware('auth')->middleware('subscribed');
Route::get('/profile/my-box/cart/limpar-box', 'CartController@removeAll')->name('remove-all')->middleware('auth')->middleware('subscribed');
