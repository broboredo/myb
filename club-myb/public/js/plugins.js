$(document).ready(function(){
	
	$(function(){
		$("span.typed-here").typed({
			strings: ["Nerd.", "Geek.", "Gamer."],
			typeSpeed: 80,
			loop: true,
			backDelay: 2000,
			showCursor: false,
			slideWidth: 250,
			maxSlides: 5,
		});
	});
	
	$(function() {
		$('a[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      
				if (target.length) {
					$('html, body').animate({
						scrollTop: target.offset().top
					}, 1000);
					
					return false;
				}
			}
		});
	});
	
	$('a[href="#menu"]').click(function(){
		$('body').toggleClass('menu-ativo');
		$('header nav.menu-principal').height($(document).height()-64);
	});

	var owl = $('#slider').owlCarousel({
	    loop:true,
	    margin:10,
	    autoplay: true,
	    autoplayTimeout: 3000,
	    navText: ["",""],
	    responsiveClass:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        800:{
	            items:3
	        },
	        1000:{
	            items:6
	        }
	    }
	});
	
	$('#funciona').owlCarousel({
		loop:true,
	    items:1,
	    nav: true,
	    navText: ["",""],
	    autoplay: true,
	    autoplayTimeout: 7000,
	});
    
    $('#tabs a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	});

    $('.cpf').mask('000.000.000-00');
    $('.cep').mask('00000-000');
    $('.phone').mask("(00) 0000-00000");

    var wow = new WOW(
    	{
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       true,       // trigger animations on mobile devices (default is true)
            live:         true,       // act on asynchronously loaded content (default is true)
            callback:     function(box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null // optional scroll container selector, otherwise use window
        }
    );

    wow.init();

});


var url_path = window.location.origin;
var public_path = window.location.origin+'/public';
var storage_path = window.location.origin+'/public/storage';
var current_path = window.location.href;

function successMessage(text)
{

    var n = noty({
        text        : text,
        type        : 'success',
        dismissQueue: true,
        progressBar : true,
        timeout     : 5000,
        layout      : 'topRight',
        closeWith   : ['click'],
        theme       : 'relax',
        maxVisible  : 10,
        animation   : {
            open  : 'animated bounceInUp',
            close : 'animated bounceOutUp',
            easing: 'swing',
            speed : 500
        }
    });
}

function errorMessage(text)
{
    var n = noty({
        text        : text,
        type        : 'error',
        dismissQueue: true,
        progressBar : true,
        timeout     : 5000,
        layout      : 'topRight',
        closeWith   : ['click'],
        theme       : 'relax',
        maxVisible  : 10,
        animation   : {
            open  : 'animated bounceInUp',
            close : 'animated bounceOutUp',
            easing: 'swing',
            speed : 500
        }
    });
}

function disableButton(button, text)
{
    $(button).addClass('disabled').attr('title', text);
}

function enableButton(button)
{
    $(button).removeClass('disabled').attr('title', "");
}
//# sourceMappingURL=plugins.js.map
