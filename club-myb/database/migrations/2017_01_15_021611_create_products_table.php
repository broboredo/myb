<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->text('description')->nullable();
            $table->string('img')->nullable();
            $table->float('price');
            $table->float('purchase_price');
            $table->integer('quantity')->default(1);
            $table->boolean('adults')->default(false);
            $table->boolean('subscriber_details')->default(false);
            $table->boolean('enable')->default(true);
            $table->unsignedInteger('partner_id')->nullable();
            $table->foreign('partner_id')->references('id')->on('partners')->onDelete('set null');
            $table->integer('coin_value');
            $table->boolean('vip')->default(false);
            $table->boolean('craft_product')->default(false);
            $table->timestamp('release');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
