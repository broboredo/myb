<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePurchaseHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('purchase_histories', function($table)
        {
            $table->dropForeign('purchase_histories_product_id_foreign');
        });


        Schema::table('purchase_histories', function($table)
        {
            $table->dropColumn('product_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_histories', function($table)
        {
            $table->unsignedInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');$table->dropColumn('product_id');
        });
    }
}
