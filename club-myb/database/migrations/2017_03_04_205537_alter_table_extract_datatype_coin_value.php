<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableExtractDatatypeCoinValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('extract_coins', function (Blueprint $table) {
            $table->dropColumn('coin_value');
        });

        Schema::table('extract_coins', function($table)
        {
            $table->decimal('coin_value', 5, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('extract_coins', function (Blueprint $table) {
            $table->dropColumn('coin_value');
        });

        Schema::table('extract_coins', function($table)
        {
            $table->integer('coin_value');
        });
    }
}
