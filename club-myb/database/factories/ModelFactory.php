<?php

use Stripe\Subscription;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
	static $password;

	return [
			'name' => $faker->name,
			'email' => $faker->unique()->safeEmail,
			'cpf' => $faker->unique()->randomNumber(),
			'password' => $password ?: $password = bcrypt('secret'),
			'remember_token' => str_random(10),
			'date_birth' => $faker->dateTimeAD,
			'photo_url' => $faker->imageUrl(),
	];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\Laravel\Spark\Subscription::class, function (Faker\Generator $faker) {
    
    return [
		'user_id' => App\User::all()->random()->id,
		'name' => $faker->name,
		'stripe_id' => $faker->word,
		'stripe_plan' => $faker->word,
		'quantity' => $faker->numberBetween(1, 12)
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Product::class, function (Faker\Generator $faker) {

	return [
		'name' => $faker->name,
		'description' => $faker->text,
		'img' => $faker->imageUrl(),
		'price' => $faker->randomFloat(2, 0, 150),
		'purchase_price' => $faker->randomFloat(2, 0, 300),
		'quantity' => $faker->randomNumber(3),
		'adults' => $faker->boolean,
		'subscriber_details' => $faker->boolean,
		'enable' => $faker->boolean,
		'partner_id' => function () {
			return factory(App\Myb\Partner::class)->create()->id;
		},
		'release' => $faker->dateTime,
		'vip' => $faker->boolean,
		'craft_product' => $faker->boolean,
		'release' => date("Y-m-d H:i:s", strtotime("+1 month"))
	];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Myb\Partner::class, function (Faker\Generator $faker) {

	return [
		'name' => $faker->name,
		'company_name' => $faker->name,
		'url' => $faker->url,
		'contract_end' => $faker->dateTime,
		'cnpj' => $faker->unique()->randomNumber()
	];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Cart::class, function (Faker\Generator $faker) {

	return [
		'user_id' => function () {
			return factory(App\User::class)->create()->id;
		},
		'product_id' => function () {
			return factory(App\Product::class)->create()->id;
		}
	];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Myb\ExtractCoins::class, function (Faker\Generator $faker) {

	$integer = $faker->numberBetween(-100, 75);
	
	return [
		'user_id' => function () {
			return factory(App\User::class)->create()->id;
		},
		'coin_value' => $integer,
		'debit' => ($integer < 0) ? true : false,
		'active' => $faker->boolean,
	];
});


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(\App\PurchaseHistory::class, function (Faker\Generator $faker) {

	return [
			'user_id' => App\User::all()->random()->id,
			'product_id' => App\Product::all()->random()->id,
			'status' => 'Enviado',
	];
});
