var elixir = require('laravel-elixir'),
	path = require('path'),
	watch = require('gulp-watch');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


gulp.task('stream', function () {
    return watch('resource/assets/css/**/*.css', { ignoreInitial: false })
        .pipe(gulp.dest('public/css/style.css'));
});

elixir(function(mix) {
	//spark
    mix.less('app.less')
        .webpack('app.js', null, null, {
            resolve: {
                modules: [
                    path.resolve(__dirname, 'vendor/laravel/spark/resources/assets/js'),
                    'node_modules'
                ]
            }
        })
        .copy('node_modules/sweetalert/dist/sweetalert.min.js', 'public/js/sweetalert.min.js')
        .copy('node_modules/sweetalert/dist/sweetalert.css', 'public/css/sweetalert.css');
    

    //bootstrap
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'public/js/bootstrap.min.js');
    mix.copy('node_modules/bootstrap/dist/css/bootstrap.min.css', 'public/css/bootstrap.min.css');
    mix.copy('node_modules/bootstrap/dist/css/bootstrap-theme.min.css', 'public/css/bootstrap-theme.min.css');
    mix.copy('node_modules/bootstrap/dist/fonts', 'public/fonts');
    mix.copy('vendor/igorescobar/jquery-mask-plugin/src/jquery.mask.js', 'public/js/jquery.mask.js');
    
    //bootstrap-select
    mix.copy('node_modules/bootstrap-select/dist/css/bootstrap-select.min.css', 'public/css/bootstrap-select.min.css');
    mix.copy('node_modules/bootstrap-select/dist/js/ie18n/**/*', 'public/js/ie18n/');
    mix.copy('node_modules/bootstrap-select/dist/js/bootstrap-select.min.js', 'public/js/bootstrap-select.min.js');

    //animate.css
    mix.copy('node_modules/animate.css/animate.min.css', 'public/css/animate.min.css');

    //wowjs
    mix.copy('node_modules/wowjs/dist/wow.min.js', 'public/js/wow.min.js');

    //noty
    mix.copy('node_modules/noty/js/noty/packaged/jquery.noty.packaged.min.js', 'public/js/noty.min.js');

    mix.scripts(['node_modules/jquery/dist/jquery.js'], 'public/js/jquery.min.js');
    mix.scripts(['plugins/typed.min.js'], 'public/js/typed.min.js');
    mix.scripts(['plugins/owl.carousel.min.js'], 'public/js/owl.carousel.min.js');

    mix.scripts(['admin/custom.js'], 'public/js/admin/admin.js');
    
    //scripts
    mix.scripts([
    	'custom.js',
    ],
    'public/js/plugins.js');

    //vitrine scripts
    mix.scripts([
        'vitrine.js'
    ],
    'public/js/vitrine.js');

    //styles
    mix.styles([
    	'fonts.css',
        'typed.css',
        'site/default.css',
        'site/menu.css',
        'site/banner.css',
        'site/works.css',
        'site/video.css',
        'site/plans.css',
        'site/friends.css',
        'site/contato.css',
        'site/about.css',
        'site/register.css',
        'site/form.css',
        'site/form.custom.css',
        'site/terms.css',
        'plugins/owl.carousel.min.css'
    ],
	'public/css/style.css');

    //styles
    mix.styles([
        'profile/default.css',
        'profile/menu.css',
        'profile/vitrine.css',
        'profile/product.css',
        'profile/my-box.css',
        'profile/details-plan.css',
        'profile/change-plan.css',
        'profile/fidelity.css',
    ],
    'public/css/profile.css');

    //styles
    mix.styles([
        'admin/custom.css'
    ],
    'public/css/admin/admin.css');
    
    //IE
    mix.styles(['ie8.css'], 'public/css/ie8.css');
    mix.styles(['ie9.css'], 'public/css/ie9.css');
    
    //fonts
    mix.copy('resources/assets/fonts', 'public/fonts');
    
    //images
    mix.copy('resources/assets/img', 'public/img');
});
